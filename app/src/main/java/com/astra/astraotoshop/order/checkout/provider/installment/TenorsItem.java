package com.astra.astraotoshop.order.checkout.provider.installment;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TenorsItem{

	@SerializedName("name")
	private String name;

	@SerializedName("value")
	private List<String> value;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setValue(List<String> value){
		this.value = value;
	}

	public List<String> getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"TenorsItem{" + 
			"name = '" + name + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}