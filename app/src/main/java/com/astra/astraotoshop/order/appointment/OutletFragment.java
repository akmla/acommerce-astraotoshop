package com.astra.astraotoshop.order.appointment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.appointment.provider.AppointmentPresenter;
import com.astra.astraotoshop.order.appointment.provider.SlotItem;
import com.astra.astraotoshop.utils.base.BaseDialogFragment;
import com.astra.astraotoshop.utils.view.FontIconic;
import com.astra.astraotoshop.utils.view.Loading;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 3/12/2018.
 */

public class OutletFragment extends BaseDialogFragment implements AppointmentContract.AppointmentView, BackListener, AdapterView.OnItemClickListener, OutletAdapter.OutletListener {

    @BindView(R.id.back)
    FontIconic back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.listItem)
    ListView listItem;
    @BindView(R.id.save)
    Button save;
    Unbinder unbinder;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.searchContainer)
    ConstraintLayout searchContainer;

    private AppointmentContract.AppointmentPresenter presenter;
    private OutletAdapter adapter;
    private int position = 0;
    private final String[] searchHint = {"Cari Provinsi", "Cari Kota/Kabupaten"};
    private final String[] titles = {"Provinsi", "Kota/Kabupaten", "Bengkel"};
    private String outlet;
    private Loading loading;

    public static OutletFragment newInstance(String productId) {

        Bundle args = new Bundle();
        args.putString("productId", productId);
        OutletFragment fragment = new OutletFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String productId = getArguments().getString("productId");
        presenter = new AppointmentPresenter(this, productId);
        adapter = new OutletAdapter(getContext(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragement_outlet, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter.getAppointment("");
        listItem.setOnItemClickListener(this);
        loading = new Loading(getContext(), getLayoutInflater());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        title.setText(titles[position]);
        listItem.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().setOnKeyListener((dialog, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_UP) {
                onBack();
                return true;
            }
            return false;
        });
    }

    @Override
    public void onBackPressed() {
        System.out.println();
    }

    @Override
    public void onDataAvailable() {
        adapter.setData(presenter.getData());
        updateUI();
        search.setText("");
    }

    @Override
    public void onSlotAvailable(List<SlotItem> slotItems) {
        ((AppointmentActivity) getActivity()).setOutlet(slotItems, outlet, presenter.getAppointment());
        position = 0;
        if (loading != null) loading.dismiss();
        dismiss();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showButton(boolean isShow) {
        if (isShow) save.setVisibility(View.VISIBLE);
        else save.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String data = String.valueOf(parent.getItemAtPosition(position));
        if (((String) parent.getItemAtPosition(position)).contains("||")) {
            adapter.setCheckItem(view.findViewById(R.id.checkIcon));
            outlet = data;
            save.setEnabled(true);
        } else {
            presenter.getAppointment(data);
            this.position++;
            adapter.setData(new ArrayList<>());
        }
    }

    @OnTextChanged(R.id.search)
    public void onSearch(CharSequence charSequence, int start, int count, int after) {
        if (charSequence.length() > 2) {
            searchData(charSequence.toString());
        } else if (charSequence.length() == 0) {
            adapter.setData(presenter.getData());
        }
    }

    @OnClick(R.id.back)
    public void onBackClicked() {
        onBack();
    }

    @OnClick(R.id.save)
    public void onSaveClicked() {
        if (loading != null) loading.show("Harap Tungggu");
        presenter.getAppointment(outlet);
    }

    public BackListener getOnBackListener() {
        return this;
    }

    private void onBack() {
        search.setText("");
        save.setVisibility(View.GONE);
        presenter.clearPrevious();
        adapter.setData(presenter.getData());
        position--;
        if (position > -1)
            updateUI();
        else {
            position = 0;
            dismiss();
        }
    }

    private void updateUI() {
        title.setText(titles[position]);
        if (presenter.getData().get(0).contains("||")) {
            searchContainer.setVisibility(View.GONE);
            save.setEnabled(false);
        } else {
            search.setHint(searchHint[position]);
            searchContainer.setVisibility(View.VISIBLE);
        }
    }

    private void searchData(String value) {
        Observable.from(presenter.getData())
                .filter(s -> s.toLowerCase().contains(value.toLowerCase()))
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        strings -> adapter.setData(strings),
                        er -> {
                        }
                );


    }

    @Override
    public void onUpdate() {
//        search.setText("");
    }
}
