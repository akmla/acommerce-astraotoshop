package com.astra.astraotoshop.order.checkout;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodsItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.FontIconic;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 2/7/2018.
 */

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.PaymentMethodViewHolder> {

    private List<PaymentMethodsItem> methodsItems;
    private ListListener listener;
    private FontIconic checkState;

    public PaymentMethodAdapter(ListListener listener) {
        this.listener = listener;
        methodsItems = new ArrayList<>();
    }

    @Override
    public PaymentMethodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PaymentMethodViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_choice_check, parent, false));
    }

    @Override
    public void onBindViewHolder(PaymentMethodViewHolder holder, int position) {
        try {
            holder.name.setText(methodsItems.get(position).getTitle());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return methodsItems.size();
    }

    public void setMethodsItems(List<PaymentMethodsItem> methodsItems) {
        if (methodsItems != null) {
            this.methodsItems = methodsItems;
            notifyDataSetChanged();
        }
    }


    class PaymentMethodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.checkMark)
        FontIconic checkMark;

        public PaymentMethodViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                if (checkState != null) checkState.setVisibility(View.GONE);
                checkState = checkMark;
                checkMark.setVisibility(View.VISIBLE);
                listener.onItemClick(android.R.id.text1, getAdapterPosition(), methodsItems.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
