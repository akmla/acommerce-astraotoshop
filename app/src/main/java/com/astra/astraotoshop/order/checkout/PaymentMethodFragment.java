package com.astra.astraotoshop.order.checkout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodsItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 2/7/2018.
 */

public class PaymentMethodFragment extends DialogFragment implements ListListener {

    @BindView(R.id.paymentMethod)
    RecyclerView paymentMethod;
    Unbinder unbinder;
    private PaymentMethodAdapter adapter;
    private List<PaymentMethodsItem> methodsItems;
    private PaymentMethodsItem paymentMethodsItem;

    public static PaymentMethodFragment newInstance() {

        Bundle args = new Bundle();

        PaymentMethodFragment fragment = new PaymentMethodFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
        adapter = new PaymentMethodAdapter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_method2, container, false);
        unbinder = ButterKnife.bind(this, view);
        paymentMethod.setLayoutManager(new LinearLayoutManager(getContext()));
        paymentMethod.setAdapter(adapter);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter.setMethodsItems(methodsItems);
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        paymentMethodsItem = (PaymentMethodsItem) data;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.save)
    public void save() {
        if (paymentMethodsItem != null &&getActivity() instanceof Checkout2Activity) {
            ((Checkout2Activity) getActivity()).setPaymentMethod(paymentMethodsItem);
        } else dismiss();
    }

    public void setMethodsItems(List<PaymentMethodsItem> methodsItems) {
        this.methodsItems = methodsItems;
    }

    @OnClick(R.id.close)
    public void onViewClicked() {
        dismiss();
    }
}
