package com.astra.astraotoshop.order.checkout.provider.address;

import com.google.gson.annotations.SerializedName;

public class Kelurahan {

    @SerializedName("value")
    private String value;

    @SerializedName("attribute_code")
    private String attributeCode="kelurahan";

    public Kelurahan() {
    }

    public Kelurahan(String value) {

        this.value = value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setAttributeCode(String attributeCode) {
        this.attributeCode = attributeCode;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    @Override
    public String toString() {
        return
                "Kelurahan{" +
                        "value = '" + value + '\'' +
                        ",attribute_code = '" + attributeCode + '\'' +
                        "}";
    }
}