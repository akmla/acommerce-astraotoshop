package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

public class CartListItem {

	@SerializedName("product_type")
	private String productType;

	@SerializedName("item_id")
	private int itemId;

	@SerializedName("price")
	private int price;

	@SerializedName("qty")
	private int qty;

	@SerializedName("quote_id")
	private int quoteId;

	@SerializedName("name")
	private String name;

	@SerializedName("sku")
	private String sku;

	@SerializedName("extension_attributes")
	private ExtensionAttributes extensionAttributes;

	public CartListItem(String sku,ExtensionAttributes extensionAttributes) {
		this.sku = sku;
		this.extensionAttributes = extensionAttributes;
	}

	public CartListItem(int itemId, ExtensionAttributes extensionAttributes) {
		this.itemId = itemId;
		this.extensionAttributes = extensionAttributes;
	}

	public CartListItem() {
	}

	public ExtensionAttributes getExtensionAttributes() {
		return extensionAttributes;
	}

	public void setExtensionAttributes(ExtensionAttributes extensionAttributes) {
		this.extensionAttributes = extensionAttributes;
	}

	private boolean isNeedUpdate;

	public boolean isNeedUpdate() {
		return isNeedUpdate;
	}

	public void setNeedUpdate(boolean needUpdate) {
		isNeedUpdate = needUpdate;
	}

	public void setProductType(String productType){
		this.productType = productType;
	}

	public String getProductType(){
		return productType;
	}

	public void setItemId(int itemId){
		this.itemId = itemId;
	}

	public int getItemId(){
		return itemId;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setQty(int qty){
		this.qty = qty;
	}

	public int getQty(){
		return qty;
	}

	public void setQuoteId(int quoteId){
		this.quoteId = quoteId;
	}

	public int getQuoteId(){
		return quoteId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setSku(String sku){
		this.sku = sku;
	}

	public String getSku(){
		return sku;
	}

	@Override
 	public String toString(){
		return 
			"CartListItem{" +
			"product_type = '" + productType + '\'' + 
			",item_id = '" + itemId + '\'' + 
			",price = '" + price + '\'' + 
			",qty = '" + qty + '\'' + 
			",quote_id = '" + quoteId + '\'' + 
			",name = '" + name + '\'' + 
			",sku = '" + sku + '\'' + 
			"}";
		}
}