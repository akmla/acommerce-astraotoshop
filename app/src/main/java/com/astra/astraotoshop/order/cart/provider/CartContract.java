package com.astra.astraotoshop.order.cart.provider;

import com.astra.astraotoshop.order.cart.provider.totals.CartTotalEntity;

import java.util.List;

import rx.functions.Action1;

/**
 * Created by Henra Setia Nugraha on 1/16/2018.
 */

public interface CartContract {
    interface CartView {
        void setCartList(List<CartListItem> items);

        void onCartItemDeleted();

        void onCartUpdated();

        void showError();
    }

    interface ExternalCart {
        void onItemAdded();
        void onItemFailedToAdd(String message);
        void onItemUpdated();
        void onItemFailedToUpdate(String message);
        void onItemDeleted();
        void onItemFailedToDelete();
    }

    interface CartPresenter {
        void addToCart(String sku, int qty);

        void addToCart(String sku, int qty, List<AppointmentParamItem> appointmentItem);

        void onAdded();

        void onFailedToAdd(Throwable throwable);

        void updateCart(CartListItem data);

        void onUpdated();

        void onFailedToUpdate(Throwable throwable);

        void deleteCart(String itemId);

        void onDeleted();

        void onFailedToDelete(Throwable throwable);

        void requestCartList();

        void onCartListReceived(CartListEntity carts);

        void onFailedReceiving(Throwable throwable);

        List<CartListItem> getCartsItem();
    }

    interface CartPriceView {
        void setTotalCart(CartTotalEntity cart);

        void showError();
    }

    interface CartPricePresenter {
        void getTotal(String storeCode);

        void onTotalReceived(CartTotalEntity cartTotal);

        void onFailedReceivingTotal(Throwable throwable);

        CartTotalEntity getTotal();
    }

    interface CartPriceInteractor {
        void requestTotal(String storeCode);
    }

    interface CartVoucherView {
        void setVoucherView(boolean isAdd);

        void showVoucherError(boolean isAdd);
    }

    interface CartVoucherPresenter {
        void addVoucher(String voucherCode);

        void deleteVoucher();

        void onVoucherAdded();

        void onVoucherDeleted();

        void onAddVoucherFailed();

        void onDeleteVoucherFailed();
    }

    interface CartVoucherInteractor {
        void addVoucher(String storeCode, String voucherCode);

        void deleteVoucher(String storeCode);
    }

    interface CartInteractor {
        void requestQuoteId(CartQuoteEntity userId, String storeCode, Action1<String> success, Action1<Throwable> error);

        void requestCartList(String storeCode);

        void addToCart(String storeCode, AddCartBody cart);

        void updateCart(String storeCode, int itemId, int quoteId, UpdateCartBody cartBody);

        void deleteCart(String storeCode, String itemId);
    }
}
