package com.astra.astraotoshop.order.cart.provider.totals;

import com.astra.astraotoshop.order.cart.provider.CartContract;

/**
 * Created by Henra Setia Nugraha on 1/28/2018.
 */

public class CartPricePresenter implements CartContract.CartPricePresenter {

    private CartContract.CartPriceInteractor interactor;
    private CartContract.CartPriceView view;
    private CartTotalEntity cartTotalEntity;

    public CartPricePresenter(CartContract.CartPriceView view) {
        this.view = view;
        interactor = new CartPriceInteractor(this);
    }

    @Override
    public void getTotal(String storeCode) {
        interactor.requestTotal(storeCode);
    }

    @Override
    public void onTotalReceived(CartTotalEntity cartTotal) {
        this.cartTotalEntity=cartTotal;
        view.setTotalCart(cartTotal);
    }

    @Override
    public void onFailedReceivingTotal(Throwable throwable) {
        System.out.println();
    }

    @Override
    public CartTotalEntity getTotal() {
        return cartTotalEntity;
    }
}
