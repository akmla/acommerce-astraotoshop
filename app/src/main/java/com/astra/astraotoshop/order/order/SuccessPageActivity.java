package com.astra.astraotoshop.order.order;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.astra.astraotoshop.R;
//import com.astra.astraotoshop.home.product.HomeProductActivity;
import com.astra.astraotoshop.order.cart.CartAppointmentAdapter;
import com.astra.astraotoshop.order.cart.provider.CartListItem;
import com.astra.astraotoshop.order.cart.provider.ExtensionAttributes;
import com.astra.astraotoshop.order.checkout.CheckoutContract;
import com.astra.astraotoshop.order.checkout.ShippingProductAdapter;
import com.astra.astraotoshop.order.checkout.provider.ItemMultiWarehouse;
import com.astra.astraotoshop.order.checkout.provider.ShippingMethodEntity;
import com.astra.astraotoshop.order.checkout.provider.ShippingPresenter;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodsItem;
import com.astra.astraotoshop.user.order.OrderContract;
import com.astra.astraotoshop.user.order.provider.OrderDetail;
import com.astra.astraotoshop.user.order.provider.OrderDetailPresenter;
import com.astra.astraotoshop.user.order.provider.ProductssItem;
import com.astra.astraotoshop.user.profile.provider.Addresses;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SuccessPageActivity extends BaseActivity implements OrderContract.OrderDetailView {

    @BindView(R.id.orderid)
    TextView orderid;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.deliverName)
    TextView deliverName;
    @BindView(R.id.paymentLabelContainer)
    RelativeLayout paymentLabelContainer;
    @BindView(R.id.paymentMethod)
    TextView paymentMethod;
    @BindView(R.id.products)
    RecyclerView products;
    @BindView(R.id.shop)
    Button shop;
    @BindView(R.id.deliveryPrice)
    TextView deliveryPrice;
    @BindView(R.id.vaNumber)
    TextView vaNumber;
    @BindView(R.id.shippingContainer)
    LinearLayout shippingContainer;

    private OrderContract.OrderDetailPresenter presenter;
    private CheckoutContract.ShippingPresenter shippingPresenter = new ShippingPresenter();
    private ShippingProductAdapter shippingAdapter;
    private CartAppointmentAdapter appointmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.success_page);
        ButterKnife.bind(this);
        presenter = new OrderDetailPresenter(this);
        String orderId = getIntent().getStringExtra("orderId");
        String storeCode = getIntent().getStringExtra("storeCode");
        setAddressValue();
        listSetup();
        setShippingValue();
        setPaymentValue();
        presenter.requestOrderDetail(orderId, storeCode);

        if (isProductService()) {
            shippingContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void showOrder(OrderDetail orderDetail) {
        orderid.setText("#" + orderDetail.getIncrementId());
        if (orderDetail.getPaymentMethod().equalsIgnoreCase("virtual"))
            vaNumber.setText(orderDetail.getVaNumber());
        if (isProductService()) mappingProductService(orderDetail);
        else mappingList(orderDetail.getItems());
    }

    @Override
    public void onError() {

    }

    private void listSetup() {
        shippingAdapter = new ShippingProductAdapter(this);
        products.setLayoutManager(new LinearLayoutManager(this));
        products.setAdapter(shippingAdapter);
    }

    private void setAddressValue() {
        Addresses addresses = getIntent().getParcelableExtra("address");
        if (addresses != null) {
            name.setText(addresses.getFirstname() + " " + addresses.getLastname());
            address.setText(StringFormat.getAddress(addresses));
        }
    }

    private void setShippingValue() {
        ShippingMethodEntity shipping = getIntent().getParcelableExtra("shipping");
        if (shipping != null) {
            String htmlValue = shipping.getMethodTitle() + "  <b>" +
                    StringFormat.addCurrencyFormat(shipping.getAmount()) + "</b>";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                deliverName.setText(Html.fromHtml(htmlValue, Html.FROM_HTML_MODE_LEGACY));
            } else deliverName.setText(Html.fromHtml(htmlValue));
//            deliverName.setText(shipping.getMethodTitle());
//            StringFormat.applyCurrencyFormat(deliveryPrice, shipping.getBaseAmount());
            shippingAdapter.setItemMultiWarehouses(shippingPresenter.getProductList(shipping.getMethodDescription()));
        }
    }

    private void setPaymentValue() {
        PaymentMethodsItem payment = getIntent().getParcelableExtra("payment");
        if (payment != null) {
            paymentMethod.setText(payment.getTitle());
        }
    }

    private void mappingList(List<ProductssItem> items) {
        List<ItemMultiWarehouse> multiWarehouses = new ArrayList<>();
        ItemMultiWarehouse warehouse;
        for (ProductssItem item : items) {
            warehouse = new ItemMultiWarehouse();
            warehouse.setImage(item.getImageUrl());
            warehouse.setName(item.getName());
            warehouse.setSKU(item.getSku());
            warehouse.setPrice(item.getOriginalPrice());
            warehouse.setSpecialPrice(item.getBasePrice());
            if (item.getMethodDescriptions().size() > 1) {
                warehouse.setQty(item.getMethodDescriptions().get(0).getQTY());
                warehouse.setEstimate(item.getMethodDescriptions().get(0).getESTIMATE());
                warehouse.setMultiEstimate(item.getMethodDescriptions().get(1).getESTIMATE());
                warehouse.setMultiQty(item.getMethodDescriptions().get(1).getQTY());
            } else {
                warehouse.setQty(item.getMethodDescriptions().get(0).getQTY());
                warehouse.setEstimate(item.getMethodDescriptions().get(0).getESTIMATE());
            }
            multiWarehouses.add(warehouse);
        }
        shippingAdapter.setItemMultiWarehouses(multiWarehouses);
    }

    private void mappingProductService(OrderDetail orderDetail) {
        appointmentAdapter = new CartAppointmentAdapter(this, false, null);
        CartListItem cartListItem = new CartListItem();
        cartListItem.setName(orderDetail.getItems().get(0).getName());

        ExtensionAttributes attributes = new ExtensionAttributes();
        attributes.setImageUrl(orderDetail.getItems().get(0).getImageUrl());
        attributes.setAppointmentsItemCart(new ArrayList<>(Collections.singletonList(orderDetail.getItems().get(0).getAppointmentsItemsCart())));
        cartListItem.setExtensionAttributes(attributes);

        try {
            cartListItem.setPrice(Integer.parseInt(orderDetail.getItems().get(0).getPrice().split("\\.")[0]));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        appointmentAdapter.setCarts(new ArrayList<>(Arrays.asList(cartListItem)));
        products.setAdapter(appointmentAdapter);
    }

    @OnClick(R.id.shop)
    public void toHome() {
//        Intent intent = getIntent(this, HomeProductActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//        finish();
    }
}
