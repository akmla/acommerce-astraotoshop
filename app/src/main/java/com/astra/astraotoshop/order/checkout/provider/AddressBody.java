package com.astra.astraotoshop.order.checkout.provider;

import com.google.gson.annotations.SerializedName;

public class AddressBody {

    @SerializedName("addressId")
    private int addressId;

    public AddressBody() {
    }

    public AddressBody(int addressId) {
        this.addressId = addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getAddressId() {
        return addressId;
    }

    @Override
    public String toString() {
        return
                "AddressShippingBody{" +
                        "addressId = '" + addressId + '\'' +
                        "}";
    }
}