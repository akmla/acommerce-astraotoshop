package com.astra.astraotoshop.order.checkout.provider.address;

import com.google.gson.annotations.SerializedName;

public class CustomAttributes {

    @SerializedName("mobile_phone")
    private MobilePhone mobilePhone;

    @SerializedName("kelurahan")
    private Kelurahan kelurahan;

    public CustomAttributes() {
    }

    public CustomAttributes(MobilePhone mobilePhone, Kelurahan kelurahan) {

        this.mobilePhone = mobilePhone;
        this.kelurahan = kelurahan;
    }

    public void setMobilePhone(MobilePhone mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public MobilePhone getMobilePhone() {
        return mobilePhone;
    }

    public void setKelurahan(Kelurahan kelurahan) {
        this.kelurahan = kelurahan;
    }

    public Kelurahan getKelurahan() {
        return kelurahan;
    }

    @Override
    public String toString() {
        return
                "CustomAttributes{" +
                        "mobile_phone = '" + mobilePhone + '\'' +
                        ",kelurahan = '" + kelurahan + '\'' +
                        "}";
    }
}