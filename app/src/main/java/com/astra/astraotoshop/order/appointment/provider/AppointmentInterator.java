package com.astra.astraotoshop.order.appointment.provider;

import android.content.Context;

import com.astra.astraotoshop.order.appointment.AppointmentContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 3/6/2018.
 */

public class AppointmentInterator extends BaseInteractor implements AppointmentContract.AppontmentInteractor {

    private AppointmentContract.AppointmentPresenter presenter;

    public AppointmentInterator(AppointmentContract.AppointmentPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void getAppointment(String productId, Map<String, String> query) {
        getGeneralNetworkManager()
                .getAvailable(
                        new NetworkHandler(),
                        productId,
                        query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        entity -> {
                            presenter.onAppointmentReceived(entity);
                        },
                        e->{
                            presenter.onFailedReceiveData(e);
                        }
                );
    }
}
