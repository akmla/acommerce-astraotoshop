package com.astra.astraotoshop.order.appointment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.utils.view.FontIconic;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 3/12/2018.
 */

public class OutletAdapter extends BaseAdapter {
    private List<String> data;
    private Context context;
    private FontIconic tempIcon;
    private OutletListener listener;

    public OutletAdapter(Context context, OutletListener listener) {
        this.context = context;
        this.listener = listener;
        data = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_outlet2, parent, false);
        }

        ViewHolder holder = new ViewHolder(convertView);

        String name = data.get(position);
        if (name.contains("||")) {
            if (String.valueOf(name.charAt(name.indexOf("||") + 2)).contains(" "))
                name = name.substring(0, name.indexOf("|") - 1) + "\n" + name.substring(name.indexOf("|") + 3);
            else name = name.replace("||", "\n");
            holder.name.setText(name);
            holder.arrowIcon.setVisibility(View.INVISIBLE);
            holder.checkIcon.setVisibility(View.GONE);
        } else {
            holder.name.setText(data.get(position));
            holder.checkIcon.setVisibility(View.GONE);
        }

        return convertView;
    }

    public void updateUI() {
        if (listener != null) {
            listener.onUpdate();
        }
    }

    public void setData(List<String> data) {
        listener.onUpdate();
        this.data = data;
        notifyDataSetChanged();
    }

    public boolean setCheckItem(FontIconic checkItem) {
        if (checkItem != null) {
            if (tempIcon != null) {
                tempIcon.setVisibility(View.GONE);
            }
            tempIcon = checkItem;
            checkItem.setVisibility(View.VISIBLE);
        }
        return true;
    }

    static class ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.arrowIcon)
        FontIconic arrowIcon;
        @BindView(R.id.checkIcon)
        FontIconic checkIcon;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    interface OutletListener {
        void onUpdate();
    }
}
