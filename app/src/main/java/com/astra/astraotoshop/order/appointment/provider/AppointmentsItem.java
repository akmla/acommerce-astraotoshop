package com.astra.astraotoshop.order.appointment.provider;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AppointmentsItem {

    @SerializedName("address")
    private List<String> address;

    @SerializedName("city")
    private List<String> city;

    @SerializedName("slot")
    private List<SlotItem> slot;

    @SerializedName("region")
    private List<String> region;

    @SerializedName("holiday")
    private List<Object> holiday;

    public AppointmentsItem() {
        this.address = new ArrayList<>();
        this.city = new ArrayList<>();
        this.slot = new ArrayList<>();
        this.region = new ArrayList<>();
    }

    public void setAddress(List<String> address) {
        this.address = address;
    }

    public List<String> getAddress() {
        return address;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    public List<String> getCity() {
        return city;
    }

    public void setSlot(List<SlotItem> slot) {
        this.slot = slot;
    }

    public List<SlotItem> getSlot() {
        return slot;
    }

    public void setRegion(List<String> region) {
        this.region = region;
    }

    public List<String> getRegion() {
        return region;
    }

    public void setHoliday(List<Object> holiday) {
        this.holiday = holiday;
    }

    public List<Object> getHoliday() {
        return holiday;
    }

    @Override
    public String toString() {
        return
                "AppointmentsItemCart{" +
                        "address = '" + address + '\'' +
                        ",city = '" + city + '\'' +
                        ",slot = '" + slot + '\'' +
                        ",region = '" + region + '\'' +
                        ",holiday = '" + holiday + '\'' +
                        "}";
    }
}