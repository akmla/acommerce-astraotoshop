package com.astra.astraotoshop.order.appointment;

import com.astra.astraotoshop.order.appointment.provider.AppointmentEntity;
import com.astra.astraotoshop.order.appointment.provider.AppointmentsItem;
import com.astra.astraotoshop.order.appointment.provider.SlotItem;
import com.astra.astraotoshop.order.cart.provider.AppointmentParamItem;

import java.util.List;
import java.util.Map;

/**
 * Created by Henra Setia Nugraha on 3/5/2018.
 */

public interface AppointmentContract {
    interface AppointmentView{
        void onDataAvailable();
        void showError(String message);
        void showButton(boolean isShow);
        void onSlotAvailable(List<SlotItem> slotItems);
    }

    interface AppointmentExternal{
        void setSlot(List<SlotItem> slots);
        void showSlotErrorMessage();
    }

    interface AppointmentPresenter{
        void getAppointment(String data);
        void onAppointmentReceived(AppointmentEntity entity);
        void onFailedReceiveData(Throwable e);
        void getSlotItems(String region,String city,String address);

        List<String> getData();
        List<SlotItem> getSlot();
        void clearPrevious();
        void clearData();
        AppointmentsItem getAppointment();
    }

    interface AppontmentInteractor{
        void getAppointment(String productId, Map<String,String> query);
    }

    interface MainAppointment{
        void addToCart(List<AppointmentParamItem> params);
    }

    interface MainAppointmentPresenter{
        void setOutlet();
        void setSchedule();
        void generateAppointment();
    }
}
