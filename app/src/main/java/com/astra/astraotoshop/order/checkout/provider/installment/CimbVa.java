package com.astra.astraotoshop.order.checkout.provider.installment;

import com.google.gson.annotations.SerializedName;

public class CimbVa {

    @SerializedName("instructions")
    private String instructions;

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getInstructions() {
        return instructions;
    }

    @Override
    public String toString() {
        return
                "CimbVa{" +
                        "instructions = '" + instructions + '\'' +
                        "}";
    }
}