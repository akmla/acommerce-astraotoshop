package com.astra.astraotoshop.order.checkout.provider;

import com.astra.astraotoshop.order.checkout.CheckoutContract;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 2/8/2018.
 */

public class ShippingPresenter implements CheckoutContract.ShippingPresenter {


    @Override
    public List<ItemMultiWarehouse> getProductList(String productJSON) {
        Type collectionType = new TypeToken<List<ItemMultiWarehouse>>() {
        }.getType();
        return mappingMultiWarehouse(new Gson().fromJson(productJSON, collectionType));
    }

    private List<ItemMultiWarehouse> mappingMultiWarehouse(List<ItemMultiWarehouse> items) {
        List<ItemMultiWarehouse> multiWarehouses = new ArrayList<>();
        if (items != null) {
            int i = 0;
//            do {
//                multiWarehouses.add(items.get(i));
//
//                if (i < (items.size() - 1)) {
//                    if (items.get(i + 1).getItemId().equals(multiWarehouses.get(i).getItemId())) {
//                        multiWarehouses.get(i).setMultiEstimate(items.get(i+1).getEstimate());
//                        multiWarehouses.get(i).setMultiQty(items.get(i+1).getQty());
//                        items.remove(i + 1);
//                    }
//                }
//
//                i++;
//            } while (i < items.size());
            do {
                if (i < (items.size() - 1)) {
                    if (items.get(i + 1).getItemId().equals(items.get(i).getItemId())) {
                        items.get(i).setMultiEstimate(items.get(i+1).getEstimate());
                        items.get(i).setMultiQty(items.get(i+1).getQty());
                        items.remove(i + 1);
                    }
                }
                i++;
            }while (i < items.size());
        }
        return items;
    }
}
