package com.astra.astraotoshop.order.checkout;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 11/23/2017.
 */

public class CheckoutAddressAdapter extends RecyclerView.Adapter<CheckoutAddressAdapter.CheckoutAddressViewHolder> {

    Context context;

    public CheckoutAddressAdapter(Context context) {
        this.context = context;
    }

    @Override
    public CheckoutAddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CheckoutAddressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkout_address, parent, false));
    }

    @Override
    public void onBindViewHolder(CheckoutAddressViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class CheckoutAddressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkIcon)
        TextView checkIcon;

        CheckoutAddressViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/fontawesome.ttf");
            checkIcon.setTypeface(typeface);
        }
    }
}
