package com.astra.astraotoshop.order.checkout.provider.payment;

import com.google.gson.annotations.SerializedName;

public class PaymentMethodBody {

    @SerializedName("paymentMethod")
    private PaymentMethod paymentMethod;

    public PaymentMethodBody(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    @Override
    public String toString() {
        return
                "PaymentMethodBody{" +
                        "paymentMethod = '" + paymentMethod + '\'' +
                        "}";
    }
}