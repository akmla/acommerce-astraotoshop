package com.astra.astraotoshop.order.checkout.revamp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;

public class PaymentListAdapter extends RecyclerView.Adapter<PaymentListAdapter.PaymentListViewHolder> {
    @NonNull
    @Override
    public PaymentListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PaymentListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentListViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    class PaymentListViewHolder extends RecyclerView.ViewHolder {
        PaymentListViewHolder(View itemView) {
            super(itemView);
        }
    }
}
