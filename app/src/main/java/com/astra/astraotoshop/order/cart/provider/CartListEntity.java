package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartListEntity{

	@SerializedName("is_virtual")
	private boolean isVirtual;

	@SerializedName("is_active")
	private boolean isActive;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("items")
	private List<CartListItem> items;

	public void setIsVirtual(boolean isVirtual){
		this.isVirtual = isVirtual;
	}

	public boolean isIsVirtual(){
		return isVirtual;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setItems(List<CartListItem> items){
		this.items = items;
	}

	public List<CartListItem> getItems(){
		return items;
	}

	@Override
 	public String toString(){
		return 
			"CartListEntity{" + 
			"is_virtual = '" + isVirtual + '\'' + 
			",is_active = '" + isActive + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",items = '" + items + '\'' + 
			"}";
		}
}