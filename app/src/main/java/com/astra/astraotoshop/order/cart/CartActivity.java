package com.astra.astraotoshop.order.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.checkout.Checkout2Activity;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CartActivity extends BaseActivity {

    @BindView(R.id.itemList)
    RecyclerView itemList;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getState() == getResources().getInteger(R.integer.state_product_service))
            setupProductServiceList();
        else if ((getState() == getResources().getInteger(R.integer.state_product)))
            setupProductList();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        itemList.setLayoutManager(layoutManager);
    }


    private void setupProductServiceList() {
        CartAdapter cartAdapter = new CartAdapter(this);
        itemList.setAdapter(cartAdapter);
    }

    private void setupProductList() {
        CartAppointmentAdapter productAdapter = new CartAppointmentAdapter(this, null);
        itemList.setAdapter(productAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.checkout)
    public void checkout() {
        Intent intent = new Intent(this, Checkout2Activity.class);
        intent.putExtra(STATE, getState());
        startActivity(intent);
    }
}
