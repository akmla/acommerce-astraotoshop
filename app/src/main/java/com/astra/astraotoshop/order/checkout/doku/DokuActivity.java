package com.astra.astraotoshop.order.checkout.doku;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.astra.astraotoshop.AOPApplication;
import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.checkout.provider.ShippingMethodEntity;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodsItem;
import com.astra.astraotoshop.order.order.SuccessPageActivity;
import com.astra.astraotoshop.user.profile.provider.Addresses;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DokuActivity extends BaseActivity implements WebViewListener {

    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private String orderCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doku);
        ButterKnife.bind(this);
        actionBarSetup();
        webSetup();
        orderCode = getIntent().getStringExtra("orderCode");
        if (orderCode != null) {
            String url ="";
//                    String.format(BuildConfig.MAINURL+"%s/dokuhosted/payment/request?order_id=%s&is_mobile=1", getStoreCode(), orderCode);
//            String url = "https://www.astraotoshop.com";
            if (getIntent().hasExtra("bank") && getIntent().hasExtra("tenor")) {
                url += "&customer_bank=" + getIntent().getStringExtra("bank") + "&tennor=" + getIntent().getStringExtra("tenor");
            }
            webview.loadUrl(url);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void webSetup() {
        webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebChromeClient(new WebChromeClient());
        webview.setWebViewClient(new AOPWebClient(this, this));
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setBuiltInZoomControls(false);
        webview.clearHistory();
    }

    @Override
    public void onSuccess() {
        toSuccessPage();
    }

    @Override
    public void onFailed() {
        Toast.makeText(this, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
//        AOPApplication.start();
    }

    @Override
    public void onLoaded() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        webview.getSettings().setJavaScriptEnabled(false);
        super.onDestroy();
    }

    private void toSuccessPage() {
        Intent intent = getIntent(this, SuccessPageActivity.class);
        intent.putExtra("storeCode", getStoreCode());
        intent.putExtra("orderId", getIntent().getStringExtra("orderCode"));
        intent.putExtra("address", (Addresses) getIntent().getParcelableExtra("address"));
        intent.putExtra("shipping", (ShippingMethodEntity) getIntent().getParcelableExtra("shipping"));
        intent.putExtra("payment", (PaymentMethodsItem) getIntent().getParcelableExtra("payment"));
        startActivity(intent);
    }
}
