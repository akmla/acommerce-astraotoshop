package com.astra.astraotoshop;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.astra.astraotoshop.product.search.Contracts;
import com.astra.astraotoshop.product.search.TestInteractor;
import com.astra.astraotoshop.provider.entity.MenuProfile;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;
import com.astra.astraotoshop.utils.view.ListUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 10/18/2017.
 */

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.profile_name)
    TextView profileName;
    @BindView(R.id.profile_email)
    TextView profileEmail;
    @BindView(R.id.profile_update)
    TextView profileUpdate;
    @BindView(R.id.profile_list_menu)
    ListView profileListMenu;
    @BindView(R.id.profile_signout)
    TextView profileSignout;
    @BindView(R.id.scroll)
    ScrollView scroll;

    Contracts.Interactor interactor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        ButterKnife.bind(this);

        String[] items = {"panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        List<MenuProfile> profiles = ListUtil.getMenuList(this, 0);
        profileListMenu.setAdapter(adapter);
        ListUtil.setRealHeight(profileListMenu);

        testing();
    }

    @Override
    protected void onStart() {
        super.onStart();
        scroll.fullScroll(View.FOCUS_UP);
    }

    private void testing() {
        interactor = new TestInteractor(this);
        interactor.request(new NetworkHandler());
    }
}