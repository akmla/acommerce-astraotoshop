package com.astra.astraotoshop.utils.network.service;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.utils.network.GeneralNetworkManager;
import com.astra.astraotoshop.utils.network.handler.NetworkHandlerContract;
import com.astra.astraotoshop.utils.pref.Pref;

import rx.Observable;

/**
 * Created by Henra Setia Nugraha on 1/5/2018.
 */

public class CategoryService extends GeneralNetworkManager {

    public Observable<CategoryEntity> requestCategory(NetworkHandlerContract networkHandlerContract, String storeCode) {
        String header = "bearer " + Pref.getPreference().getString(BuildConfig.ATKN);
        return addInterceptor(getServices().getCategory(header), networkHandlerContract);
    }
}
