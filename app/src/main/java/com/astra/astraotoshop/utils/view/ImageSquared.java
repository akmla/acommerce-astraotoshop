package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Henra Setia Nugraha on 10/26/2017.
 */

public class ImageSquared extends ImageView {
    public ImageSquared(Context context) {
        super(context);
    }

    public ImageSquared(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageSquared(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(),getMeasuredWidth());
    }
}
