package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Henra Setia Nugraha on 11/29/2017.
 */

public class FontIconic extends TextView {
    public FontIconic(Context context) {
        super(context);
    }

    public FontIconic(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FontIconic(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        try {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/ionicons.ttf");
            setTypeface(typeface);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
