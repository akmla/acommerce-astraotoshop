package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.astra.astraotoshop.R;

public class CustomFontFace extends AppCompatTextView {
    public CustomFontFace(Context context) {
        super(context);
    }

    public CustomFontFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFontFace(context,attrs);
    }

    public CustomFontFace(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFontFace(context,attrs);
    }

    private void setFontFace(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontText);
        int cf = typedArray.getInt(R.styleable.CustomFontText_font_name, 0);
        int idFontName;
        switch (cf) {
            case 0:
                idFontName = R.string.ExoSemiBold;
                break;
            case 1:
                idFontName = R.string.RobotoRegular;
                break;
            default:
                idFontName = R.string.RobotoThin;
                break;
        }

        String fontName = getResources().getString(idFontName);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                "fonts/" + fontName + ".ttf");
        setTypeface(typeface);

        typedArray.recycle();
    }
}
