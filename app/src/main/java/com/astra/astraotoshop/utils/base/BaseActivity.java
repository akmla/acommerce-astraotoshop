package com.astra.astraotoshop.utils.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.profile.ProfileFragment;
import com.astra.astraotoshop.utils.pref.Pref;

/**
 * Created by Henra Setia Nugraha on 12/20/2017.
 */

public class BaseActivity extends AppCompatActivity {
    protected static final String STATE = "state";
    protected static final int REGISTER_CODE = 33;
    //TODO development
//    private int state = -11;
    //TODO prod
    private int state = 0;
    protected View cartBadge;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (getIntent().hasExtra(STATE))
            state = getIntent().getIntExtra(STATE, -11);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REGISTER_CODE && resultCode == RESULT_OK && !isLogin()) {
            showAccount();
        }
    }

    protected int getState() {
        return state;
    }

    protected void registerCustomMenu(Menu menu, int itemMenuID) {
        try {
            MenuItem menuItem = menu.findItem(itemMenuID);
            View itemView = menuItem.getActionView();
            itemView.setOnClickListener((View -> onOptionsItemSelected(menuItem)));
            cartBadge = itemView.findViewById(R.id.iconBadge);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCartBadgeVisibility(boolean isShow) {
        if (cartBadge != null) {
            if (isShow) cartBadge.setVisibility(View.VISIBLE);
            else cartBadge.setVisibility(View.GONE);
        }
    }

    protected void actionBarSetup(String title) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (!TextUtils.isEmpty(title))
            getSupportActionBar().setTitle(title);

        for (int i = 0; i < toolbar.getChildCount(); i++) {
            if (toolbar.getChildAt(i) instanceof TextView) {
                if (((TextView) toolbar.getChildAt(i)).getText().toString().equals(toolbar.getTitle())) {
                    ((TextView) toolbar.getChildAt(i)).setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/rubik.ttf"));
                }
            }
        }
    }

    protected void actionBarSetup() {
        actionBarSetup("");
    }

    public void setCustomTitle(String title) {
        try {
            getSupportActionBar().setTitle(title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected boolean isLogin() {
        return Pref.getPreference().getString(BuildConfig.UID) != null;
    }

    protected String getStoreCode() {
        return getResources().getStringArray(R.array.storeCode)[getState()];
    }

    public Intent getIntent(Context context, Class target) {
        return (new Intent(context, target)).putExtra(STATE, getState());
    }

    protected boolean isProductService() {
        return getState() == getResources().getInteger(R.integer.state_product_service);
    }

    protected void showAccount() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("account");
        if (fragment != null) transaction.remove(fragment);
        transaction.commit();
        ProfileFragment profileFragment = ProfileFragment.newInstance(getState());
        profileFragment.show(getSupportFragmentManager(), "account");
    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
