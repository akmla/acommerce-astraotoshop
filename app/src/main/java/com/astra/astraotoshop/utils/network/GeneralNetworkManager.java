package com.astra.astraotoshop.utils.network;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.home.provider.StaticBlock;
import com.astra.astraotoshop.home.provider.StaticCategory;
import com.astra.astraotoshop.order.appointment.provider.AppointmentEntity;
import com.astra.astraotoshop.order.cart.provider.AddCartBody;
import com.astra.astraotoshop.order.cart.provider.CartListEntity;
import com.astra.astraotoshop.order.cart.provider.CartQuoteEntity;
import com.astra.astraotoshop.order.cart.provider.UpdateCartBody;
import com.astra.astraotoshop.order.cart.provider.totals.CartTotalEntity;
import com.astra.astraotoshop.order.checkout.provider.AddressBody;
import com.astra.astraotoshop.order.checkout.provider.ShippingMethodEntity;
import com.astra.astraotoshop.order.checkout.provider.VaBody;
import com.astra.astraotoshop.order.checkout.provider.address.AddressShippingBody;
import com.astra.astraotoshop.order.checkout.provider.installment.PaymentDesc;
import com.astra.astraotoshop.order.checkout.provider.payment.AvailablePaymentMethod;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodBody;
import com.astra.astraotoshop.order.history.provider.OrderHistory;
import com.astra.astraotoshop.product.catalog.provider.ProductListEntity;
import com.astra.astraotoshop.product.filter.provider.BrandFilterEntity;
import com.astra.astraotoshop.product.product.provider.ProductEntity;
import com.astra.astraotoshop.product.review.provider.ReviewEntity;
import com.astra.astraotoshop.product.search.provider.Suggestion;
import com.astra.astraotoshop.provider.entity.Response;
import com.astra.astraotoshop.provider.entity.TokenEntity;
import com.astra.astraotoshop.user.address.provider.AddAddressBody;
import com.astra.astraotoshop.user.address.provider.CityEntity;
import com.astra.astraotoshop.user.address.provider.RegionEntity;
import com.astra.astraotoshop.user.order.provider.OrderDetail;
import com.astra.astraotoshop.user.password.provider.ChangePassword;
import com.astra.astraotoshop.user.password.provider.IsEmailAvailable;
import com.astra.astraotoshop.user.password.provider.ResetPassword;
import com.astra.astraotoshop.user.profile.provider.ProfileEntity;
import com.astra.astraotoshop.user.profile.provider.UpdateProfile;
import com.astra.astraotoshop.user.review.provider.CustomerReview;
import com.astra.astraotoshop.user.signin.provider.SocialLogin;
import com.astra.astraotoshop.user.signup.provider.Register;
import com.astra.astraotoshop.user.signup.provider.RegisterBody;
import com.astra.astraotoshop.user.subsricption.provider.Subscription;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherDetail;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherList;
import com.astra.astraotoshop.user.wishlist.provider.AddWishlistBody;
import com.astra.astraotoshop.user.wishlist.provider.WishlistEntity;
import com.astra.astraotoshop.utils.network.handler.NetworkHandlerContract;
import com.astra.astraotoshop.utils.pref.Pref;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * Created by Henra Setia Nugraha on 10/26/2017.
 */

public class GeneralNetworkManager extends BaseNetwork<ApiServices> {

    @Override
    protected String getBaseUrl() {
        return BuildConfig.BASEURL;
//        return "https://private-4143fb-testing463.apiary-mock.com/";
    }

    @Override
    protected Class<ApiServices> getRestClass() {
        return ApiServices.class;
    }

    @Override
    protected GsonBuilder gsonHandler(GsonBuilder builder) {
        builder.setLenient();
        return builder;
    }

    private String getAdminHeader() {
        return "bearer " + Pref.getPreference().getString(BuildConfig.ATKN);
    }

    private String getCustomerHeader(String storeCode) {
        return "bearer " + Pref.getPreference().getString(storeCode + BuildConfig.CTKN);
    }

    public Observable<String> requestToken(NetworkHandlerContract networkHandlerContract, String role, TokenEntity tokenEntity) {
        return addInterceptor(getServices().getToken(role, tokenEntity), networkHandlerContract);
    }

    public Observable<Response> request(NetworkHandlerContract networkHandlerContract) {
        return addInterceptor(getServices().getData(), networkHandlerContract);
    }

    public Observable<ProfileEntity> requestProfile(NetworkHandlerContract networkHandlerContract, String storeCode) {
        return addInterceptor(getServices().getProfile(getCustomerHeader(storeCode)), networkHandlerContract);
    }

    public Observable<ProfileEntity> updateProfile(NetworkHandlerContract networkHandlerContract, String storeCode, UpdateProfile profile) {
        return addInterceptor(getServices().updateProfile(getCustomerHeader(storeCode), profile), networkHandlerContract);
    }

    public Observable<Object> loginFB(NetworkHandlerContract networkHandlerContract, String tokenFB, String firstName, String lastName, String email) {
        return addInterceptor(getServices().loginFB(getAdminHeader(), tokenFB, firstName, lastName, email), networkHandlerContract);
    }

    public Observable<Object> loginG(NetworkHandlerContract networkHandlerContract, String tokenG, String firstName, String lastName, String email) {
        return addInterceptor(getServices().loginG(getAdminHeader(), tokenG, firstName, lastName, email), networkHandlerContract);
    }

    public Observable<CustomerReview> requestCustomerReview(NetworkHandlerContract networkHandlerContract, String storeCode) {
        return addInterceptor(getServices().getCustomerReview(getCustomerHeader(storeCode), storeCode), networkHandlerContract);
    }

    public Observable<VoucherList> requestVoucherList(NetworkHandlerContract networkHandlerContract, String storeCode) {
        return addInterceptor(getServices().getVoucherList(getCustomerHeader(storeCode), storeCode), networkHandlerContract);
    }

    public Observable<VoucherDetail> requestVoucher(NetworkHandlerContract networkHandlerContract, String storeCode, String voucherId) {
        return addInterceptor(getServices().getVoucher(getCustomerHeader(storeCode), storeCode, voucherId), networkHandlerContract);
    }

    public Observable<String> changeSubscription(NetworkHandlerContract networkHandlerContract, String state, String storeCode) {
        return addInterceptor(getServices().changeSubscription(getCustomerHeader(storeCode), state, storeCode), networkHandlerContract);
    }

    public Observable<Object> changePassword(NetworkHandlerContract networkHandlerContract, String storeCode, ChangePassword changePassword) {
        return addInterceptor(getServices().changePassword(getCustomerHeader(storeCode), changePassword), networkHandlerContract);
    }

    public Observable<Object> resetPassword(NetworkHandlerContract networkHandlerContract, ResetPassword resetPassword) {
        return addInterceptor(getServices().resetPassword(getAdminHeader(), resetPassword), networkHandlerContract);
    }

    public Observable<Boolean> isEmailAvailable(NetworkHandlerContract networkHandlerContract, IsEmailAvailable emailAvailable) {
        return addInterceptor(getServices().isEmailAvailable(getAdminHeader(), emailAvailable), networkHandlerContract);
    }

    public Observable<Subscription> getSubscription(NetworkHandlerContract networkHandlerContract, String storeCode) {
        return addInterceptor(getServices().getSubscription(getCustomerHeader(storeCode), storeCode), networkHandlerContract);
    }

    public Observable<ProductListEntity> requestProductList(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String catId,
            Map<String, String> filters) {
        return addInterceptor(getServices().getProductList(getAdminHeader(), catId, filters), networkHandlerContract);
    }

    public Observable<ProductListEntity> requestProductSearch(
            NetworkHandlerContract networkHandlerContract,
            String query,
            String storeCode,
            Map<String, String> filters) {
        return addInterceptor(getServices().getProductSearch(getAdminHeader(), storeCode, query, filters), networkHandlerContract);
    }

    public Observable<ProductListEntity> requestBestSelling(
            NetworkHandlerContract networkHandlerContract,
            String storeCode) {
        return addInterceptor(getServices().getBestSelling(getAdminHeader(), storeCode), networkHandlerContract);
    }

    public Observable<ProductListEntity> requestNewestProduct(
            NetworkHandlerContract networkHandlerContract,
            String storeCode) {
        return addInterceptor(getServices().getNewestProduct(getAdminHeader(), storeCode), networkHandlerContract);
    }

    public Observable<ProductListEntity> requestPromoProduct(
            NetworkHandlerContract networkHandlerContract,
            String storeCode) {
        return addInterceptor(getServices().getPromoProduct(getAdminHeader(), storeCode), networkHandlerContract);
    }

    public Observable<StaticBlock> requestStaticBlock(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String identifier) {
        return addInterceptor(getServices().getStaticBlock(getAdminHeader(), storeCode, identifier), networkHandlerContract);
    }

    public Observable<StaticCategory> requestStaticCategory(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String identifier) {
        return addInterceptor(getServices().getStaticCategory(getAdminHeader(), storeCode, identifier), networkHandlerContract);
    }

    public Observable<ProductEntity> requestProduct(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String id) {

        String header = "bearer " + Pref.getPreference().getString(BuildConfig.ATKN);
        return addInterceptor(getServices().getProduct(header, storeCode, id), networkHandlerContract);
    }

    public Observable<Suggestion> getSuggest(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String query) {
        return addInterceptor(getServices().getSuggest(storeCode, query), networkHandlerContract);
    }

    public Observable<List<BrandFilterEntity>> requestBrandFilter(
            NetworkHandlerContract networkHandlerContract) {
        return addInterceptor(getServices().getBrandFilter(getAdminHeader()), networkHandlerContract);
    }

    public Observable<ReviewEntity> requestProductReview(
            NetworkHandlerContract networkHandlerContract,
            String productId,
            Map<String, String> filter) {
        return addInterceptor(getServices().getProductReview(getAdminHeader(), productId, filter), networkHandlerContract);
    }

    public Observable<String> requestQuoteId(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            CartQuoteEntity customerId) {
        return addInterceptor(getServices().getQuoteId(getAdminHeader(), storeCode, customerId), networkHandlerContract);
    }

    public Observable<Object> addCart(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            AddCartBody cart) {
        return addInterceptor(getServices().addCart(getAdminHeader(), storeCode, cart), networkHandlerContract);
    }

    public Observable<CartListEntity> requestCartList(
            NetworkHandlerContract networkHandlerContract,
            String storeCode) {
        return addInterceptor(getServices().getCartList(getCustomerHeader(storeCode), storeCode), networkHandlerContract);
    }

    public Observable<Object> deleteItemCart(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String itemId) {
        return addInterceptor(getServices().deleteItemCart(getCustomerHeader(storeCode), storeCode, itemId), networkHandlerContract);
    }

    public Observable<Object> updateItemCart(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            int itemId,
            int quoteId,
            UpdateCartBody cart) {
        return addInterceptor(getServices().updateCart(getAdminHeader(), storeCode, itemId, quoteId, cart), networkHandlerContract);
    }

    public Observable<CartTotalEntity> requestTotalCart(
            NetworkHandlerContract networkHandlerContract,
            String storeCode) {
        return addInterceptor(getServices().getTotalCart(getCustomerHeader(storeCode), storeCode), networkHandlerContract);
    }

    public Observable<Boolean> updateVoucher(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String voucherCode) {
        return addInterceptor(getServices().updateVoucher(getCustomerHeader(storeCode), storeCode, voucherCode), networkHandlerContract);
    }

    public Observable<Boolean> deleteVoucher(
            NetworkHandlerContract networkHandlerContract,
            String storeCode) {
        return addInterceptor(getServices().deleteVoucher(getCustomerHeader(storeCode), storeCode), networkHandlerContract);
    }

    public Observable<List<ShippingMethodEntity>> requestShippingMethod(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            AddressBody addressBody) {
        return addInterceptor(getServices().getShippingMethod(getCustomerHeader(storeCode), storeCode, addressBody), networkHandlerContract);
    }

    public Observable<AvailablePaymentMethod> saveShippingMethod(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            AddressShippingBody addressBody) {
        return addInterceptor(getServices().saveShippingMethod(getCustomerHeader(storeCode), storeCode, addressBody), networkHandlerContract);
    }

    public Observable<String> savePaymentMethod(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            PaymentMethodBody paymentMethodBody) {
        return addInterceptor(getServices().savePaymentMethod(getCustomerHeader(storeCode), storeCode, paymentMethodBody), networkHandlerContract);
    }

    public Observable<PaymentDesc> getPaymentDesc(
            NetworkHandlerContract networkHandlerContract,
            String storeCode) {
        return addInterceptor(getServices().getPaymentDesc(getCustomerHeader(storeCode), storeCode), networkHandlerContract);
    }

    public Observable<Object> saveVa(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            VaBody vaBody) {
        return addInterceptor(getServices().saveVa(getCustomerHeader(storeCode), storeCode, vaBody), networkHandlerContract);
    }

    public Observable<Register> register(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            RegisterBody registerBody) {
        return addInterceptor(getServices().register(getCustomerHeader(storeCode), storeCode, registerBody), networkHandlerContract);
    }

    public Observable<OrderHistory> requestOrderList(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            Map<String, String> queries) {
        return addInterceptor(getServices().getOrderList(getAdminHeader(), queries), networkHandlerContract);
    }

    public Observable<OrderDetail> requestOrderDetail(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String orderId) {
        return addInterceptor(getServices().getOrderDetail(getCustomerHeader(storeCode), storeCode, orderId), networkHandlerContract);
    }

    public Observable<Object> addWishlist(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            AddWishlistBody wishlistBody) {
        return addInterceptor(getServices().addWishlist(getCustomerHeader(storeCode), storeCode, wishlistBody), networkHandlerContract);
    }

    public Observable<WishlistEntity> getWishlist(
            NetworkHandlerContract networkHandlerContract,
            String storeCode) {
        return addInterceptor(getServices().getWishlist(getCustomerHeader(storeCode), storeCode), networkHandlerContract);
    }

    public Observable<Object> deleteWishlist(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String itemId) {
        return addInterceptor(getServices().deleteWishlist(getCustomerHeader(storeCode), storeCode, itemId), networkHandlerContract);
    }

    public Observable<Object> addNewAddress(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            AddAddressBody addressBody) {
        return addInterceptor(getServices().saveNewAddress(getCustomerHeader(storeCode), storeCode, addressBody), networkHandlerContract);
    }

    public Observable<Object> updateAddress(
            NetworkHandlerContract networkHandlerContract,
            String addressId,
            String storeCode,
            AddAddressBody addressBody) {
        return addInterceptor(getServices().updateAddress(getCustomerHeader(storeCode), addressId, storeCode, addressBody), networkHandlerContract);
    }

    public Observable<Object> deleteAddress(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String addressId) {
        return addInterceptor(getServices().deleteAddress(getCustomerHeader(storeCode), storeCode, addressId), networkHandlerContract);
    }

    public Observable<RegionEntity> getRegion(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String countryId) {
        return addInterceptor(getServices().getRegion(getCustomerHeader(storeCode), storeCode, countryId), networkHandlerContract);
    }

    public Observable<CityEntity> getCity(
            NetworkHandlerContract networkHandlerContract,
            String storeCode,
            String regionId) {
        return addInterceptor(getServices().getCity(getCustomerHeader(storeCode), storeCode, regionId), networkHandlerContract);
    }

    public Observable<AppointmentEntity> getAvailable(
            NetworkHandlerContract networkHandlerContract,
            String productId,
            Map<String, String> query) {
        return addInterceptor(getServices().getAvailable(productId, query), networkHandlerContract);
    }
}
