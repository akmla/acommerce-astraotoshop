package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.RelativeLayout;


/**
 * Created by Henra Setia Nugraha on 11/30/2017.
 */

public class RelativeCheckedView extends RelativeLayout implements Checkable {

    public static final int[] CHECKED_STATE = {android.R.attr.state_checked};
    private boolean mChecked;

    public RelativeCheckedView(Context context) {
        super(context);
    }

    public RelativeCheckedView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RelativeCheckedView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        refreshDrawableState();
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        mChecked = !mChecked;
        refreshDrawableState();
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        int[] states = new int[0];
        try {
            states = super.onCreateDrawableState(extraSpace + 1);
            if (mChecked) {
                mergeDrawableStates(states, CHECKED_STATE);
            }
            return states;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return states;
    }
}
