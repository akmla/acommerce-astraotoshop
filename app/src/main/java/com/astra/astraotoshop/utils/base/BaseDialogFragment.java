package com.astra.astraotoshop.utils.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.astra.astraotoshop.R;

/**
 * Created by Henra Setia Nugraha on 12/30/2017.
 */

public class BaseDialogFragment extends DialogFragment {
    protected static final String STATE = "state";
    private int state = -11;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        state = getArguments().getInt(STATE, -11);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE, state);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            state = savedInstanceState.getInt(STATE);
        }
    }

    protected int getState() {
        return state;
    }

    protected boolean isProductService() {
        return getState() == getResources().getInteger(R.integer.state_product_service);
    }

    protected String getStoreCode() {
        if (getState() > -1)
            return getResources().getStringArray(R.array.storeCode)[getState()];
        else return getResources().getStringArray(R.array.storeCode)[0];
    }
}
