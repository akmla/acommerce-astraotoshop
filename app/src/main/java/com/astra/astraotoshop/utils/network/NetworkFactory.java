package com.astra.astraotoshop.utils.network;

import com.astra.astraotoshop.utils.network.handler.ClientSSL;
import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Henra Setia Nugraha on 10/25/2017.
 * network pattern by : https://github.com/mexanjuadha/CodelabsEvent
 * do not delete this comment
 */

public class NetworkFactory {

    public static <T> T createRestAdapter(Gson gson, String baseUrl, Class<T> restClass) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(ClientSSL.getClientWithSSL());

        return builder.build().create(restClass);
    }

}
