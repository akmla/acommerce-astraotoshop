package com.astra.astraotoshop.product.catalog.product;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.base.BaseRecyclerAdapter;
import com.astra.astraotoshop.utils.view.FontIcon;
import com.astra.astraotoshop.utils.view.ImageSquared;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 12/19/2017.
 */

public class ProductAdapter extends BaseRecyclerAdapter<ProductAdapter.ProductViewHolder> {

    //    ProductServiceAdapter.ItemClickListener listener;
    private final String GROCERY = "Harga Grosir";
    private ListListener listener;
    private List<ProductItemEntity> products;
    private Context context;
    private int skuVisibility;
    private int showAddToCart;

    public ProductAdapter(ListListener listener, Context context, int skuVisibility, int showAddToCart) {
        this.listener = listener;
        this.context = context;
        this.skuVisibility = skuVisibility;
        this.showAddToCart = showAddToCart;
        products = new ArrayList<>();
    }

    public ProductAdapter(ListListener listener, Context context, int skuVisibility) {
        this.listener = listener;
        this.context = context;
        this.skuVisibility = skuVisibility;
        products = new ArrayList<>();
    }

    @Override
    protected ProductViewHolder viewHolder(ViewGroup parent, int viewType) {
        return new ProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product2, parent, false));
    }

    @Override
    protected void bind(ProductViewHolder holder, int position) {
        Glide.with(context).load(products.get(position).getImage()).into(holder.image);
        holder.name.setText(products.get(position).getName());
        holder.sku.setText(products.get(position).getSku());
        holder.sku.setVisibility(skuVisibility);
        holder.iconAddCart.setVisibility(showAddToCart);
        holder.addCart.setVisibility(showAddToCart);

        if (products.get(position).getSpecialPrice() != null) {
            StringFormat.applyCurrencyFormat(holder.firstPrice, products.get(position).getSpecialPrice());
            StringFormat.applyCurrencyFormat(holder.secondPrice, products.get(position).getPrice());
            holder.secondPrice.setVisibility(View.VISIBLE);

            holder.discount.setText(
                    discountCalculate(products.get(position).getPrice(), products.get(position).getSpecialPrice()));
            holder.discount.setVisibility(View.VISIBLE);
        } else {
            StringFormat.applyCurrencyFormat(holder.firstPrice, products.get(position).getPrice());
            holder.secondPrice.setVisibility(View.GONE);
            holder.discount.setVisibility(View.GONE);
        }

        if (products.get(position).getTierPrice() != null) {
            setText(holder.grocery, GROCERY
                    + StringFormat.addCurrencyFormat(products.get(position).getTierPrice().get(
                    products.get(position).getTierPrice().size() - 1
            ).getPrice()), true);
        }

        if (products.get(position).getRating() > 0) {
            holder.review.setRating(Float.parseFloat(String.valueOf(products.get(position).getRating())));
        }

        try {
            if (Integer.valueOf(StringFormat.clearCurrencyFormat(products.get(position).getQty())) == 0) {
                holder.addCart.setTextColor(context.getResources().getColor(android.R.color.darker_gray));
                holder.iconAddCart.setTextColor(context.getResources().getColor(android.R.color.darker_gray));
            } else {
                holder.addCart.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                holder.iconAddCart.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String discountCalculate(String price, String specialPrice) {
        Double dPrice = Double.parseDouble(price);
        Double dSpecialPrice = Double.parseDouble(specialPrice);

        double cutPrice = (dPrice - dSpecialPrice) / dPrice;
        int discount = (int) Math.ceil(cutPrice * 100);
        return String.valueOf(discount + "%");
    }

    private void setText(TextView view, String value, boolean nullHide) {
        if (value != null) {
            view.setText(value);
            view.setVisibility(View.VISIBLE);
        } else if (nullHide) {
            view.setVisibility(View.GONE);
        }
    }

    @Override
    protected int itemCount() {
        return products.size();
    }

    public void addProducts(List<ProductItemEntity> products, boolean addMore) {
        if (addMore) {
            int last = this.products.size() - 1;
            this.products.addAll(products);
            notifyItemRangeInserted(last, products.size());
        } else {
            this.products.clear();
            this.products = products;
            notifyDataSetChanged();
        }
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageSquared image;
        @BindView(R.id.discount)
        TextView discount;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.sku)
        TextView sku;
        @BindView(R.id.firstPrice)
        TextView firstPrice;
        @BindView(R.id.secondPrice)
        TextView secondPrice;
        @BindView(R.id.grocery)
        TextView grocery;
        @BindView(R.id.review)
        RatingBar review;
        @BindView(R.id.addCart)
        TextView addCart;
        @BindView(R.id.iconAddCart)
        FontIcon iconAddCart;

        ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.addCart)
        public void addCart() {
            try {
                listener.onItemClick(R.id.buy, getAdapterPosition(), products.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.container)
        public void itemClick() {
            try {
                listener.onItemClick(R.id.container, getAdapterPosition(), products.get(getAdapterPosition()).getEntityId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
