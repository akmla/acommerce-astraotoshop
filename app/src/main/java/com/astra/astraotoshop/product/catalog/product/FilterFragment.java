package com.astra.astraotoshop.product.catalog.product;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.appyvet.materialrangebar.RangeBar;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.provider.BrandEntity;
import com.astra.astraotoshop.product.catalog.provider.PriceEntity;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FilterFragment extends Fragment implements RangeBar.OnRangeBarChangeListener {

    @BindView(R.id.sortChips)
    ChipCloud sortChips;
    @BindView(R.id.minPrice)
    CustomFontFace minPrice;
    @BindView(R.id.maxPrice)
    CustomFontFace maxPrice;
    @BindView(R.id.brandChips)
    ChipCloud brandChips;
    Unbinder unbinder;
    @BindView(R.id.priceSlider)
    RangeBar priceSlider;
    FilterStateModel stateModel;
    private List<BrandEntity> brands;
    private List<Integer> tempBrand = new ArrayList<>();
    private int tempSort = 0;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_filter_revamp, container, false);
        unbinder = ButterKnife.bind(this, view);
        setChipListener();
        priceSlider.setOnRangeBarChangeListener(this);
        stateModel = new FilterStateModel();
        return view;
    }

    private void setChipListener() {
        sortChips.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int i) {
                tempSort = i;
            }

            @Override
            public void chipDeselected(int i) {
                System.out.println();
            }
        });

        brandChips.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int i) {
                if (!tempBrand.contains(i))
                    tempBrand.add(i);
            }

            @Override
            public void chipDeselected(int i) {
                if (tempBrand.contains(i)) {
                    tempBrand.remove(Integer.valueOf(i));
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        sortChips.setSelectedChip(0);
    }

    public void setBrands(List<BrandEntity> brands) {
        this.brands = brands;
        for (BrandEntity data : brands) {
            brandChips.addChip(data.getLabel());
        }
    }

    public void setPriceRange(PriceEntity price) {
        minPrice.setText(StringFormat.addCurrencyFormat(price.getMin()));
        maxPrice.setText(StringFormat.addCurrencyFormat(price.getMax()));
//        priceSlider.setTickStart(Float.parseFloat(String.valueOf(price.getMin())));
//        priceSlider.setTickEnd(Float.parseFloat(price.getMax()));
        priceSlider.setTickInterval(50000);
    }

    public void setStateModel(FilterStateModel stateModel) {
        if (stateModel != null) {
            this.stateModel = stateModel;
            updateUI();
        }
    }

    private void updateUI() {
        tempSort = 0;
        tempBrand = new ArrayList<>();
        if (brands != null && stateModel != null) {
            brandChips.removeAllViews();
            for (int i = 0; i < brands.size(); i++) {
                brandChips.addChip(brands.get(i).getLabel());
                if (stateModel.getBrandIndex().contains(i)){
                    brandChips.setSelectedChip(i);
                    tempBrand.add(i);
                }
            }
            sortChips.setSelectedChip(stateModel.getSortKey());
        }
    }

    @Override
    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
        minPrice.setText(StringFormat.addCurrencyFormat(leftPinValue));
        maxPrice.setText(StringFormat.addCurrencyFormat(rightPinValue));
    }

    @OnClick(R.id.applyFilter)
    public void onApplyClick() {
        stateModel.setSortKey(tempSort);
        stateModel.setBrandIndex(tempBrand);
        try {
            ((ProductsActivity) getActivity()).saveState(stateModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.resetFilter)
    public void onResetClick() {
        try {
            for (int i = 0; i < brands.size(); i++)
                brandChips.chipDeselected(i);
            ((ProductsActivity) getActivity()).setReset();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
