package com.astra.astraotoshop.product.product.provider;

import android.text.TextUtils;

import com.astra.astraotoshop.order.cart.provider.AppointmentParamItem;
import com.astra.astraotoshop.order.cart.provider.AppointmentsItemCart;
import com.astra.astraotoshop.order.cart.provider.CartListItem;
import com.astra.astraotoshop.order.cart.provider.ExtensionAttributes;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/10/2018.
 */

public class ProductDetailPresenter implements ProductDetailContract.ProductDetailPresenter {

    private ProductDetailContract.ProductDetailInteractor interactor;
    private ProductDetailContract.ProductDetailView view;
    private ProductEntity productEntity;
    private CartListItem cartListItem;

    public ProductDetailPresenter(ProductDetailContract.ProductDetailView view) {
        this.view = view;
        interactor = new ProductDetailInteractor(null, this);
    }

    @Override
    public void requestProduct(String storeCode, String id) {
        if (!TextUtils.isEmpty(id)) {
            interactor.requestProduct(new NetworkHandler(), storeCode, id);
        }
    }

    @Override
    public void onProductReceived(ProductEntity product) {
        if (product != null) {
            productEntity = product;
            view.setProduct(product);
        }
    }

    @Override
    public void onFailedReceivingProduct(Throwable throwable) {
        view.showError();
    }

    @Override
    public ProductEntity getProduct() {
        return productEntity;
    }

    @Override
    public List<AppointmentsItemCart> getAppointment(List<CartListItem> items, String sku) {
        if (cartListItem != null) {
            if (cartListItem.getExtensionAttributes() != null) {
                return cartListItem.getExtensionAttributes().getAppointmentsItemCart();
            }
        }

        List<AppointmentsItemCart> list = new ArrayList<>();
        for (CartListItem item : items) {
            if (String.valueOf(item.getSku()).equals(sku)) {
                list = item.getExtensionAttributes().getAppointmentsItemCart();
                cartListItem = item;
                break;
            }
        }
        return list;
    }

    @Override
    public void addAppointmentData(AppointmentsItemCart item) {
        if (cartListItem == null) {
            cartListItem = new CartListItem();
            cartListItem.setExtensionAttributes(new ExtensionAttributes(Collections.singletonList(new AppointmentsItemCart())));
        }

        if (item != null) {
            if (cartListItem.getExtensionAttributes().getAppointmentsItemCart().size() > 0) {
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getRegion().add(item.getRegion().get(0));
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getCity().add(item.getCity().get(0));
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getAddress().add(item.getAddress().get(0));
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getApDate().add(item.getApDate().get(0));
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getApHours().add(item.getApHours().get(0));
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getSlotName().add(item.getSlotName().get(0));
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getWhsId().add(item.getWhsId().get(0));
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getMechanicComeToHome().add(item.getMechanicComeToHome().get(0));
            } else {
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().add(item);
            }
        }
    }

    @Override
    public void editAppointment(int position, AppointmentsItemCart item) {
        cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getRegion().set(position, item.getRegion().get(0));
        cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getCity().set(position, item.getCity().get(0));
        cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getAddress().set(position, item.getAddress().get(0));
        cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getSlotName().set(position, item.getSlotName().get(0));
        cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getWhsId().set(position, item.getWhsId().get(0));
        cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getApHours().set(position, item.getApHours().get(0));
        cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getApDate().set(position, item.getApDate().get(0));
        cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getMechanicComeToHome().set(position, item.getMechanicComeToHome().get(0));
    }

    @Override
    public void prepareAppointmentData() {
        List<AppointmentParamItem> paramItems = new ArrayList<>();
        AppointmentParamItem paramItem;
        for (int i = 0; i < cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getCity().size(); i++) {
            paramItem = new AppointmentParamItem(
                    cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getAddress().get(i),
                    cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getCity().get(i),
                    cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getWhsId().get(i),
                    0,
                    cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getApHours().get(i),
                    cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getApDate().get(i),
                    cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getMechanicComeToHome().get(i),
                    cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getRegion().get(i),
                    cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getSlotName().get(i)
            );
            paramItems.add(paramItem);
        }
        view.appointmentComplete(paramItems);
    }

    @Override
    public void deleteItem(int position) {
        if (cartListItem != null) {
            if (cartListItem.getExtensionAttributes().getAppointmentsItemCart().size() > 0) {
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getRegion().remove(position);
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getCity().remove(position);
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getAddress().remove(position);
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getApHours().remove(position);
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getApDate().remove(position);
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getSlotName().remove(position);
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getWhsId().remove(position);
                cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getMechanicComeToHome().remove(position);
            }
        }
    }

    @Override
    public CartListItem getCartItem(String sku, int itemId, int quoteId) {
        cartListItem.setSku(sku);
        cartListItem.setItemId(itemId);
        cartListItem.setQuoteId(quoteId);
        cartListItem.setQty(cartListItem.getExtensionAttributes().getAppointmentsItemCart().get(0).getAddress().size());
        return cartListItem;
    }

    @Override
    public void clearAppointment() {
        if (cartListItem != null) {
            cartListItem.getExtensionAttributes().setAppointmentsItemCart(Collections.singletonList(new AppointmentsItemCart()));
        }
    }
}
