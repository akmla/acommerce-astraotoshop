package com.astra.astraotoshop.product.search.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProdSuggest {

    @SerializedName("totalItems")
    private int totalItems;

    @SerializedName("indices")
    private List<IndicesItem> indices;

    @SerializedName("cache")
    private boolean cache;

    @SerializedName("textEmpty")
    private String textEmpty;

    @SerializedName("success")
    private boolean success;

    @SerializedName("query")
    private String query;

    @SerializedName("noResults")
    private boolean noResults;

    @SerializedName("textAll")
    private String textAll;

    @SerializedName("time")
    private double time;

    @SerializedName("urlAll")
    private String urlAll;

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setIndices(List<IndicesItem> indices) {
        this.indices = indices;
    }

    public List<IndicesItem> getIndices() {
        return indices;
    }

    public void setCache(boolean cache) {
        this.cache = cache;
    }

    public boolean isCache() {
        return cache;
    }

    public void setTextEmpty(String textEmpty) {
        this.textEmpty = textEmpty;
    }

    public String getTextEmpty() {
        return textEmpty;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public void setNoResults(boolean noResults) {
        this.noResults = noResults;
    }

    public boolean isNoResults() {
        return noResults;
    }

    public void setTextAll(String textAll) {
        this.textAll = textAll;
    }

    public String getTextAll() {
        return textAll;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getTime() {
        return time;
    }

    public void setUrlAll(String urlAll) {
        this.urlAll = urlAll;
    }

    public String getUrlAll() {
        return urlAll;
    }

    @Override
    public String toString() {
        return
                "ProdSuggest{" +
                        "totalItems = '" + totalItems + '\'' +
                        ",indices = '" + indices + '\'' +
                        ",cache = '" + cache + '\'' +
                        ",textEmpty = '" + textEmpty + '\'' +
                        ",success = '" + success + '\'' +
                        ",query = '" + query + '\'' +
                        ",noResults = '" + noResults + '\'' +
                        ",textAll = '" + textAll + '\'' +
                        ",time = '" + time + '\'' +
                        ",urlAll = '" + urlAll + '\'' +
                        "}";
    }
}