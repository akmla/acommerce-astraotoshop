package com.astra.astraotoshop.product.filter.provider;

import com.astra.astraotoshop.product.catalog.provider.ProductContract;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/14/2018.
 */

public interface FilterContract {
    interface FilterView{
        void setBrand(List<BrandFilterEntity> brands);
        void showError();
        void onFilterApply(List<String >brandValues,List<BrandFilterEntity> brands);
    }

    interface FilterPresenter{
        void requestBrand();
        void onBrandsReceived(List<BrandFilterEntity> brands);
        void onFailedReceivingBrands(Throwable error);

        void injectProductPresenter(ProductContract.ProductListPresenter presenter);
        void updateFilter();
        void updateFilter(String key,String value);
        void removeFilterItem(String key);
        void attachView(FilterView filterView);
        void resetFilter();

        int[] getPriceState();
        void updatePriceState(int[] price);
        List<BrandFilterEntity> getBrands();
        void updateBrands(List<BrandFilterEntity> brands);
        int getSortState();
        void updateSortState(int position);
        void updateBrandValue(List<String> brandValue);
        void removeBrandValue(BrandFilterEntity brand);
        List<String> getBrandValue();

    }

    interface FilterInteractor{
        void requestBrand();
    }
}
