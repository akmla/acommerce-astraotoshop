package com.astra.astraotoshop.product.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.search.provider.ItemsItem;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Henra Setia Nugraha on 6/11/2018.
 */

public class ProductSuggestAdapter extends RecyclerView.Adapter<ProductSuggestAdapter.ProductSuggestViewHolder> {

    private List<ItemsItem> suggestItem;
    private Context context;
    private ListListener listener;

    public ProductSuggestAdapter(Context context, ListListener listener) {
        this.context = context;
        this.listener = listener;
        suggestItem = new ArrayList<>();
    }

    @Override
    public ProductSuggestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductSuggestViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(ProductSuggestViewHolder holder, int position) {
        Glide.with(context).load(suggestItem.get(position).getImage()).into(holder.suggestProductImage);
        StringFormat.setText(holder.suggestProductTitle, suggestItem.get(position).getName());
        StringFormat.setText(holder.suggestProductDesc, suggestItem.get(position).getDescription());
        StringFormat.applyCurrencyFormat(holder.suggestProductPrice, suggestItem.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return suggestItem.size();
    }

    public void setSuggestItem(List<ItemsItem> suggestItem) {
        this.suggestItem = suggestItem;
        notifyDataSetChanged();
    }

    class ProductSuggestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.suggestProductImage)
        CircleImageView suggestProductImage;
        @BindView(R.id.suggestProductTitle)
        TextView suggestProductTitle;
        @BindView(R.id.suggestProductDesc)
        TextView suggestProductDesc;
        @BindView(R.id.suggestProductPrice)
        CustomFontFace suggestProductPrice;

        public ProductSuggestViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                listener.onItemClick(0, getAdapterPosition(), suggestItem.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
