package com.astra.astraotoshop.product.category.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.category.provider.CategoryContract;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.product.category.provider.SubCatPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/17/2017.
 */

public class SubCategoryFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final java.lang.String DATA_KEY = "data";
    @BindView(R.id.subCategoryList)
    ListView subCategoryList;
    Unbinder unbinder;
    private CategoryContract.CategoryView listener;
    private CategoryContract.SubCatPresenter presenter;
    private SubCategoryAdapter adapter;

    public static SubCategoryFragment newInstance(CategoryEntity category) {

        Bundle args = new Bundle();
        args.putParcelable(DATA_KEY, category);
        SubCategoryFragment fragment = new SubCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void addListener(CategoryContract.CategoryView listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SubCatPresenter();
        presenter.collectIntentData(getArguments().getParcelable(DATA_KEY));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_category, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    //TODO save data to savedInstance
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new SubCategoryAdapter(getContext(), presenter.getCategories());
        subCategoryList.setAdapter(adapter);
        subCategoryList.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        try {
            listener.onItemSelect(
                    presenter.getCategoryByPosition(position),
                    presenter.getChildTitle(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getTitle(){
        return presenter.getTitle();
    }

    public void updateList(CategoryEntity categories) {
        presenter.collectIntentData(categories);
        adapter.setCategories(presenter.getCategories());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
