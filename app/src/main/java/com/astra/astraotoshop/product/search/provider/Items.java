package com.astra.astraotoshop.product.search.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Items {

    @SerializedName("text_suggest")
    private List<TextSuggestItem> textSuggest;

    @SerializedName("prod_suggest")
    private ProdSuggest prodSuggest;

    public void setTextSuggest(List<TextSuggestItem> textSuggest) {
        this.textSuggest = textSuggest;
    }

    public List<TextSuggestItem> getTextSuggest() {
        return textSuggest;
    }

    public void setProdSuggest(ProdSuggest prodSuggest) {
        this.prodSuggest = prodSuggest;
    }

    public ProdSuggest getProdSuggest() {
        return prodSuggest;
    }

    @Override
    public String toString() {
        return
                "Items{" +
                        "text_suggest = '" + textSuggest + '\'' +
                        ",prod_suggest = '" + prodSuggest + '\'' +
                        "}";
    }
}