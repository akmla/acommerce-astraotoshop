package com.astra.astraotoshop.product.filter.provider;

import com.astra.astraotoshop.product.catalog.provider.ProductContract;
import com.astra.astraotoshop.product.catalog.provider.ProductListPresenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Henra Setia Nugraha on 1/14/2018.
 */

public class FilterPresenter implements FilterContract.FilterPresenter {

    private FilterContract.FilterView view;
    private FilterContract.FilterInteractor interactor;
    private List<BrandFilterEntity> brands;
    private ProductContract.ProductListPresenter productListPresenter;
    private Map<String, String> tempFilters;
    private int sortPosition = 0;
    private int[] priceState = new int[2];
    private List<String> brandValue;

    public FilterPresenter(FilterContract.FilterView view) {
        this(view, null);
    }

    public FilterPresenter(FilterContract.FilterView view, ProductContract.ProductListPresenter productListPresenter) {
        this.view = view;
        this.productListPresenter = productListPresenter;
        if (productListPresenter != null)
            tempFilters = productListPresenter.getFilters();
        interactor = new FilterInteractor(this);
        brands = new ArrayList<>();
    }

    @Override
    public void requestBrand() {
        interactor.requestBrand();
    }

    @Override
    public void onBrandsReceived(List<BrandFilterEntity> brands) {
        if (brands.size() > 0) {
            this.brands = brands;
            view.setBrand(brands);
        }
    }

    @Override
    public void onFailedReceivingBrands(Throwable error) {
        System.out.println();
    }

    @Override
    public void injectProductPresenter(ProductContract.ProductListPresenter presenter) {
        productListPresenter = presenter;
        tempFilters = productListPresenter.getFilters();
    }

    @Override
    public void updateFilter() {
        productListPresenter.setFilters(tempFilters);
    }

    @Override
    public void updateFilter(String key, String value) {
        try {
            tempFilters.put(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeFilterItem(String key) {
        try {
            tempFilters.remove(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void attachView(FilterContract.FilterView filterView) {
        view = filterView;
    }

    @Override
    public void resetFilter() {
        tempFilters = ProductListPresenter.getDefaultFilter();
    }

    @Override
    public List<BrandFilterEntity> getBrands() {
        if (brands.size() <= 0) requestBrand();
        return brands;
    }

    @Override
    public int getSortState() {
        return sortPosition;
    }

    @Override
    public void updateBrands(List<BrandFilterEntity> brands) {
        this.brands = brands;
    }

    @Override
    public void updatePriceState(int[] price) {
        this.priceState = price;
    }

    @Override
    public void updateSortState(int position) {
        sortPosition = position;
    }

    @Override
    public void updateBrandValue(List<String> brandValue) {
        this.brandValue = brandValue;
    }

    @Override
    public void removeBrandValue(BrandFilterEntity brand) {
        brandValue.remove(brand.getValue());
        for (BrandFilterEntity item : brands) {
            if (item.getValue().equals(brand.getValue())) {
                brand.setSelected(false);
            }
        }
    }

    @Override
    public List<String> getBrandValue() {
        return brandValue != null ? brandValue : new ArrayList<>();
    }

    @Override
    public int[] getPriceState() {
        return priceState;
    }
}
