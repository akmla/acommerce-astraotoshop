package com.astra.astraotoshop.product.search;

import android.content.Context;

import com.astra.astraotoshop.provider.model.Response;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

/**
 * Created by Henra Setia Nugraha on 11/3/2017.
 */

public interface Contracts {
    interface View{
        void onResult(Response response);
    }

    interface Interactor{
        void request(NetworkHandler networkHandler);
    }
}
