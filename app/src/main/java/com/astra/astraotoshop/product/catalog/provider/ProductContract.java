package com.astra.astraotoshop.product.catalog.provider;

import com.astra.astraotoshop.product.catalog.product.FilterStateModel;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.List;
import java.util.Map;

/**
 * Created by Henra Setia Nugraha on 1/8/2018.
 */

public interface ProductContract {
    interface ProductList{
        void setProductList(List<ProductItemEntity> productList, boolean isAddMore, int totalCount);
        void showError();
        void onFilterApply(Map<String, String> presenter);

        void setFilterData(List<BrandEntity> brands, PriceEntity price);
    }

    interface ProductListPresenter{
        void updateFilter(String key,String value);
        void deleteFilterItem(String key);
        void resetFilter();
        Map<String,String> getFilters();
        void setFilters(Map<String,String> filters);
        boolean isAddMore();
        void requestProductList(String storeCode, String catId);
        void onProductListReceived(ProductListEntity productList);
        void onFailedReceivingProduct(Throwable error);
        void setFilter(Map<String ,String > filters);

        void setFilterState(FilterStateModel stateModel);

        FilterStateModel getStateModel();

        void setResetState();
    }

    interface ProductListInteractor{
        void requestProductList(
                NetworkHandler networkHandler,
                String storeCode,
                String catId, Map<String, String> filters);
    }
}
