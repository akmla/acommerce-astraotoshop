package com.astra.astraotoshop.product.search;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.search.provider.TextSuggestItem;
import com.astra.astraotoshop.utils.view.CustomFontFace;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 6/22/2018.
 */

public class PopularSuggestionAdapter extends RecyclerView.Adapter<PopularSuggestionAdapter.PopularSuggestionViewHolder> {

    private List<TextSuggestItem> textSuggestItems;
    private ListListener listener;

    public PopularSuggestionAdapter(ListListener listener) {
        this.listener = listener;
        textSuggestItems = new ArrayList<>();
    }

    @Override
    public PopularSuggestionAdapter.PopularSuggestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PopularSuggestionViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_search_popular, parent, false
        ));
    }

    @Override
    public void onBindViewHolder(PopularSuggestionAdapter.PopularSuggestionViewHolder holder, int position) {
        holder.textView.setText(textSuggestItems.get(position).getTitle());
        holder.count.setText(textSuggestItems.get(position).getNumResults());
    }

    @Override
    public int getItemCount() {
        return textSuggestItems.size();
    }

    public void setTextSuggestItems(List<TextSuggestItem> textSuggestItems) {
        this.textSuggestItems = textSuggestItems;
        notifyDataSetChanged();
    }

    class PopularSuggestionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.suggest)
        CustomFontFace textView;
        @BindView(R.id.productCount)
        TextView count;

        public PopularSuggestionViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                listener.onItemClick(android.R.id.text1, getAdapterPosition(), textSuggestItems.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
