package com.astra.astraotoshop.product.search;

import com.astra.astraotoshop.product.search.provider.Suggestion;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 6/8/2018.
 */

public interface SearchContract {
    interface SearchView{
        void showSuggest(Suggestion suggestion);
        void showError();
    }

    interface SearchPresenter{
        void search(String query);
        void onSuggestReceived(Suggestion suggestion);
        void onSuggestError(Throwable throwable);
        void addRecent(String value);
        void deleteRecent();
        List<String> loadRecent();
    }

    interface SearchInteractor{
        void search(String storeCode, String query);
    }
}
