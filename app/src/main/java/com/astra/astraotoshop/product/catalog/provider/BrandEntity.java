package com.astra.astraotoshop.product.catalog.provider;

import com.google.gson.annotations.SerializedName;

public class BrandEntity{

	@SerializedName("label")
	private String label;

	@SerializedName("value")
	private String value;

	public void setLabel(String label){
		this.label = label;
	}

	public String getLabel(){
		return label;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}
}