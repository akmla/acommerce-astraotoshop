package com.astra.astraotoshop.product.catalog.product;

import java.util.ArrayList;
import java.util.List;

public class FilterStateModel {
    private int sortKey=0;
    private String sortValue;
    private int minPriceKey;
    private String minPriceValue;
    private int maxPriceKey;
    private String maxPriceValue;
    private List<Integer> brandIndex = new ArrayList<>();
    private List<Integer> brandKey = new ArrayList<>();

    public int getSortKey() {
        return sortKey;
    }

    public void setSortKey(int sortKey) {
        this.sortKey = sortKey;
    }

    public String getSortValue() {
        return sortValue;
    }

    public void setSortValue(String sortValue) {
        this.sortValue = sortValue;
    }

    public int getMinPriceKey() {
        return minPriceKey;
    }

    public void setMinPriceKey(int minPriceKey) {
        this.minPriceKey = minPriceKey;
    }

    public String getMinPriceValue() {
        return minPriceValue;
    }

    public void setMinPriceValue(String minPriceValue) {
        this.minPriceValue = minPriceValue;
    }

    public int getMaxPriceKey() {
        return maxPriceKey;
    }

    public void setMaxPriceKey(int maxPriceKey) {
        this.maxPriceKey = maxPriceKey;
    }

    public String getMaxPriceValue() {
        return maxPriceValue;
    }

    public void setMaxPriceValue(String maxPriceValue) {
        this.maxPriceValue = maxPriceValue;
    }

    public List<Integer> getBrandIndex() {
        return brandIndex;
    }

    public void setBrandIndex(List<Integer> brandIndex) {
        this.brandIndex = brandIndex;
    }

    public List<Integer> getBrandKey() {
        return brandKey;
    }

    public void setBrandKey(List<Integer> brandKey) {
        this.brandKey = brandKey;
    }
}
