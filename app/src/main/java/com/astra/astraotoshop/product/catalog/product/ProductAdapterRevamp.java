package com.astra.astraotoshop.product.catalog.product;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.base.BaseRecyclerAdapter;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.ImageSquared;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 12/19/2017.
 */

public class ProductAdapterRevamp extends BaseRecyclerAdapter<ProductAdapterRevamp.ProductViewHolder> {

    private ListListener listener;
    private List<ProductItemEntity> products;

    public ProductAdapterRevamp(ListListener listener) {
        this.listener = listener;
        products = new ArrayList<>();
    }

    @Override
    protected ProductViewHolder viewHolder(ViewGroup parent, int viewType) {
        return new ProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_revamp, parent, false));
    }

    @Override
    protected void bind(ProductViewHolder holder, int position) {
        setText(holder.pruductName, products.get(position).getName(), false);
        if (products.get(position).getSpecialPrice() != null && !products.get(position).getSpecialPrice().equals("")) {
            setColor(holder.price, R.color.mainRed);
            StringFormat.applyCurrencyFormat(holder.specialPrice, products.get(position).getSpecialPrice());
            holder.specialPrice.setVisibility(View.VISIBLE);
            holder.discount.setText(discountCalculate(products.get(position).getPrice(),products.get(position).getSpecialPrice()));
            holder.discountContainer.setVisibility(View.VISIBLE);
        } else {
            setColor(holder.price, android.R.color.black);
            holder.discountContainer.setVisibility(View.GONE);
            holder.specialPrice.setVisibility(View.GONE);
        }

        StringFormat.applyCurrencyFormat(holder.price, products.get(position).getPrice());
        Glide.with(holder.pruductImage.getContext()).load(products.get(position).getImage()).into(holder.pruductImage);
    }

    private void setColor(View view, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            ((TextView) view).setTextColor(view.getContext().getColor(color));
        else
            ((TextView) view).setTextColor(view.getContext().getResources().getColor(color));
    }

    private String discountCalculate(String price, String specialPrice) {
        Double dPrice = Double.parseDouble(price);
        Double dSpecialPrice = Double.parseDouble(specialPrice);

        double cutPrice = (dPrice - dSpecialPrice) / dPrice;
        int discount = (int) Math.ceil(cutPrice * 100);
        return String.valueOf(discount + "%");
    }

    private void setText(TextView view, String value, boolean nullHide) {
        if (value != null) {
            view.setText(value);
            view.setVisibility(View.VISIBLE);
        } else if (nullHide) {
            view.setVisibility(View.GONE);
        }
    }

    @Override
    protected int itemCount() {
        return products.size();
    }

    public void addProducts(List<ProductItemEntity> products, boolean isAddMore) {
        if (isAddMore) {
            int last = this.products.size() - 1;
            this.products.addAll(products);
            notifyItemRangeInserted(last, products.size());
        } else {
            this.products.clear();
            this.products = products;
            notifyDataSetChanged();
        }
    }


    class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.productImage)
        ImageSquared pruductImage;
        @BindView(R.id.pruductName)
        CustomFontFace pruductName;
        @BindView(R.id.btnBuy)
        CustomFontFace btnBuy;
        @BindView(R.id.price)
        CustomFontFace price;
        @BindView(R.id.specialPrice)
        CustomFontFace specialPrice;
        @BindView(R.id.discount)
        CustomFontFace discount;
        @BindView(R.id.discountContainer)
        LinearLayout discountContainer;

        ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @OnClick(R.id.btnBuy)
        public void onBtnBuyClick() {
            listener.onItemClick(R.id.btnBuy, getAdapterPosition(), products.get(getAdapterPosition()));
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(0, getAdapterPosition(), products.get(getAdapterPosition()));
        }
    }
}
