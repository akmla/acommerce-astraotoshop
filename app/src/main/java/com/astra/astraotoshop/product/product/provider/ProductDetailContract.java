package com.astra.astraotoshop.product.product.provider;

import com.astra.astraotoshop.order.cart.provider.AppointmentParamItem;
import com.astra.astraotoshop.order.cart.provider.AppointmentsItemCart;
import com.astra.astraotoshop.order.cart.provider.CartListItem;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/10/2018.
 */

public interface ProductDetailContract {
    interface ProductDetailView {
        void setProduct(ProductEntity product);
        void showError();
        void appointmentComplete(List<AppointmentParamItem> paramItems);
    }

    interface ProductDetailPresenter {
        void requestProduct(String storeCode, String id);
        void onProductReceived(ProductEntity product);
        void onFailedReceivingProduct(Throwable throwable);
        ProductEntity getProduct();
        List<AppointmentsItemCart> getAppointment(List<CartListItem> items, String sku);
        void addAppointmentData(AppointmentsItemCart item);
        void editAppointment(int position, AppointmentsItemCart item);
        void prepareAppointmentData();
        void deleteItem(int position);
        CartListItem getCartItem(String sku, int itemId, int storeCode);
        void clearAppointment();
    }

    interface ProductDetailInteractor {
        void requestProduct(NetworkHandler networkHandler,String storeCode, String id);
    }
}
