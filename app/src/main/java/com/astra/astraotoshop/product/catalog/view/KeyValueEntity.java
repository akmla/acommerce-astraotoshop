package com.astra.astraotoshop.product.catalog.view;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Henra Setia Nugraha on 1/14/2018.
 */

public class KeyValueEntity implements Parcelable{
    private String key;
    private String value;

    public KeyValueEntity(String key, String value) {
        this.key = key;
        this.value = value;
    }

    protected KeyValueEntity(Parcel in) {
        key = in.readString();
        value = in.readString();
    }

    public static final Creator<KeyValueEntity> CREATOR = new Creator<KeyValueEntity>() {
        @Override
        public KeyValueEntity createFromParcel(Parcel in) {
            return new KeyValueEntity(in);
        }

        @Override
        public KeyValueEntity[] newArray(int size) {
            return new KeyValueEntity[size];
        }
    };

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(value);
    }
}
