package com.astra.astraotoshop.product.catalog.provider;

import com.google.gson.annotations.SerializedName;

public class PriceEntity {

	@SerializedName("min")
	private int min;

	@SerializedName("max")
	private String max;

	public void setMin(int min){
		this.min = min;
	}

	public int getMin(){
		return min;
	}

	public void setMax(String max){
		this.max = max;
	}

	public String getMax(){
		return max;
	}
}