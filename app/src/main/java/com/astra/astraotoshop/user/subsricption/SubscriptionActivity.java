package com.astra.astraotoshop.user.subsricption;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.subsricption.provider.SubscriptionPresenter;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubscriptionActivity extends BaseActivity implements SubscriptionContract.SubscriptionView {

    @BindView(R.id.subscription)
    CheckBox subscription;

    private SubscriptionContract.SubscriptionPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        ButterKnife.bind(this);
        actionBarSetup("Berlangganan Newsletter");
        presenter = new SubscriptionPresenter(this, getStoreCode());
        presenter.getSubscription();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    public void subscription(boolean isSubscribe) {
        subscription.setChecked(isSubscribe);
    }

    @Override
    public void showMessage(String message) {
        if (message != null)
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.save)
    public void onSaveClicked() {
        presenter.changeSubscription(subscription.isChecked());
    }
}
