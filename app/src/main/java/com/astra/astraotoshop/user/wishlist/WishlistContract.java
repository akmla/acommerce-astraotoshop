package com.astra.astraotoshop.user.wishlist;

import com.astra.astraotoshop.user.wishlist.provider.AddWishlistBody;
import com.astra.astraotoshop.user.wishlist.provider.WishlistEntity;
import com.astra.astraotoshop.user.wishlist.provider.WishlistItem;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 2/22/2018.
 */

public interface WishlistContract {
    interface AddWishlist{
        void showSuccessMessage();
        void showFailedMessage();
    }

    interface WishlistView{
        void showWishlist(List<WishlistItem> wishlistItems);
        void showMessage(boolean isDelete, String message);
    }

    interface WishlistPresenter{
        void requestWishlist();
        void addWishlist(int productId);
        void deleteItemWishlist(WishlistItem wishlistItemId);

        void onWishlistReceived(WishlistEntity wishlistEntity);
        void onFailedReceivingWishlist(Throwable error);

        void onAddSucceed();
        void onAddFailed(Throwable error);

        void onDeleteSucceed();
        void onDeleteFailed(Throwable error);
    }

    interface WishlistInteractor{
        void requestWishlist(String storeCode);
        void addWishlist(String storeCode, AddWishlistBody wishlistBody);
        void deleteItemWishlist(String storeCode, String wishlistItemId);
    }
}
