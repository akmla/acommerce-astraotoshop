package com.astra.astraotoshop.user.profile.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Addresses implements Parcelable{

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("city")
    private String city;

    @SerializedName("region_id")
    private int regionId;

    @SerializedName("postcode")
    private String postcode;

    @SerializedName("telephone")
    private String telephone;

    @SerializedName("default_shipping")
    private boolean defaultShipping;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("custom_attributes")
    private List<com.astra.astraotoshop.user.address.provider.CustomAttributesItem> customAttributes;

    @SerializedName("street")
    private List<String> street;

    @SerializedName("id")
    private int id;

    @SerializedName("customer_id")
    private int customerId;

    @SerializedName("region")
    private Region region;

    @SerializedName("fax")
    public String fax;

    @SerializedName("default_billing")
    private boolean defaultBilling;

    @SerializedName("country_id")
    private String countryId;

    protected Addresses(Parcel in) {
        firstname = in.readString();
        city = in.readString();
        regionId = in.readInt();
        postcode = in.readString();
        telephone = in.readString();
        defaultShipping = in.readByte() != 0;
        lastname = in.readString();
        customAttributes = in.createTypedArrayList(com.astra.astraotoshop.user.address.provider.CustomAttributesItem.CREATOR);
        street = in.createStringArrayList();
        id = in.readInt();
        customerId = in.readInt();
        region = in.readParcelable(Region.class.getClassLoader());
        fax = in.readString();
        defaultBilling = in.readByte() != 0;
        countryId = in.readString();
    }

    public static final Creator<Addresses> CREATOR = new Creator<Addresses>() {
        @Override
        public Addresses createFromParcel(Parcel in) {
            return new Addresses(in);
        }

        @Override
        public Addresses[] newArray(int size) {
            return new Addresses[size];
        }
    };

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setDefaultShipping(boolean defaultShipping) {
        this.defaultShipping = defaultShipping;
    }

    public boolean isDefaultShipping() {
        return defaultShipping;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setCustomAttributes(List<com.astra.astraotoshop.user.address.provider.CustomAttributesItem> customAttributes) {
        this.customAttributes = customAttributes;
    }

    public List<com.astra.astraotoshop.user.address.provider.CustomAttributesItem> getCustomAttributes() {
        return customAttributes;
    }

    public void setStreet(List<String> street) {
        this.street = street;
    }

    public List<String> getStreet() {
        return street;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Region getRegion() {
        return region;
    }

    public void setDefaultBilling(boolean defaultBilling) {
        this.defaultBilling = defaultBilling;
    }

    public boolean isDefaultBilling() {
        return defaultBilling;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstname);
        dest.writeString(city);
        dest.writeInt(regionId);
        dest.writeString(postcode);
        dest.writeString(telephone);
        dest.writeByte((byte) (defaultShipping ? 1 : 0));
        dest.writeString(lastname);
        dest.writeTypedList(customAttributes);
        dest.writeStringList(street);
        dest.writeInt(id);
        dest.writeInt(customerId);
        dest.writeParcelable(region, flags);
        dest.writeString(fax);
        dest.writeByte((byte) (defaultBilling ? 1 : 0));
        dest.writeString(countryId);
    }
}