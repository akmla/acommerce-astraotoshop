package com.astra.astraotoshop.user.signup.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomeAttribute {

    @SerializedName("custom_attributes")
    private List<CustomAttributesItem> customAttributes;

    public CustomeAttribute(List<CustomAttributesItem> customAttributes) {
        this.customAttributes = customAttributes;
    }

    public void setCustomAttributes(List<CustomAttributesItem> customAttributes) {
        this.customAttributes = customAttributes;
    }

    public List<CustomAttributesItem> getCustomAttributes() {
        return customAttributes;
    }

    @Override
    public String toString() {
        return
                "CustomeAttribute{" +
                        "custom_attributes = '" + customAttributes + '\'' +
                        "}";
    }
}