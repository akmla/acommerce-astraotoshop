package com.astra.astraotoshop.user.password;

import com.astra.astraotoshop.user.password.provider.ChangePassword;
import com.astra.astraotoshop.user.password.provider.IsEmailAvailable;
import com.astra.astraotoshop.user.password.provider.ResetPassword;

import rx.functions.Action1;

/**
 * Created by Henra Setia Nugraha on 5/15/2018.
 */

public interface PasswordContract {
    interface ChangePasswordView {
        void showPasswordMessage(boolean isSuccess);
    }
    interface ChangePasswordPresenter {
        void changePassword(String storeCode,String currentPass,String newPass);
        void onPasswordChanged();
        void onPasswordUnchanged();
    }
    interface ChangePasswordInteractor {
        void changePassword(String storeCode, ChangePassword changePassword);
    }

    interface ResetPasswordView{
        void showResetPasswordMessage(boolean isSuccess);
        void showEmailNotExistMessage();
    }
    interface ResetPasswordPresenter{
        void resetPassword(String email);
        void onPasswordReset();
        void onResetPasswordFail();
    }
    interface ResetPasswordInteractor{
        void emailChecking(IsEmailAvailable email, Action1<Boolean> action1, Action1<Throwable> throwableAction1);
        void resetPassword(ResetPassword resetPassword);
    }
}
