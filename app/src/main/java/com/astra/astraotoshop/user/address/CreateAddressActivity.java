package com.astra.astraotoshop.user.address;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.address.provider.AddressPresenter;
import com.astra.astraotoshop.user.address.provider.CustomAttributesItem;
import com.astra.astraotoshop.user.profile.provider.Addresses;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.Loading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import fr.ganfra.materialspinner.MaterialSpinner;

public class CreateAddressActivity extends BaseActivity implements AddressContract.AddressView, AdapterView.OnItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.firstName)
    EditText firstName;
    @BindView(R.id.lastName)
    EditText lastName;
    @BindView(R.id.company)
    EditText company;
    @BindView(R.id.telephone)
    EditText telephone;
    @BindView(R.id.fax)
    EditText fax;
    @BindView(R.id.country)
    MaterialSpinner country;
    @BindView(R.id.region)
    MaterialSpinner region;
    @BindView(R.id.city)
    MaterialSpinner city;
    @BindView(R.id.postCode)
    EditText postCode;
    @BindView(R.id.street)
    EditText street;
    @BindView(R.id.defaultAddress)
    CheckBox defaultAddress;
    @BindView(R.id.firstNameContainer)
    TextInputLayout firstNameContainer;
    @BindView(R.id.lastNameContainer)
    TextInputLayout lastNameContainer;
    @BindView(R.id.companyContainer)
    TextInputLayout companyContainer;
    @BindView(R.id.telephoneContainer)
    TextInputLayout telephoneContainer;
    @BindView(R.id.faxContainer)
    TextInputLayout faxContainer;
    @BindView(R.id.postCodeContainer)
    TextInputLayout postCodeContainer;
    @BindView(R.id.streetContainer)
    TextInputLayout streetContainer;
    @BindView(R.id.district)
    EditText district;
    @BindView(R.id.districtContainer)
    TextInputLayout districtContainer;
    @BindView(R.id.rtrw)
    EditText rtrw;
    @BindView(R.id.rtrwContainer)
    TextInputLayout rtrwContainer;
    @BindView(R.id.delete)
    Button delete;
    @BindView(R.id.saveAddress)
    Button saveAddress;

    private AddressContract.AddressPresenter presenter;
    private ArrayAdapter<String> countryAdapter;
    private ArrayAdapter<String> regionAdapter;
    private ArrayAdapter<String> cityAdapter;
    private Loading loading;
    private String regionCode;
    private String cityValue;
    private boolean isUpdate = false;
    private int addressId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_address);
        ButterKnife.bind(this);
        actionBarSetup();
        presenter = new AddressPresenter(this, getStoreCode());
        loading = new Loading(this, getLayoutInflater());
        initAdapter();
        getIntentData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.saveAddress)
    public void saveAddress() {
        if (isValid()) {
            presenter.saveAddress(
                    firstName.getText().toString(),
                    lastName.getText().toString(),
                    postCode.getText().toString(),
                    presenter.getCity(city.getSelectedItemPosition()).getName(),
                    presenter.getRegion(region.getSelectedItemPosition()).getRegionId(),
                    telephone.getText().toString(),
                    Collections.singletonList(street.getText().toString()),
                    district.getText().toString(),
                    rtrw.getText().toString(),
                    defaultAddress.isChecked(),
                    String.valueOf(addressId),
                    isUpdate
            );
            loading.show("Harap tunggu");
        }
    }

    @Override
    public void onAddressSaved() {
        loading.dismiss();
        Toast.makeText(this, "Berhasil menyimpan alamat", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onSaveFailed() {
        loading.dismiss();
        Toast.makeText(this, "Gagal menyimpan alamat", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddressDeleted() {
        loading.dismiss();
        Toast.makeText(this, "Alamat berhasil di hapus", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onDeleteAddressFailed() {
        loading.dismiss();
        Toast.makeText(this, "Alamat gagal di hapus", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRegionReceived(List<String> regions) {
        setData(regionAdapter, regions);
        if (regionCode != null) {
            for (int i = 0; i < regions.size(); i++) {
                if (regions.get(i).equalsIgnoreCase(regionCode)) {
                    region.setSelection(i + 1);
                    break;
                }
            }
        }
    }

    @Override
    public void onCityReceived(List<String> cities) {
        setData(cityAdapter, cities);
        if (cityValue != null) {
            for (int i = 0; i < cities.size(); i++) {
                if (cities.get(i).equalsIgnoreCase(cityValue)) {
                    city.setSelection(i + 1);
                    break;
                }
            }
        }
    }

    @Override
    public void onReceivingFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.country: {
                try {
                    if (position == -1)
                        setDefaultSpinnerData(regionAdapter, region);
                    if (!country.getSelectedItem().equals(country.getHint()))
                        presenter.requestRegion("ID");
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                break;
            }
            case R.id.region: {
                try {
                    if (position == -1)
                        setDefaultSpinnerData(cityAdapter, city);
                    if (!region.getSelectedItem().equals(region.getHint())) {
                        setDefaultSpinnerData(cityAdapter, city);
                        presenter.requestCity(presenter.getRegion(region.getSelectedItemPosition()).getRegionId());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick(R.id.delete)
    public void deleteAddress() {
        presenter.deleteAddress(String.valueOf(addressId));
        loading.show("Harap tunggu");
    }

    @OnTouch({R.id.country, R.id.city, R.id.region})
    public boolean onSpinnerTouch(View view, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            view.performClick();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        return true;
    }

    private void getIntentData() {
        isUpdate = getIntent().getBooleanExtra("isUpdate", false);
        if (isUpdate) {
            setCustomTitle("Ubah Alamat");
            buttonSetup();
            Addresses addresses = getIntent().getParcelableExtra("address");
            if (addresses != null) {
                setAddressValue(addresses);
            }
        }
    }

    private void setAddressValue(Addresses addresses) {
        firstName.setText(addresses.getFirstname());
        lastName.setText(addresses.getLastname());
        telephone.setText(addresses.getTelephone());
        street.setText(addresses.getStreet().get(0));
        postCode.setText(addresses.getPostcode());
        addressId = addresses.getId();

        for (CustomAttributesItem item : addresses.getCustomAttributes()) {
            if (item.getAttributeCode().equalsIgnoreCase("kelurahan")) {
                district.setText(item.getValue());
            } else if (item.getAttributeCode().equalsIgnoreCase("rt_rw")) {
                rtrw.setText(item.getValue());
            }
        }

        fax.setText(addresses.getFax());
        country.setSelection(1);
        regionCode = addresses.getRegion().getRegion();
        cityValue = addresses.getCity();

    }

    private void buttonSetup() {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) saveAddress.getLayoutParams();
        layoutParams.setMarginStart(getPX(8));
        layoutParams.setMarginEnd(getPX(24));
        saveAddress.setLayoutParams(layoutParams);
        delete.setVisibility(View.VISIBLE);
    }

    private void initAdapter() {
        countryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Collections.singletonList("Indonesia"));
        regionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new ArrayList<>());
        cityAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new ArrayList<>());

        country.setAdapter(countryAdapter);
        country.setOnItemSelectedListener(this);
        region.setAdapter(regionAdapter);
        region.setOnItemSelectedListener(this);
        city.setAdapter(cityAdapter);
        city.setOnItemSelectedListener(this);
    }

    public void setDefaultSpinnerData(ArrayAdapter adapter, MaterialSpinner spinner) {
        setData(adapter, new ArrayList<>());
        spinner.setSelection(0);
    }

    private void setData(ArrayAdapter adapter, List<String> data) {
        adapter.clear();
        adapter.addAll(data);
        adapter.notifyDataSetChanged();
    }

    private boolean isValid() {
        List<Boolean> valid = new ArrayList<>();
        valid.add(textEmpty(firstName, firstNameContainer, "Nama depan wajib diisi"));
        valid.add(textEmpty(lastName, lastNameContainer, "Nama belakang wajib diisi"));
        valid.add(textEmpty(telephone, telephoneContainer, "Telepon wajib diisi"));
        valid.add(textEmpty(postCode, postCodeContainer, "Kode pos wajib diisi"));
        valid.add(textEmpty(street, streetContainer, "Nama jalan wajib diisi"));
        valid.add(textEmpty(district, districtContainer, "Nama kelurahan wajib diisi"));
        valid.add(spinnerSelected(country, "Pilih Negara"));
        valid.add(spinnerSelected(region, "Pilih Provinsi"));
        valid.add(spinnerSelected(city, "Pilih Kota"));
        valid.add(telephone.getText().length() > 5);
        if (!(telephone.getText().length() > 5))
            telephoneContainer.setError("Nomor telopon minimal 6 angka");
        else telephoneContainer.setError(null);
        return !valid.contains(false);
    }

    private Boolean textEmpty(TextView view, TextInputLayout container, String errorMessage) {
        if (TextUtils.isEmpty(view.getText().toString())) {
            container.setError(errorMessage);
            return false;
        } else container.setError("");
        return true;
    }

    private Boolean spinnerSelected(MaterialSpinner spinner, String errorMessage) {
        if (spinner.getSelectedItem() != null) {
            if (spinner.getSelectedItem().equals(spinner.getHint())) {
                spinner.setError(errorMessage);
                return false;
            } else {
                spinner.setError(null);
                return true;
            }
        } else {
            spinner.setError(errorMessage);
            return false;
        }
    }

    private int getPX(int DP) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
    }
}
