package com.astra.astraotoshop.user.order.provider;

import com.astra.astraotoshop.order.cart.provider.AppointmentsItemCart;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductssItem {

    @SerializedName("tax_invoiced")
    private String taxInvoiced;

    @SerializedName("free_shipping")
    private String freeShipping;

    @SerializedName("base_discount_tax_compensation_invoiced")
    private String baseDiscountTaxCompensationInvoiced;

    @SerializedName("gift_message_available")
    private String giftMessageAvailable;

    @SerializedName("tax_canceled")
    private Object taxCanceled;

    @SerializedName("gw_tax_amount")
    private Object gwTaxAmount;

    @SerializedName("weee_tax_applied_amount")
    private Object weeeTaxAppliedAmount;

    @SerializedName("tax_percent")
    private String taxPercent;

    @SerializedName("discount_refunded")
    private Object discountRefunded;

    @SerializedName("base_weee_tax_row_disposition")
    private Object baseWeeeTaxRowDisposition;

    @SerializedName("gw_price_refunded")
    private Object gwPriceRefunded;

    @SerializedName("wmos_respond")
    private String wmosRespond;

    @SerializedName("price_incl_tax")
    private String priceInclTax;

    @SerializedName("price")
    private String price;

    @SerializedName("product_id")
    private String productId;

    @SerializedName("tax_before_discount")
    private Object taxBeforeDiscount;

    @SerializedName("base_tax_refunded")
    private Object baseTaxRefunded;

    @SerializedName("base_row_total")
    private String baseRowTotal;

    @SerializedName("sku")
    private String sku;

    @SerializedName("base_weee_tax_disposition")
    private Object baseWeeeTaxDisposition;

    @SerializedName("base_discount_refunded")
    private Object baseDiscountRefunded;

    @SerializedName("gw_price")
    private Object gwPrice;

    @SerializedName("base_weee_tax_applied_row_amnt")
    private Object baseWeeeTaxAppliedRowAmnt;

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("gw_id")
    private Object gwId;

    @SerializedName("base_original_price")
    private String baseOriginalPrice;

    @SerializedName("quote_item_id")
    private String quoteItemId;

    @SerializedName("row_weight")
    private String rowWeight;

    @SerializedName("giftregistry_item_id")
    private Object giftregistryItemId;

    @SerializedName("applied_rule_ids")
    private Object appliedRuleIds;

    @SerializedName("additional_data")
    private Object additionalData;

    @SerializedName("base_amount_refunded")
    private Object baseAmountRefunded;

    @SerializedName("no_discount")
    private String noDiscount;

    @SerializedName("gw_base_price_refunded")
    private Object gwBasePriceRefunded;

    @SerializedName("discount_percent")
    private String discountPercent;

    @SerializedName("order_id")
    private String orderId;

    @SerializedName("gw_tax_amount_refunded")
    private Object gwTaxAmountRefunded;

    @SerializedName("weee_tax_row_disposition")
    private Object weeeTaxRowDisposition;

    @SerializedName("discount_tax_compensation_canceled")
    private Object discountTaxCompensationCanceled;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("row_total")
    private String rowTotal;

    @SerializedName("amount_refunded")
    private Object amountRefunded;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("weee_tax_applied")
    private String weeeTaxApplied;

    @SerializedName("qty_invoiced")
    private String qtyInvoiced;

    @SerializedName("row_invoiced")
    private String rowInvoiced;

    @SerializedName("base_tax_amount")
    private String baseTaxAmount;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("base_discount_tax_compensation_refunded")
    private Object baseDiscountTaxCompensationRefunded;

    @SerializedName("base_discount_amount")
    private String baseDiscountAmount;

    @SerializedName("weee_tax_disposition")
    private Object weeeTaxDisposition;

    @SerializedName("gw_base_tax_amount_invoiced")
    private Object gwBaseTaxAmountInvoiced;

    @SerializedName("base_row_total_incl_tax")
    private String baseRowTotalInclTax;

    @SerializedName("event_id")
    private Object eventId;

    @SerializedName("qty_refunded")
    private Object qtyRefunded;

    @SerializedName("parent_item_id")
    private Object parentItemId;

    @SerializedName("gw_base_tax_amount_refunded")
    private Object gwBaseTaxAmountRefunded;

    @SerializedName("discount_invoiced")
    private String discountInvoiced;

    @SerializedName("base_tax_invoiced")
    private String baseTaxInvoiced;

    @SerializedName("tax_amount")
    private String taxAmount;

    @SerializedName("original_price")
    private String originalPrice;

    @SerializedName("locked_do_ship")
    private Object lockedDoShip;

    @SerializedName("discount_amount")
    private String discountAmount;

    @SerializedName("appointment")
    private Object appointment;

    @SerializedName("wmos_inbound")
    private String wmosInbound;

    @SerializedName("weee_tax_applied_row_amount")
    private Object weeeTaxAppliedRowAmount;

    @SerializedName("gw_base_price")
    private Object gwBasePrice;

    @SerializedName("qty_backordered")
    private Object qtyBackordered;

    @SerializedName("weight")
    private String weight;

    @SerializedName("discount_tax_compensation_amount")
    private String discountTaxCompensationAmount;

    @SerializedName("base_tax_before_discount")
    private Object baseTaxBeforeDiscount;

    @SerializedName("gw_price_invoiced")
    private Object gwPriceInvoiced;

    @SerializedName("base_price_incl_tax")
    private String basePriceInclTax;

    @SerializedName("product_options")
    private ProductOptions productOptions;

    @SerializedName("name")
    private String name;

    @SerializedName("base_discount_invoiced")
    private String baseDiscountInvoiced;

    @SerializedName("ext_order_item_id")
    private Object extOrderItemId;

    @SerializedName("base_weee_tax_applied_amount")
    private Object baseWeeeTaxAppliedAmount;

    @SerializedName("is_virtual")
    private String isVirtual;

    @SerializedName("discount_tax_compensation_invoiced")
    private String discountTaxCompensationInvoiced;

    @SerializedName("gw_base_tax_amount")
    private Object gwBaseTaxAmount;

    @SerializedName("base_cost")
    private Object baseCost;

    @SerializedName("gift_message_id")
    private Object giftMessageId;

    @SerializedName("description")
    private Object description;

    @SerializedName("qty_shipped")
    private Object qtyShipped;

    @SerializedName("qty_ordered")
    private String qtyOrdered;

    @SerializedName("qty_canceled")
    private Object qtyCanceled;

    @SerializedName("gw_base_price_invoiced")
    private Object gwBasePriceInvoiced;

    @SerializedName("gw_tax_amount_invoiced")
    private Object gwTaxAmountInvoiced;

    @SerializedName("base_price")
    private String basePrice;

    @SerializedName("row_total_incl_tax")
    private String rowTotalInclTax;

    @SerializedName("locked_do_invoice")
    private Object lockedDoInvoice;

    @SerializedName("qty_returned")
    private Object qtyReturned;

    @SerializedName("discount_tax_compensation_refunded")
    private Object discountTaxCompensationRefunded;

    @SerializedName("item_id")
    private String itemId;

    @SerializedName("tax_refunded")
    private Object taxRefunded;

    @SerializedName("base_discount_tax_compensation_amount")
    private String baseDiscountTaxCompensationAmount;

    @SerializedName("product_type")
    private String productType;

    @SerializedName("base_row_invoiced")
    private String baseRowInvoiced;

    @SerializedName("is_qty_decimal")
    private String isQtyDecimal;

    @SerializedName("method_description")
    private List<MethodDescription> methodDescriptions;

    @SerializedName("appointment_data")
    private AppointmentsItemCart appointmentsItemsCart;

    public AppointmentsItemCart getAppointmentsItemsCart() {
        return appointmentsItemsCart;
    }

    public void setAppointmentsItemsCart(AppointmentsItemCart appointmentsItemsCart) {
        this.appointmentsItemsCart = appointmentsItemsCart;
    }

    public List<MethodDescription> getMethodDescriptions() {
        return methodDescriptions;
    }

    public void setMethodDescriptions(List<MethodDescription> methodDescriptions) {
        this.methodDescriptions = methodDescriptions;
    }

    public void setTaxInvoiced(String taxInvoiced) {
        this.taxInvoiced = taxInvoiced;
    }

    public String getTaxInvoiced() {
        return taxInvoiced;
    }

    public void setFreeShipping(String freeShipping) {
        this.freeShipping = freeShipping;
    }

    public String getFreeShipping() {
        return freeShipping;
    }

    public void setBaseDiscountTaxCompensationInvoiced(String baseDiscountTaxCompensationInvoiced) {
        this.baseDiscountTaxCompensationInvoiced = baseDiscountTaxCompensationInvoiced;
    }

    public String getBaseDiscountTaxCompensationInvoiced() {
        return baseDiscountTaxCompensationInvoiced;
    }

    public void setGiftMessageAvailable(String giftMessageAvailable) {
        this.giftMessageAvailable = giftMessageAvailable;
    }

    public String getGiftMessageAvailable() {
        return giftMessageAvailable;
    }

    public void setTaxCanceled(Object taxCanceled) {
        this.taxCanceled = taxCanceled;
    }

    public Object getTaxCanceled() {
        return taxCanceled;
    }

    public void setGwTaxAmount(Object gwTaxAmount) {
        this.gwTaxAmount = gwTaxAmount;
    }

    public Object getGwTaxAmount() {
        return gwTaxAmount;
    }

    public void setWeeeTaxAppliedAmount(Object weeeTaxAppliedAmount) {
        this.weeeTaxAppliedAmount = weeeTaxAppliedAmount;
    }

    public Object getWeeeTaxAppliedAmount() {
        return weeeTaxAppliedAmount;
    }

    public void setTaxPercent(String taxPercent) {
        this.taxPercent = taxPercent;
    }

    public String getTaxPercent() {
        return taxPercent;
    }

    public void setDiscountRefunded(Object discountRefunded) {
        this.discountRefunded = discountRefunded;
    }

    public Object getDiscountRefunded() {
        return discountRefunded;
    }

    public void setBaseWeeeTaxRowDisposition(Object baseWeeeTaxRowDisposition) {
        this.baseWeeeTaxRowDisposition = baseWeeeTaxRowDisposition;
    }

    public Object getBaseWeeeTaxRowDisposition() {
        return baseWeeeTaxRowDisposition;
    }

    public void setGwPriceRefunded(Object gwPriceRefunded) {
        this.gwPriceRefunded = gwPriceRefunded;
    }

    public Object getGwPriceRefunded() {
        return gwPriceRefunded;
    }

    public void setWmosRespond(String wmosRespond) {
        this.wmosRespond = wmosRespond;
    }

    public String getWmosRespond() {
        return wmosRespond;
    }

    public void setPriceInclTax(String priceInclTax) {
        this.priceInclTax = priceInclTax;
    }

    public String getPriceInclTax() {
        return priceInclTax;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setTaxBeforeDiscount(Object taxBeforeDiscount) {
        this.taxBeforeDiscount = taxBeforeDiscount;
    }

    public Object getTaxBeforeDiscount() {
        return taxBeforeDiscount;
    }

    public void setBaseTaxRefunded(Object baseTaxRefunded) {
        this.baseTaxRefunded = baseTaxRefunded;
    }

    public Object getBaseTaxRefunded() {
        return baseTaxRefunded;
    }

    public void setBaseRowTotal(String baseRowTotal) {
        this.baseRowTotal = baseRowTotal;
    }

    public String getBaseRowTotal() {
        return baseRowTotal;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSku() {
        return sku;
    }

    public void setBaseWeeeTaxDisposition(Object baseWeeeTaxDisposition) {
        this.baseWeeeTaxDisposition = baseWeeeTaxDisposition;
    }

    public Object getBaseWeeeTaxDisposition() {
        return baseWeeeTaxDisposition;
    }

    public void setBaseDiscountRefunded(Object baseDiscountRefunded) {
        this.baseDiscountRefunded = baseDiscountRefunded;
    }

    public Object getBaseDiscountRefunded() {
        return baseDiscountRefunded;
    }

    public void setGwPrice(Object gwPrice) {
        this.gwPrice = gwPrice;
    }

    public Object getGwPrice() {
        return gwPrice;
    }

    public void setBaseWeeeTaxAppliedRowAmnt(Object baseWeeeTaxAppliedRowAmnt) {
        this.baseWeeeTaxAppliedRowAmnt = baseWeeeTaxAppliedRowAmnt;
    }

    public Object getBaseWeeeTaxAppliedRowAmnt() {
        return baseWeeeTaxAppliedRowAmnt;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setGwId(Object gwId) {
        this.gwId = gwId;
    }

    public Object getGwId() {
        return gwId;
    }

    public void setBaseOriginalPrice(String baseOriginalPrice) {
        this.baseOriginalPrice = baseOriginalPrice;
    }

    public String getBaseOriginalPrice() {
        return baseOriginalPrice;
    }

    public void setQuoteItemId(String quoteItemId) {
        this.quoteItemId = quoteItemId;
    }

    public String getQuoteItemId() {
        return quoteItemId;
    }

    public void setRowWeight(String rowWeight) {
        this.rowWeight = rowWeight;
    }

    public String getRowWeight() {
        return rowWeight;
    }

    public void setGiftregistryItemId(Object giftregistryItemId) {
        this.giftregistryItemId = giftregistryItemId;
    }

    public Object getGiftregistryItemId() {
        return giftregistryItemId;
    }

    public void setAppliedRuleIds(Object appliedRuleIds) {
        this.appliedRuleIds = appliedRuleIds;
    }

    public Object getAppliedRuleIds() {
        return appliedRuleIds;
    }

    public void setAdditionalData(Object additionalData) {
        this.additionalData = additionalData;
    }

    public Object getAdditionalData() {
        return additionalData;
    }

    public void setBaseAmountRefunded(Object baseAmountRefunded) {
        this.baseAmountRefunded = baseAmountRefunded;
    }

    public Object getBaseAmountRefunded() {
        return baseAmountRefunded;
    }

    public void setNoDiscount(String noDiscount) {
        this.noDiscount = noDiscount;
    }

    public String getNoDiscount() {
        return noDiscount;
    }

    public void setGwBasePriceRefunded(Object gwBasePriceRefunded) {
        this.gwBasePriceRefunded = gwBasePriceRefunded;
    }

    public Object getGwBasePriceRefunded() {
        return gwBasePriceRefunded;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setGwTaxAmountRefunded(Object gwTaxAmountRefunded) {
        this.gwTaxAmountRefunded = gwTaxAmountRefunded;
    }

    public Object getGwTaxAmountRefunded() {
        return gwTaxAmountRefunded;
    }

    public void setWeeeTaxRowDisposition(Object weeeTaxRowDisposition) {
        this.weeeTaxRowDisposition = weeeTaxRowDisposition;
    }

    public Object getWeeeTaxRowDisposition() {
        return weeeTaxRowDisposition;
    }

    public void setDiscountTaxCompensationCanceled(Object discountTaxCompensationCanceled) {
        this.discountTaxCompensationCanceled = discountTaxCompensationCanceled;
    }

    public Object getDiscountTaxCompensationCanceled() {
        return discountTaxCompensationCanceled;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setRowTotal(String rowTotal) {
        this.rowTotal = rowTotal;
    }

    public String getRowTotal() {
        return rowTotal;
    }

    public void setAmountRefunded(Object amountRefunded) {
        this.amountRefunded = amountRefunded;
    }

    public Object getAmountRefunded() {
        return amountRefunded;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setWeeeTaxApplied(String weeeTaxApplied) {
        this.weeeTaxApplied = weeeTaxApplied;
    }

    public String getWeeeTaxApplied() {
        return weeeTaxApplied;
    }

    public void setQtyInvoiced(String qtyInvoiced) {
        this.qtyInvoiced = qtyInvoiced;
    }

    public String getQtyInvoiced() {
        return qtyInvoiced;
    }

    public void setRowInvoiced(String rowInvoiced) {
        this.rowInvoiced = rowInvoiced;
    }

    public String getRowInvoiced() {
        return rowInvoiced;
    }

    public void setBaseTaxAmount(String baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }

    public String getBaseTaxAmount() {
        return baseTaxAmount;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setBaseDiscountTaxCompensationRefunded(Object baseDiscountTaxCompensationRefunded) {
        this.baseDiscountTaxCompensationRefunded = baseDiscountTaxCompensationRefunded;
    }

    public Object getBaseDiscountTaxCompensationRefunded() {
        return baseDiscountTaxCompensationRefunded;
    }

    public void setBaseDiscountAmount(String baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }

    public String getBaseDiscountAmount() {
        return baseDiscountAmount;
    }

    public void setWeeeTaxDisposition(Object weeeTaxDisposition) {
        this.weeeTaxDisposition = weeeTaxDisposition;
    }

    public Object getWeeeTaxDisposition() {
        return weeeTaxDisposition;
    }

    public void setGwBaseTaxAmountInvoiced(Object gwBaseTaxAmountInvoiced) {
        this.gwBaseTaxAmountInvoiced = gwBaseTaxAmountInvoiced;
    }

    public Object getGwBaseTaxAmountInvoiced() {
        return gwBaseTaxAmountInvoiced;
    }

    public void setBaseRowTotalInclTax(String baseRowTotalInclTax) {
        this.baseRowTotalInclTax = baseRowTotalInclTax;
    }

    public String getBaseRowTotalInclTax() {
        return baseRowTotalInclTax;
    }

    public void setEventId(Object eventId) {
        this.eventId = eventId;
    }

    public Object getEventId() {
        return eventId;
    }

    public void setQtyRefunded(Object qtyRefunded) {
        this.qtyRefunded = qtyRefunded;
    }

    public Object getQtyRefunded() {
        return qtyRefunded;
    }

    public void setParentItemId(Object parentItemId) {
        this.parentItemId = parentItemId;
    }

    public Object getParentItemId() {
        return parentItemId;
    }

    public void setGwBaseTaxAmountRefunded(Object gwBaseTaxAmountRefunded) {
        this.gwBaseTaxAmountRefunded = gwBaseTaxAmountRefunded;
    }

    public Object getGwBaseTaxAmountRefunded() {
        return gwBaseTaxAmountRefunded;
    }

    public void setDiscountInvoiced(String discountInvoiced) {
        this.discountInvoiced = discountInvoiced;
    }

    public String getDiscountInvoiced() {
        return discountInvoiced;
    }

    public void setBaseTaxInvoiced(String baseTaxInvoiced) {
        this.baseTaxInvoiced = baseTaxInvoiced;
    }

    public String getBaseTaxInvoiced() {
        return baseTaxInvoiced;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setLockedDoShip(Object lockedDoShip) {
        this.lockedDoShip = lockedDoShip;
    }

    public Object getLockedDoShip() {
        return lockedDoShip;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setAppointment(Object appointment) {
        this.appointment = appointment;
    }

    public Object getAppointment() {
        return appointment;
    }

    public void setWmosInbound(String wmosInbound) {
        this.wmosInbound = wmosInbound;
    }

    public String getWmosInbound() {
        return wmosInbound;
    }

    public void setWeeeTaxAppliedRowAmount(Object weeeTaxAppliedRowAmount) {
        this.weeeTaxAppliedRowAmount = weeeTaxAppliedRowAmount;
    }

    public Object getWeeeTaxAppliedRowAmount() {
        return weeeTaxAppliedRowAmount;
    }

    public void setGwBasePrice(Object gwBasePrice) {
        this.gwBasePrice = gwBasePrice;
    }

    public Object getGwBasePrice() {
        return gwBasePrice;
    }

    public void setQtyBackordered(Object qtyBackordered) {
        this.qtyBackordered = qtyBackordered;
    }

    public Object getQtyBackordered() {
        return qtyBackordered;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeight() {
        return weight;
    }

    public void setDiscountTaxCompensationAmount(String discountTaxCompensationAmount) {
        this.discountTaxCompensationAmount = discountTaxCompensationAmount;
    }

    public String getDiscountTaxCompensationAmount() {
        return discountTaxCompensationAmount;
    }

    public void setBaseTaxBeforeDiscount(Object baseTaxBeforeDiscount) {
        this.baseTaxBeforeDiscount = baseTaxBeforeDiscount;
    }

    public Object getBaseTaxBeforeDiscount() {
        return baseTaxBeforeDiscount;
    }

    public void setGwPriceInvoiced(Object gwPriceInvoiced) {
        this.gwPriceInvoiced = gwPriceInvoiced;
    }

    public Object getGwPriceInvoiced() {
        return gwPriceInvoiced;
    }

    public void setBasePriceInclTax(String basePriceInclTax) {
        this.basePriceInclTax = basePriceInclTax;
    }

    public String getBasePriceInclTax() {
        return basePriceInclTax;
    }

    public void setProductOptions(ProductOptions productOptions) {
        this.productOptions = productOptions;
    }

    public ProductOptions getProductOptions() {
        return productOptions;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBaseDiscountInvoiced(String baseDiscountInvoiced) {
        this.baseDiscountInvoiced = baseDiscountInvoiced;
    }

    public String getBaseDiscountInvoiced() {
        return baseDiscountInvoiced;
    }

    public void setExtOrderItemId(Object extOrderItemId) {
        this.extOrderItemId = extOrderItemId;
    }

    public Object getExtOrderItemId() {
        return extOrderItemId;
    }

    public void setBaseWeeeTaxAppliedAmount(Object baseWeeeTaxAppliedAmount) {
        this.baseWeeeTaxAppliedAmount = baseWeeeTaxAppliedAmount;
    }

    public Object getBaseWeeeTaxAppliedAmount() {
        return baseWeeeTaxAppliedAmount;
    }

    public void setIsVirtual(String isVirtual) {
        this.isVirtual = isVirtual;
    }

    public String getIsVirtual() {
        return isVirtual;
    }

    public void setDiscountTaxCompensationInvoiced(String discountTaxCompensationInvoiced) {
        this.discountTaxCompensationInvoiced = discountTaxCompensationInvoiced;
    }

    public String getDiscountTaxCompensationInvoiced() {
        return discountTaxCompensationInvoiced;
    }

    public void setGwBaseTaxAmount(Object gwBaseTaxAmount) {
        this.gwBaseTaxAmount = gwBaseTaxAmount;
    }

    public Object getGwBaseTaxAmount() {
        return gwBaseTaxAmount;
    }

    public void setBaseCost(Object baseCost) {
        this.baseCost = baseCost;
    }

    public Object getBaseCost() {
        return baseCost;
    }

    public void setGiftMessageId(Object giftMessageId) {
        this.giftMessageId = giftMessageId;
    }

    public Object getGiftMessageId() {
        return giftMessageId;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getDescription() {
        return description;
    }

    public void setQtyShipped(Object qtyShipped) {
        this.qtyShipped = qtyShipped;
    }

    public Object getQtyShipped() {
        return qtyShipped;
    }

    public void setQtyOrdered(String qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    public String getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyCanceled(Object qtyCanceled) {
        this.qtyCanceled = qtyCanceled;
    }

    public Object getQtyCanceled() {
        return qtyCanceled;
    }

    public void setGwBasePriceInvoiced(Object gwBasePriceInvoiced) {
        this.gwBasePriceInvoiced = gwBasePriceInvoiced;
    }

    public Object getGwBasePriceInvoiced() {
        return gwBasePriceInvoiced;
    }

    public void setGwTaxAmountInvoiced(Object gwTaxAmountInvoiced) {
        this.gwTaxAmountInvoiced = gwTaxAmountInvoiced;
    }

    public Object getGwTaxAmountInvoiced() {
        return gwTaxAmountInvoiced;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setRowTotalInclTax(String rowTotalInclTax) {
        this.rowTotalInclTax = rowTotalInclTax;
    }

    public String getRowTotalInclTax() {
        return rowTotalInclTax;
    }

    public void setLockedDoInvoice(Object lockedDoInvoice) {
        this.lockedDoInvoice = lockedDoInvoice;
    }

    public Object getLockedDoInvoice() {
        return lockedDoInvoice;
    }

    public void setQtyReturned(Object qtyReturned) {
        this.qtyReturned = qtyReturned;
    }

    public Object getQtyReturned() {
        return qtyReturned;
    }

    public void setDiscountTaxCompensationRefunded(Object discountTaxCompensationRefunded) {
        this.discountTaxCompensationRefunded = discountTaxCompensationRefunded;
    }

    public Object getDiscountTaxCompensationRefunded() {
        return discountTaxCompensationRefunded;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setTaxRefunded(Object taxRefunded) {
        this.taxRefunded = taxRefunded;
    }

    public Object getTaxRefunded() {
        return taxRefunded;
    }

    public void setBaseDiscountTaxCompensationAmount(String baseDiscountTaxCompensationAmount) {
        this.baseDiscountTaxCompensationAmount = baseDiscountTaxCompensationAmount;
    }

    public String getBaseDiscountTaxCompensationAmount() {
        return baseDiscountTaxCompensationAmount;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }

    public void setBaseRowInvoiced(String baseRowInvoiced) {
        this.baseRowInvoiced = baseRowInvoiced;
    }

    public String getBaseRowInvoiced() {
        return baseRowInvoiced;
    }

    public void setIsQtyDecimal(String isQtyDecimal) {
        this.isQtyDecimal = isQtyDecimal;
    }

    public String getIsQtyDecimal() {
        return isQtyDecimal;
    }

    @Override
    public String toString() {
        return
                "ProductssItem{" +
                        "tax_invoiced = '" + taxInvoiced + '\'' +
                        ",free_shipping = '" + freeShipping + '\'' +
                        ",base_discount_tax_compensation_invoiced = '" + baseDiscountTaxCompensationInvoiced + '\'' +
                        ",gift_message_available = '" + giftMessageAvailable + '\'' +
                        ",tax_canceled = '" + taxCanceled + '\'' +
                        ",gw_tax_amount = '" + gwTaxAmount + '\'' +
                        ",weee_tax_applied_amount = '" + weeeTaxAppliedAmount + '\'' +
                        ",tax_percent = '" + taxPercent + '\'' +
                        ",discount_refunded = '" + discountRefunded + '\'' +
                        ",base_weee_tax_row_disposition = '" + baseWeeeTaxRowDisposition + '\'' +
                        ",gw_price_refunded = '" + gwPriceRefunded + '\'' +
                        ",wmos_respond = '" + wmosRespond + '\'' +
                        ",price_incl_tax = '" + priceInclTax + '\'' +
                        ",price = '" + price + '\'' +
                        ",product_id = '" + productId + '\'' +
                        ",tax_before_discount = '" + taxBeforeDiscount + '\'' +
                        ",base_tax_refunded = '" + baseTaxRefunded + '\'' +
                        ",base_row_total = '" + baseRowTotal + '\'' +
                        ",sku = '" + sku + '\'' +
                        ",base_weee_tax_disposition = '" + baseWeeeTaxDisposition + '\'' +
                        ",base_discount_refunded = '" + baseDiscountRefunded + '\'' +
                        ",gw_price = '" + gwPrice + '\'' +
                        ",base_weee_tax_applied_row_amnt = '" + baseWeeeTaxAppliedRowAmnt + '\'' +
                        ",image_url = '" + imageUrl + '\'' +
                        ",gw_id = '" + gwId + '\'' +
                        ",base_original_price = '" + baseOriginalPrice + '\'' +
                        ",quote_item_id = '" + quoteItemId + '\'' +
                        ",row_weight = '" + rowWeight + '\'' +
                        ",giftregistry_item_id = '" + giftregistryItemId + '\'' +
                        ",applied_rule_ids = '" + appliedRuleIds + '\'' +
                        ",additional_data = '" + additionalData + '\'' +
                        ",base_amount_refunded = '" + baseAmountRefunded + '\'' +
                        ",no_discount = '" + noDiscount + '\'' +
                        ",gw_base_price_refunded = '" + gwBasePriceRefunded + '\'' +
                        ",discount_percent = '" + discountPercent + '\'' +
                        ",order_id = '" + orderId + '\'' +
                        ",gw_tax_amount_refunded = '" + gwTaxAmountRefunded + '\'' +
                        ",weee_tax_row_disposition = '" + weeeTaxRowDisposition + '\'' +
                        ",discount_tax_compensation_canceled = '" + discountTaxCompensationCanceled + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",row_total = '" + rowTotal + '\'' +
                        ",amount_refunded = '" + amountRefunded + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",weee_tax_applied = '" + weeeTaxApplied + '\'' +
                        ",qty_invoiced = '" + qtyInvoiced + '\'' +
                        ",row_invoiced = '" + rowInvoiced + '\'' +
                        ",base_tax_amount = '" + baseTaxAmount + '\'' +
                        ",store_id = '" + storeId + '\'' +
                        ",base_discount_tax_compensation_refunded = '" + baseDiscountTaxCompensationRefunded + '\'' +
                        ",base_discount_amount = '" + baseDiscountAmount + '\'' +
                        ",weee_tax_disposition = '" + weeeTaxDisposition + '\'' +
                        ",gw_base_tax_amount_invoiced = '" + gwBaseTaxAmountInvoiced + '\'' +
                        ",base_row_total_incl_tax = '" + baseRowTotalInclTax + '\'' +
                        ",event_id = '" + eventId + '\'' +
                        ",qty_refunded = '" + qtyRefunded + '\'' +
                        ",parent_item_id = '" + parentItemId + '\'' +
                        ",gw_base_tax_amount_refunded = '" + gwBaseTaxAmountRefunded + '\'' +
                        ",discount_invoiced = '" + discountInvoiced + '\'' +
                        ",base_tax_invoiced = '" + baseTaxInvoiced + '\'' +
                        ",tax_amount = '" + taxAmount + '\'' +
                        ",original_price = '" + originalPrice + '\'' +
                        ",locked_do_ship = '" + lockedDoShip + '\'' +
                        ",discount_amount = '" + discountAmount + '\'' +
                        ",appointment = '" + appointment + '\'' +
                        ",wmos_inbound = '" + wmosInbound + '\'' +
                        ",weee_tax_applied_row_amount = '" + weeeTaxAppliedRowAmount + '\'' +
                        ",gw_base_price = '" + gwBasePrice + '\'' +
                        ",qty_backordered = '" + qtyBackordered + '\'' +
                        ",weight = '" + weight + '\'' +
                        ",discount_tax_compensation_amount = '" + discountTaxCompensationAmount + '\'' +
                        ",base_tax_before_discount = '" + baseTaxBeforeDiscount + '\'' +
                        ",gw_price_invoiced = '" + gwPriceInvoiced + '\'' +
                        ",base_price_incl_tax = '" + basePriceInclTax + '\'' +
                        ",product_options = '" + productOptions + '\'' +
                        ",name = '" + name + '\'' +
                        ",base_discount_invoiced = '" + baseDiscountInvoiced + '\'' +
                        ",ext_order_item_id = '" + extOrderItemId + '\'' +
                        ",base_weee_tax_applied_amount = '" + baseWeeeTaxAppliedAmount + '\'' +
                        ",is_virtual = '" + isVirtual + '\'' +
                        ",discount_tax_compensation_invoiced = '" + discountTaxCompensationInvoiced + '\'' +
                        ",gw_base_tax_amount = '" + gwBaseTaxAmount + '\'' +
                        ",base_cost = '" + baseCost + '\'' +
                        ",gift_message_id = '" + giftMessageId + '\'' +
                        ",description = '" + description + '\'' +
                        ",qty_shipped = '" + qtyShipped + '\'' +
                        ",qty_ordered = '" + qtyOrdered + '\'' +
                        ",qty_canceled = '" + qtyCanceled + '\'' +
                        ",gw_base_price_invoiced = '" + gwBasePriceInvoiced + '\'' +
                        ",gw_tax_amount_invoiced = '" + gwTaxAmountInvoiced + '\'' +
                        ",base_price = '" + basePrice + '\'' +
                        ",row_total_incl_tax = '" + rowTotalInclTax + '\'' +
                        ",locked_do_invoice = '" + lockedDoInvoice + '\'' +
                        ",qty_returned = '" + qtyReturned + '\'' +
                        ",discount_tax_compensation_refunded = '" + discountTaxCompensationRefunded + '\'' +
                        ",item_id = '" + itemId + '\'' +
                        ",tax_refunded = '" + taxRefunded + '\'' +
                        ",base_discount_tax_compensation_amount = '" + baseDiscountTaxCompensationAmount + '\'' +
                        ",product_type = '" + productType + '\'' +
                        ",base_row_invoiced = '" + baseRowInvoiced + '\'' +
                        ",is_qty_decimal = '" + isQtyDecimal + '\'' +
                        "}";
    }
}