package com.astra.astraotoshop.user.token;

import android.content.Context;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.provider.entity.TokenEntity;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/4/2018.
 */

public class TokenAdminInteractor extends BaseInteractor implements TokenContract.TokenInteractor {

    TokenContract.TokenPresenter presenter;

    public TokenAdminInteractor(Context context, TokenContract.TokenPresenter presenter) {
        super(context);
        this.presenter=presenter;
    }

    @Override
    public void requestToken(NetworkHandler networkHandler, String rolePath, TokenEntity tokenEntity, String role) {
        getGeneralNetworkManager().requestToken(networkHandler,rolePath, tokenEntity)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        token -> {
                            presenter.onTokenReceived(token,role);
                        },error->{
                            presenter.onRequestTokenFailed();
                        });
    }
}
