package com.astra.astraotoshop.user.profile;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.profile.provider.ProfileContract;
import com.astra.astraotoshop.user.profile.provider.ProfileEntity;
import com.astra.astraotoshop.user.profile.provider.ProfilePresenter;
import com.astra.astraotoshop.user.profile.provider.UpdateProfilePresenter;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.Loading;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class EditProfileActivity extends BaseActivity implements ProfileContract.ProfileView, ProfileContract.UpdateProfileView {

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.emailContainer)
    TextInputLayout emailContainer;
    @BindView(R.id.lastName)
    EditText lastName;
    @BindView(R.id.lastNameContainer)
    TextInputLayout lastNameContainer;
    @BindView(R.id.firstName)
    EditText firstName;
    @BindView(R.id.firstNameContainer)
    TextInputLayout firstNameContainer;
    @BindView(R.id.save)
    Button save;
    private ProfileContract.ProfilePresenter profilePresenter;
    private ProfileContract.UpdateProfilePresenter updatePresenter;
    private Loading loading;
    private List<Boolean> validations = new ArrayList<Boolean>() {{
        add(false);
        add(false);
        add(false);
    }};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        actionBarSetup();
        profilePresenter = new ProfilePresenter(this);
        updatePresenter = new UpdateProfilePresenter(this, getStoreCode());
        loading = new Loading(this, getLayoutInflater());
        profilePresenter.requestProfile(getStoreCode());
        loading.show(getString(R.string.message_please_wait));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    public void setProfileData(ProfileEntity profileEntity) {
        updatePresenter.setProfile(profileEntity);
        firstName.setText(profileEntity.getFirstname());
        lastName.setText(profileEntity.getLastname());
        email.setText(profileEntity.getEmail());
        save.setEnabled(true);
        loading.dismiss();
    }

    @Override
    public void showError(String message) {
        loading.dismiss();
        showToastMessage(message);
    }

    @Override
    public void showUpdateProfileMessage(boolean isSuccess) {
        loading.dismiss();
        if (isSuccess) showToastMessage("Profile anda berhasil diubah");
        else showToastMessage("Profile anda gagal diubah");
    }

    @OnTextChanged(R.id.firstName)
    public void onFirstNameChange(CharSequence charSequence, int start, int before, int count) {
        if (count > 0) firstNameContainer.setError(null);
    }

    @OnTextChanged(R.id.lastName)
    public void onLastNameChange(CharSequence charSequence, int start, int before, int count) {
        if (count > 0) lastNameContainer.setError(null);
    }

    @OnTextChanged(R.id.email)
    public void onEmailChange(CharSequence charSequence, int start, int before, int count) {
        if (count > 0) emailContainer.setError(null);
    }

    @OnClick(R.id.save)
    public void onSaveClicked() {
        if (isFormValid()) {
            updatePresenter.updateProfile(
                    getStoreCode(),
                    firstName.getText().toString(),
                    lastName.getText().toString(),
                    email.getText().toString());
            loading.show(getString(R.string.message_please_wait));
        }
    }

    private boolean isFormValid() {
        validations.set(0, isFieldValid(firstName, firstNameContainer, "Nama depan wajib diisi"));
        validations.set(1, isFieldValid(lastName, lastNameContainer, "Nama belakang wajib diisi"));
        validations.set(2, isFieldValid(email, emailContainer, "Email wajib diisi") && emailValidation());
        return !validations.contains(false);
    }

    private Boolean isFieldValid(EditText field, TextInputLayout fieldContainer, String errorMessage) {
        if (TextUtils.isEmpty(field.getText())) {
            fieldContainer.setError(errorMessage);
            return false;
        } else fieldContainer.setError(null);
        return true;
    }

    private boolean emailValidation() {
        if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            emailContainer.setError("Format email salah");
            return false;
        } else emailContainer.setError(null);
        return true;
    }

    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
