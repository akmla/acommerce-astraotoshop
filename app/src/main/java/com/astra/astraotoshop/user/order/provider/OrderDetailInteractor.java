package com.astra.astraotoshop.user.order.provider;

import com.astra.astraotoshop.user.order.OrderContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 2/14/2018.
 */

public class OrderDetailInteractor extends BaseInteractor implements OrderContract.OrderDetailInteractor {

    private OrderContract.OrderDetailPresenter presenter;

    public OrderDetailInteractor(OrderContract.OrderDetailPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestOrderDetail(String orderId,String storeCode) {
        getGeneralNetworkManager()
                .requestOrderDetail(
                        new NetworkHandler(),
                        storeCode,
                        orderId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        orderDetail -> {
                            presenter.onOrderDetailReceived(orderDetail);
                        },
                        error->presenter.onFailedReceivingOrderDetail(error)
                );
    }
}
