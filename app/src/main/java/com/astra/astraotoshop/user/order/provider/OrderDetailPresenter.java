package com.astra.astraotoshop.user.order.provider;

import com.astra.astraotoshop.user.order.OrderContract;

/**
 * Created by Henra Setia Nugraha on 2/14/2018.
 */

public class OrderDetailPresenter implements OrderContract.OrderDetailPresenter {
    private OrderContract.OrderDetailView view;
    private OrderContract.OrderDetailInteractor interactor;

    public OrderDetailPresenter(OrderContract.OrderDetailView view) {
        this.view = view;
        interactor = new OrderDetailInteractor(this);
    }

    @Override
    public void requestOrderDetail(String orderId, String storeCode) {
        interactor.requestOrderDetail(orderId, storeCode);
    }

    @Override
    public void onOrderDetailReceived(OrderDetail orderDetail) {
        view.showOrder(orderDetail);
    }

    @Override
    public void onFailedReceivingOrderDetail(Throwable error) {
        view.onError();
    }
}
