package com.astra.astraotoshop.user.review;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.review.provider.CustomerReviewPresenter;
import com.astra.astraotoshop.user.review.provider.ReviewsItem;
import com.astra.astraotoshop.utils.base.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewActivity extends BaseActivity implements CustomerReviewContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.itemCount)
    TextView itemCount;
    @BindView(R.id.list)
    RecyclerView list;

    private CustomerReviewContract.ReviewPresenter presenter;
    private CustomerReviewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_list);
        ButterKnife.bind(this);
        actionBarSetup();
        itemCount.setText("Ulasan tidak ditemukan");
        presenter = new CustomerReviewPresenter(this);
        presenter.requestReview(getStoreCode());

        adapter = new CustomerReviewAdapter("user");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateCounter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showReviews(List<ReviewsItem> reviewsItems) {
        adapter.setReviews(reviewsItems);
        updateCounter();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void updateCounter() {
        if (adapter.getItemCount() == 0) {
            itemCount.setText("Ulasan tidak ditemukan");
        } else {
            itemCount.setText(String.valueOf(adapter.getItemCount()) + " ulasan ditemukan");
        }
    }
}
