package com.astra.astraotoshop.user.voucher.provider.model;

import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("item")
    private Item item;

    @SerializedName("appointment_detail")
    private AppointmentDetail appointmentDetail;

    @SerializedName("increment_id")
    private String incrementId;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("shipping_address")
    private ShippingAddress shippingAddress;

    @SerializedName("payment_method")
    private String paymentMethod;

    @SerializedName("image")
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Item getItem() {
        return item;
    }

    public void setAppointmentDetail(AppointmentDetail appointmentDetail) {
        this.appointmentDetail = appointmentDetail;
    }

    public AppointmentDetail getAppointmentDetail() {
        return appointmentDetail;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    @Override
    public String toString() {
        return
                "Order{" +
                        "item = '" + item + '\'' +
                        ",appointment_detail = '" + appointmentDetail + '\'' +
                        ",increment_id = '" + incrementId + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",shipping_address = '" + shippingAddress + '\'' +
                        ",payment_method = '" + paymentMethod + '\'' +
                        "}";
    }
}