package com.astra.astraotoshop.user.password.provider;

import com.astra.astraotoshop.user.password.PasswordContract;

/**
 * Created by Henra Setia Nugraha on 5/16/2018.
 */

public class ResetPasswordPresenter implements PasswordContract.ResetPasswordPresenter {

    private PasswordContract.ResetPasswordView view;
    PasswordContract.ResetPasswordInteractor interactor;

    public ResetPasswordPresenter(PasswordContract.ResetPasswordView view) {
        this.view = view;
        interactor = new ResetPasswordInteractor(this);
    }

    @Override
    public void resetPassword(String email) {
        interactor.emailChecking(new IsEmailAvailable(2, email), isAvailable -> {
            if (!isAvailable) interactor.resetPassword(new ResetPassword("email_reset", 2, email));
            else view.showEmailNotExistMessage();
        }, error -> {
            view.showResetPasswordMessage(false);
        });
    }

    @Override
    public void onPasswordReset() {
        view.showResetPasswordMessage(true);
    }

    @Override
    public void onResetPasswordFail() {
        view.showResetPasswordMessage(false);
    }
}
