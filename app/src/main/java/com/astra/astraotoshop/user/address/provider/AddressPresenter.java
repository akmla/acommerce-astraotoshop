package com.astra.astraotoshop.user.address.provider;

import com.astra.astraotoshop.user.address.AddressContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 2/18/2018.
 */

public class AddressPresenter implements AddressContract.AddressPresenter {
    private AddressContract.AddressInteractor interactor;
    private AddressContract.AddressView view;
    private String storeCode;
    private RegionEntity regionEntity;
    private CityEntity cityEntity;

    public AddressPresenter(AddressContract.AddressView view, String storeCode) {
        this.view = view;
        this.storeCode = storeCode;
        interactor = new AddressInteractor(this);
    }

    @Override
    public void saveAddress(String firstName, String lastName, String postCode, String city, String regionId, String telephone, List<String> street, String district, String rtRw, boolean isDefault, String addressId, boolean isUpdate) {
        AddAddressBody addressBody =
                new AddAddressBody(
                        firstName,
                        city,
                        street,
                        postCode,
                        regionId,
                        telephone,
                        isDefault,
                        "ID",
                        district,
                        rtRw,
                        isDefault,
                        lastName
                );

        if (isUpdate)
            interactor.updateAddress(storeCode, addressId, addressBody);
        else
            interactor.saveAddress(storeCode, addressBody);
    }

    @Override
    public void onAddressSaved() {
        view.onAddressSaved();
    }

    @Override
    public void onSaveFailed() {
        view.onSaveFailed();
    }

    @Override
    public void requestRegion(String countryId) {
        interactor.requestRegion(storeCode, countryId);
    }

    @Override
    public void requestCity(String regionId) {
        interactor.requestCity(storeCode, regionId);
    }

    @Override
    public void onRegionReceived(RegionEntity region) {
        regionEntity = region;
        view.onRegionReceived(getRegionList());
    }

    @Override
    public void onCityReceived(CityEntity city) {
        cityEntity = city;
        view.onCityReceived(getCityList());
    }

    @Override
    public void onReceivingFailed() {
        view.onReceivingFailed("Terjadi kesalahan");
    }

    @Override
    public void deleteAddress(String addressId) {
        interactor.deleteAddress(storeCode,addressId);
    }

    @Override
    public void onAddressDeleted() {
        view.onAddressDeleted();
    }

    @Override
    public void onDeleteAddressFailed() {
        view.onDeleteAddressFailed();
    }

    @Override
    public RegionsItem getRegion(int position) {
        return regionEntity.getItems().get(position - 1);
    }

    @Override
    public CitiesItem getCity(int position) {
        return cityEntity.getItems().get(position - 1);
    }

    private List<String> getRegionList() {
        List<String> regions = new ArrayList<>();
        if (regionEntity != null) {
            for (RegionsItem item : regionEntity.getItems())
                regions.add(item.getDefaultName());
        }

        return regions;
    }

    private List<String> getCityList() {
        List<String> cities = new ArrayList<>();
        if (cityEntity != null) {
            for (CitiesItem item : cityEntity.getItems())
                cities.add(item.getName());
        }

        return cities;
    }
}
