package com.astra.astraotoshop.user.order.provider;

import com.astra.astraotoshop.order.history.provider.OrderAddressEntity;
import com.astra.astraotoshop.user.profile.provider.Addresses;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetail {

    @SerializedName("tax_invoiced")
    private String taxInvoiced;

    @SerializedName("subtotal_canceled")
    private Object subtotalCanceled;

    @SerializedName("customer_price_list_type")
    private Object customerPriceListType;

    @SerializedName("relation_child_id")
    private Object relationChildId;

    @SerializedName("discount_refunded")
    private Object discountRefunded;

    @SerializedName("gw_price_refunded")
    private Object gwPriceRefunded;

    @SerializedName("total_refunded")
    private Object totalRefunded;

    @SerializedName("base_tax_refunded")
    private Object baseTaxRefunded;

    @SerializedName("state")
    private String state;

    @SerializedName("gw_price")
    private String gwPrice;

    @SerializedName("gw_card_base_price_incl_tax")
    private String gwCardBasePriceInclTax;

    @SerializedName("gw_card_price")
    private String gwCardPrice;

    @SerializedName("quote_id")
    private String quoteId;

    @SerializedName("wmos_outbound")
    private String wmosOutbound;

    @SerializedName("x_forwarded_for")
    private Object xForwardedFor;

    @SerializedName("base_total_invoiced")
    private String baseTotalInvoiced;

    @SerializedName("base_rwrd_crrncy_amt_invoiced")
    private Object baseRwrdCrrncyAmtInvoiced;

    @SerializedName("base_shipping_amount")
    private String baseShippingAmount;

    @SerializedName("base_subtotal_invoiced")
    private String baseSubtotalInvoiced;

    @SerializedName("applied_rule_ids")
    private Object appliedRuleIds;

    @SerializedName("gw_items_tax_amount")
    private String gwItemsTaxAmount;

    @SerializedName("subtotal")
    private String subtotal;

    @SerializedName("gw_base_price_refunded")
    private Object gwBasePriceRefunded;

    @SerializedName("gw_items_tax_refunded")
    private Object gwItemsTaxRefunded;

    @SerializedName("customer_is_guest")
    private String customerIsGuest;

    @SerializedName("items")
    private List<ProductssItem> items;

    @SerializedName("customer_sales_org")
    private Object customerSalesOrg;

    @SerializedName("shipping_refunded")
    private Object shippingRefunded;

    @SerializedName("total_canceled")
    private Object totalCanceled;

    @SerializedName("total_item_count")
    private String totalItemCount;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("gw_card_base_price")
    private String gwCardBasePrice;

    @SerializedName("customer_prefix")
    private Object customerPrefix;

    @SerializedName("discount_description")
    private Object discountDescription;

    @SerializedName("grand_total")
    private String grandTotal;

    @SerializedName("gw_card_base_tax_refunded")
    private Object gwCardBaseTaxRefunded;

    @SerializedName("base_tax_amount")
    private String baseTaxAmount;

    @SerializedName("relation_parent_real_id")
    private Object relationParentRealId;

    @SerializedName("shipping_discount_tax_compensation_amount")
    private String shippingDiscountTaxCompensationAmount;

    @SerializedName("base_discount_tax_compensation_refunded")
    private Object baseDiscountTaxCompensationRefunded;

    @SerializedName("gw_card_base_tax_amount")
    private String gwCardBaseTaxAmount;

    @SerializedName("total_qty_ordered")
    private String totalQtyOrdered;

    @SerializedName("gw_card_tax_invoiced")
    private Object gwCardTaxInvoiced;

    @SerializedName("base_adjustment_negative")
    private Object baseAdjustmentNegative;

    @SerializedName("subtotal_refunded")
    private Object subtotalRefunded;

    @SerializedName("base_to_order_rate")
    private String baseToOrderRate;

    @SerializedName("adjustment_positive")
    private Object adjustmentPositive;

    @SerializedName("base_gift_cards_refunded")
    private Object baseGiftCardsRefunded;

    @SerializedName("gw_items_base_price_incl_tax")
    private String gwItemsBasePriceInclTax;

    @SerializedName("base_subtotal")
    private String baseSubtotal;

    @SerializedName("customer_partner_function")
    private Object customerPartnerFunction;

    @SerializedName("payment_authorization_amount")
    private Object paymentAuthorizationAmount;

    @SerializedName("base_subtotal_incl_tax")
    private String baseSubtotalInclTax;

    @SerializedName("customer_gender")
    private Object customerGender;

    @SerializedName("tax_amount")
    private String taxAmount;

    @SerializedName("customer_balance_refunded")
    private Object customerBalanceRefunded;

    @SerializedName("customer_note_notify")
    private String customerNoteNotify;

    @SerializedName("rwrd_crrncy_amnt_refunded")
    private Object rwrdCrrncyAmntRefunded;

    @SerializedName("shipping_discount_amount")
    private String shippingDiscountAmount;

    @SerializedName("ext_order_id")
    private Object extOrderId;

    @SerializedName("gw_card_price_refunded")
    private Object gwCardPriceRefunded;

    @SerializedName("discount_amount")
    private String discountAmount;

    @SerializedName("total_offline_refunded")
    private Object totalOfflineRefunded;

    @SerializedName("relation_parent_id")
    private Object relationParentId;

    @SerializedName("increment_id")
    private String incrementId;

    @SerializedName("customer_ar_overdue")
    private Object customerArOverdue;

    @SerializedName("gw_card_tax_amount")
    private String gwCardTaxAmount;

    @SerializedName("base_shipping_refunded")
    private Object baseShippingRefunded;

    @SerializedName("relation_child_real_id")
    private Object relationChildRealId;

    @SerializedName("base_shipping_tax_amount")
    private String baseShippingTaxAmount;

    @SerializedName("base_grand_total")
    private String baseGrandTotal;

    @SerializedName("billing_address_id")
    private String billingAddressId;

    @SerializedName("wmos_inbound")
    private String wmosInbound;

    @SerializedName("base_total_offline_refunded")
    private Object baseTotalOfflineRefunded;

    @SerializedName("customer_ktp_address")
    private Object customerKtpAddress;

    @SerializedName("send_email")
    private String sendEmail;

    @SerializedName("weight")
    private String weight;

    @SerializedName("shipping_canceled")
    private Object shippingCanceled;

    @SerializedName("base_customer_balance_refunded")
    private Object baseCustomerBalanceRefunded;

    @SerializedName("base_total_online_refunded")
    private Object baseTotalOnlineRefunded;

    @SerializedName("base_shipping_discount_tax_compensation_amnt")
    private Object baseShippingDiscountTaxCompensationAmnt;

    @SerializedName("sap_export")
    private String sapExport;

    @SerializedName("base_discount_canceled")
    private Object baseDiscountCanceled;

    @SerializedName("base_discount_invoiced")
    private String baseDiscountInvoiced;

    @SerializedName("base_to_global_rate")
    private String baseToGlobalRate;

    @SerializedName("gw_items_price_refunded")
    private Object gwItemsPriceRefunded;

    @SerializedName("gift_cards_refunded")
    private Object giftCardsRefunded;

    @SerializedName("base_total_invoiced_cost")
    private String baseTotalInvoicedCost;

    @SerializedName("email_sent")
    private String emailSent;

    @SerializedName("discount_tax_compensation_invoiced")
    private String discountTaxCompensationInvoiced;

    @SerializedName("base_tax_canceled")
    private Object baseTaxCanceled;

    @SerializedName("gw_base_tax_amount")
    private String gwBaseTaxAmount;

    @SerializedName("gift_message_id")
    private Object giftMessageId;

    @SerializedName("base_customer_balance_amount")
    private Object baseCustomerBalanceAmount;

    @SerializedName("gw_card_base_price_refunded")
    private Object gwCardBasePriceRefunded;

    @SerializedName("gw_items_base_tax_refunded")
    private Object gwItemsBaseTaxRefunded;

    @SerializedName("gw_tax_amount_invoiced")
    private Object gwTaxAmountInvoiced;

    @SerializedName("base_subtotal_refunded")
    private Object baseSubtotalRefunded;

    @SerializedName("base_total_paid")
    private String baseTotalPaid;

    @SerializedName("customer_middlename")
    private Object customerMiddlename;

    @SerializedName("base_rwrd_crrncy_amnt_refnded")
    private Object baseRwrdCrrncyAmntRefnded;

    @SerializedName("shipping_description")
    private String shippingDescription;

    @SerializedName("gw_base_price_incl_tax")
    private String gwBasePriceInclTax;

    @SerializedName("tax_refunded")
    private Object taxRefunded;

    @SerializedName("store_to_order_rate")
    private Object storeToOrderRate;

    @SerializedName("gift_cards")
    private String giftCards;

    @SerializedName("customer_dob")
    private Object customerDob;

    @SerializedName("base_total_due")
    private String baseTotalDue;

    @SerializedName("shipping_incl_tax")
    private String shippingInclTax;

    @SerializedName("customer_division")
    private Object customerDivision;

    @SerializedName("customer_block_flag")
    private Object customerBlockFlag;

    @SerializedName("customer_firstname")
    private String customerFirstname;

    @SerializedName("base_discount_tax_compensation_invoiced")
    private String baseDiscountTaxCompensationInvoiced;

    @SerializedName("coupon_rule_name")
    private Object couponRuleName;

    @SerializedName("hold_before_status")
    private Object holdBeforeStatus;

    @SerializedName("tax_canceled")
    private Object taxCanceled;

    @SerializedName("gw_tax_amount")
    private String gwTaxAmount;

    @SerializedName("gw_items_price_incl_tax")
    private String gwItemsPriceInclTax;

    @SerializedName("gw_items_price_invoiced")
    private Object gwItemsPriceInvoiced;

    @SerializedName("base_gift_cards_invoiced")
    private Object baseGiftCardsInvoiced;

    @SerializedName("total_paid")
    private String totalPaid;

    @SerializedName("shipping_method")
    private String shippingMethod;

    @SerializedName("base_discount_refunded")
    private Object baseDiscountRefunded;

    @SerializedName("gw_card_price_incl_tax")
    private String gwCardPriceInclTax;

    @SerializedName("customer_lastname")
    private String customerLastname;

    @SerializedName("base_total_canceled")
    private Object baseTotalCanceled;

    @SerializedName("shipping_tax_refunded")
    private Object shippingTaxRefunded;

    @SerializedName("customer_taxvat")
    private Object customerTaxvat;

    @SerializedName("gw_id")
    private Object gwId;

    @SerializedName("base_subtotal_canceled")
    private Object baseSubtotalCanceled;

    @SerializedName("entity_id")
    private String entityId;

    @SerializedName("gw_items_base_price_refunded")
    private Object gwItemsBasePriceRefunded;

    @SerializedName("is_djp_exported")
    private String isDjpExported;

    @SerializedName("subtotal_incl_tax")
    private String subtotalInclTax;

    @SerializedName("base_shipping_incl_tax")
    private String baseShippingInclTax;

    @SerializedName("doku_paid")
    private String dokuPaid;

    @SerializedName("status")
    private String status;

    @SerializedName("base_total_qty_ordered")
    private Object baseTotalQtyOrdered;

    @SerializedName("global_currency_code")
    private String globalCurrencyCode;

    @SerializedName("gw_tax_amount_refunded")
    private Object gwTaxAmountRefunded;

    @SerializedName("can_ship_partially")
    private Object canShipPartially;

    @SerializedName("reward_points_balance")
    private Object rewardPointsBalance;

    @SerializedName("customer_suffix")
    private Object customerSuffix;

    @SerializedName("store_currency_code")
    private String storeCurrencyCode;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("gw_items_tax_invoiced")
    private Object gwItemsTaxInvoiced;

    @SerializedName("rwrd_currency_amount_invoiced")
    private Object rwrdCurrencyAmountInvoiced;

    @SerializedName("gw_card_tax_refunded")
    private Object gwCardTaxRefunded;

    @SerializedName("total_online_refunded")
    private Object totalOnlineRefunded;

    @SerializedName("shipping_tax_amount")
    private String shippingTaxAmount;

    @SerializedName("hold_before_state")
    private Object holdBeforeState;

    @SerializedName("store_to_base_rate")
    private Object storeToBaseRate;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("gw_card_price_invoiced")
    private Object gwCardPriceInvoiced;

    @SerializedName("coupon_code")
    private Object couponCode;

    @SerializedName("reward_points_balance_refund")
    private Object rewardPointsBalanceRefund;

    @SerializedName("customer_djp")
    private Object customerDjp;

    @SerializedName("gw_price_incl_tax")
    private String gwPriceInclTax;

    @SerializedName("base_discount_amount")
    private String baseDiscountAmount;

    @SerializedName("gw_base_tax_amount_invoiced")
    private Object gwBaseTaxAmountInvoiced;

    @SerializedName("customer_dist_channel")
    private Object customerDistChannel;

    @SerializedName("shipping_amount")
    private String shippingAmount;

    @SerializedName("subtotal_invoiced")
    private String subtotalInvoiced;

    @SerializedName("customer_balance_amount")
    private Object customerBalanceAmount;

    @SerializedName("gw_base_tax_amount_refunded")
    private Object gwBaseTaxAmountRefunded;

    @SerializedName("customer_id")
    private String customerId;

    @SerializedName("discount_invoiced")
    private String discountInvoiced;

    @SerializedName("customer_group_id")
    private String customerGroupId;

    @SerializedName("base_gift_cards_amount")
    private String baseGiftCardsAmount;

    @SerializedName("base_tax_invoiced")
    private String baseTaxInvoiced;

    @SerializedName("base_shipping_invoiced")
    private String baseShippingInvoiced;

    @SerializedName("gw_add_card")
    private Object gwAddCard;

    @SerializedName("gw_items_base_price")
    private String gwItemsBasePrice;

    @SerializedName("faktur_pajak_number")
    private String fakturPajakNumber;

    @SerializedName("customer_note")
    private Object customerNote;

    @SerializedName("base_total_refunded")
    private Object baseTotalRefunded;

    @SerializedName("can_ship_partially_item")
    private Object canShipPartiallyItem;

    @SerializedName("reward_currency_amount")
    private Object rewardCurrencyAmount;

    @SerializedName("adjustment_negative")
    private Object adjustmentNegative;

    @SerializedName("remote_ip")
    private String remoteIp;

    @SerializedName("forced_shipment_with_invoice")
    private Object forcedShipmentWithInvoice;

    @SerializedName("payment_method")
    private String paymentMethod;

    @SerializedName("shipping_address_id")
    private String shippingAddressId;

    @SerializedName("shipping_invoiced")
    private String shippingInvoiced;

    @SerializedName("gw_base_price")
    private String gwBasePrice;

    @SerializedName("discount_tax_compensation_amount")
    private String discountTaxCompensationAmount;

    @SerializedName("gw_items_base_price_invoiced")
    private Object gwItemsBasePriceInvoiced;

    @SerializedName("gw_price_invoiced")
    private Object gwPriceInvoiced;

    @SerializedName("gift_cards_amount")
    private String giftCardsAmount;

    @SerializedName("base_shipping_tax_refunded")
    private Object baseShippingTaxRefunded;

    @SerializedName("doku_export")
    private String dokuExport;

    @SerializedName("gw_items_base_tax_invoiced")
    private Object gwItemsBaseTaxInvoiced;

    @SerializedName("customer_email")
    private String customerEmail;

    @SerializedName("total_invoiced")
    private String totalInvoiced;

    @SerializedName("original_increment_id")
    private Object originalIncrementId;

    @SerializedName("bs_customer_bal_total_refunded")
    private Object bsCustomerBalTotalRefunded;

    @SerializedName("gw_card_base_tax_invoiced")
    private Object gwCardBaseTaxInvoiced;

    @SerializedName("is_virtual")
    private String isVirtual;

    @SerializedName("base_adjustment_positive")
    private Object baseAdjustmentPositive;

    @SerializedName("base_shipping_canceled")
    private Object baseShippingCanceled;

    @SerializedName("paypal_ipn_customer_notified")
    private String paypalIpnCustomerNotified;

    @SerializedName("discount_canceled")
    private Object discountCanceled;

    @SerializedName("base_customer_balance_invoiced")
    private Object baseCustomerBalanceInvoiced;

    @SerializedName("ext_customer_id")
    private Object extCustomerId;

    @SerializedName("faktur_pajak_pdf_attachment_path")
    private Object fakturPajakPdfAttachmentPath;

    @SerializedName("gw_base_price_invoiced")
    private Object gwBasePriceInvoiced;

    @SerializedName("base_shipping_discount_amount")
    private String baseShippingDiscountAmount;

    @SerializedName("gw_items_price")
    private String gwItemsPrice;

    @SerializedName("store_name")
    private String storeName;

    @SerializedName("base_reward_currency_amount")
    private Object baseRewardCurrencyAmount;

    @SerializedName("base_currency_code")
    private String baseCurrencyCode;

    @SerializedName("customer_bal_total_refunded")
    private Object customerBalTotalRefunded;

    @SerializedName("is_appointment")
    private String isAppointment;

    @SerializedName("discount_tax_compensation_refunded")
    private Object discountTaxCompensationRefunded;

    @SerializedName("gw_card_base_price_invoiced")
    private Object gwCardBasePriceInvoiced;

    @SerializedName("customer_balance_invoiced")
    private Object customerBalanceInvoiced;

    @SerializedName("customer_npwp")
    private Object customerNpwp;

    @SerializedName("total_due")
    private String totalDue;

    @SerializedName("gift_cards_invoiced")
    private Object giftCardsInvoiced;

    @SerializedName("gw_items_base_tax_amount")
    private String gwItemsBaseTaxAmount;

    @SerializedName("customer_ktp")
    private Object customerKtp;

    @SerializedName("edit_increment")
    private Object editIncrement;

    @SerializedName("base_discount_tax_compensation_amount")
    private String baseDiscountTaxCompensationAmount;

    @SerializedName("protect_code")
    private String protectCode;

    @SerializedName("order_currency_code")
    private String orderCurrencyCode;

    @SerializedName("gw_allow_gift_receipt")
    private Object gwAllowGiftReceipt;

    @SerializedName("quote_address_id")
    private Object quoteAddressId;

    @SerializedName("payment_auth_expiration")
    private Object paymentAuthExpiration;

    @SerializedName("shipping_address")
    private OrderAddressEntity addresses;

    @SerializedName("va_number")
    public String vaNumber;

    public String getVaNumber() {
        return vaNumber;
    }

    public void setVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
    }

    public OrderAddressEntity getAddresses() {
        return addresses;
    }

    public void setAddresses(OrderAddressEntity addresses) {
        this.addresses = addresses;
    }

    public void setTaxInvoiced(String taxInvoiced) {
        this.taxInvoiced = taxInvoiced;
    }

    public String getTaxInvoiced() {
        return taxInvoiced;
    }

    public void setSubtotalCanceled(Object subtotalCanceled) {
        this.subtotalCanceled = subtotalCanceled;
    }

    public Object getSubtotalCanceled() {
        return subtotalCanceled;
    }

    public void setCustomerPriceListType(Object customerPriceListType) {
        this.customerPriceListType = customerPriceListType;
    }

    public Object getCustomerPriceListType() {
        return customerPriceListType;
    }

    public void setRelationChildId(Object relationChildId) {
        this.relationChildId = relationChildId;
    }

    public Object getRelationChildId() {
        return relationChildId;
    }

    public void setDiscountRefunded(Object discountRefunded) {
        this.discountRefunded = discountRefunded;
    }

    public Object getDiscountRefunded() {
        return discountRefunded;
    }

    public void setGwPriceRefunded(Object gwPriceRefunded) {
        this.gwPriceRefunded = gwPriceRefunded;
    }

    public Object getGwPriceRefunded() {
        return gwPriceRefunded;
    }

    public void setTotalRefunded(Object totalRefunded) {
        this.totalRefunded = totalRefunded;
    }

    public Object getTotalRefunded() {
        return totalRefunded;
    }

    public void setBaseTaxRefunded(Object baseTaxRefunded) {
        this.baseTaxRefunded = baseTaxRefunded;
    }

    public Object getBaseTaxRefunded() {
        return baseTaxRefunded;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setGwPrice(String gwPrice) {
        this.gwPrice = gwPrice;
    }

    public String getGwPrice() {
        return gwPrice;
    }

    public void setGwCardBasePriceInclTax(String gwCardBasePriceInclTax) {
        this.gwCardBasePriceInclTax = gwCardBasePriceInclTax;
    }

    public String getGwCardBasePriceInclTax() {
        return gwCardBasePriceInclTax;
    }

    public void setGwCardPrice(String gwCardPrice) {
        this.gwCardPrice = gwCardPrice;
    }

    public String getGwCardPrice() {
        return gwCardPrice;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setWmosOutbound(String wmosOutbound) {
        this.wmosOutbound = wmosOutbound;
    }

    public String getWmosOutbound() {
        return wmosOutbound;
    }

    public void setXForwardedFor(Object xForwardedFor) {
        this.xForwardedFor = xForwardedFor;
    }

    public Object getXForwardedFor() {
        return xForwardedFor;
    }

    public void setBaseTotalInvoiced(String baseTotalInvoiced) {
        this.baseTotalInvoiced = baseTotalInvoiced;
    }

    public String getBaseTotalInvoiced() {
        return baseTotalInvoiced;
    }

    public void setBaseRwrdCrrncyAmtInvoiced(Object baseRwrdCrrncyAmtInvoiced) {
        this.baseRwrdCrrncyAmtInvoiced = baseRwrdCrrncyAmtInvoiced;
    }

    public Object getBaseRwrdCrrncyAmtInvoiced() {
        return baseRwrdCrrncyAmtInvoiced;
    }

    public void setBaseShippingAmount(String baseShippingAmount) {
        this.baseShippingAmount = baseShippingAmount;
    }

    public String getBaseShippingAmount() {
        return baseShippingAmount;
    }

    public void setBaseSubtotalInvoiced(String baseSubtotalInvoiced) {
        this.baseSubtotalInvoiced = baseSubtotalInvoiced;
    }

    public String getBaseSubtotalInvoiced() {
        return baseSubtotalInvoiced;
    }

    public void setAppliedRuleIds(Object appliedRuleIds) {
        this.appliedRuleIds = appliedRuleIds;
    }

    public Object getAppliedRuleIds() {
        return appliedRuleIds;
    }

    public void setGwItemsTaxAmount(String gwItemsTaxAmount) {
        this.gwItemsTaxAmount = gwItemsTaxAmount;
    }

    public String getGwItemsTaxAmount() {
        return gwItemsTaxAmount;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setGwBasePriceRefunded(Object gwBasePriceRefunded) {
        this.gwBasePriceRefunded = gwBasePriceRefunded;
    }

    public Object getGwBasePriceRefunded() {
        return gwBasePriceRefunded;
    }

    public void setGwItemsTaxRefunded(Object gwItemsTaxRefunded) {
        this.gwItemsTaxRefunded = gwItemsTaxRefunded;
    }

    public Object getGwItemsTaxRefunded() {
        return gwItemsTaxRefunded;
    }

    public void setCustomerIsGuest(String customerIsGuest) {
        this.customerIsGuest = customerIsGuest;
    }

    public String getCustomerIsGuest() {
        return customerIsGuest;
    }

    public void setItems(List<ProductssItem> items) {
        this.items = items;
    }

    public List<ProductssItem> getItems() {
        return items;
    }

    public void setCustomerSalesOrg(Object customerSalesOrg) {
        this.customerSalesOrg = customerSalesOrg;
    }

    public Object getCustomerSalesOrg() {
        return customerSalesOrg;
    }

    public void setShippingRefunded(Object shippingRefunded) {
        this.shippingRefunded = shippingRefunded;
    }

    public Object getShippingRefunded() {
        return shippingRefunded;
    }

    public void setTotalCanceled(Object totalCanceled) {
        this.totalCanceled = totalCanceled;
    }

    public Object getTotalCanceled() {
        return totalCanceled;
    }

    public void setTotalItemCount(String totalItemCount) {
        this.totalItemCount = totalItemCount;
    }

    public String getTotalItemCount() {
        return totalItemCount;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setGwCardBasePrice(String gwCardBasePrice) {
        this.gwCardBasePrice = gwCardBasePrice;
    }

    public String getGwCardBasePrice() {
        return gwCardBasePrice;
    }

    public void setCustomerPrefix(Object customerPrefix) {
        this.customerPrefix = customerPrefix;
    }

    public Object getCustomerPrefix() {
        return customerPrefix;
    }

    public void setDiscountDescription(Object discountDescription) {
        this.discountDescription = discountDescription;
    }

    public Object getDiscountDescription() {
        return discountDescription;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGwCardBaseTaxRefunded(Object gwCardBaseTaxRefunded) {
        this.gwCardBaseTaxRefunded = gwCardBaseTaxRefunded;
    }

    public Object getGwCardBaseTaxRefunded() {
        return gwCardBaseTaxRefunded;
    }

    public void setBaseTaxAmount(String baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }

    public String getBaseTaxAmount() {
        return baseTaxAmount;
    }

    public void setRelationParentRealId(Object relationParentRealId) {
        this.relationParentRealId = relationParentRealId;
    }

    public Object getRelationParentRealId() {
        return relationParentRealId;
    }

    public void setShippingDiscountTaxCompensationAmount(String shippingDiscountTaxCompensationAmount) {
        this.shippingDiscountTaxCompensationAmount = shippingDiscountTaxCompensationAmount;
    }

    public String getShippingDiscountTaxCompensationAmount() {
        return shippingDiscountTaxCompensationAmount;
    }

    public void setBaseDiscountTaxCompensationRefunded(Object baseDiscountTaxCompensationRefunded) {
        this.baseDiscountTaxCompensationRefunded = baseDiscountTaxCompensationRefunded;
    }

    public Object getBaseDiscountTaxCompensationRefunded() {
        return baseDiscountTaxCompensationRefunded;
    }

    public void setGwCardBaseTaxAmount(String gwCardBaseTaxAmount) {
        this.gwCardBaseTaxAmount = gwCardBaseTaxAmount;
    }

    public String getGwCardBaseTaxAmount() {
        return gwCardBaseTaxAmount;
    }

    public void setTotalQtyOrdered(String totalQtyOrdered) {
        this.totalQtyOrdered = totalQtyOrdered;
    }

    public String getTotalQtyOrdered() {
        return totalQtyOrdered;
    }

    public void setGwCardTaxInvoiced(Object gwCardTaxInvoiced) {
        this.gwCardTaxInvoiced = gwCardTaxInvoiced;
    }

    public Object getGwCardTaxInvoiced() {
        return gwCardTaxInvoiced;
    }

    public void setBaseAdjustmentNegative(Object baseAdjustmentNegative) {
        this.baseAdjustmentNegative = baseAdjustmentNegative;
    }

    public Object getBaseAdjustmentNegative() {
        return baseAdjustmentNegative;
    }

    public void setSubtotalRefunded(Object subtotalRefunded) {
        this.subtotalRefunded = subtotalRefunded;
    }

    public Object getSubtotalRefunded() {
        return subtotalRefunded;
    }

    public void setBaseToOrderRate(String baseToOrderRate) {
        this.baseToOrderRate = baseToOrderRate;
    }

    public String getBaseToOrderRate() {
        return baseToOrderRate;
    }

    public void setAdjustmentPositive(Object adjustmentPositive) {
        this.adjustmentPositive = adjustmentPositive;
    }

    public Object getAdjustmentPositive() {
        return adjustmentPositive;
    }

    public void setBaseGiftCardsRefunded(Object baseGiftCardsRefunded) {
        this.baseGiftCardsRefunded = baseGiftCardsRefunded;
    }

    public Object getBaseGiftCardsRefunded() {
        return baseGiftCardsRefunded;
    }

    public void setGwItemsBasePriceInclTax(String gwItemsBasePriceInclTax) {
        this.gwItemsBasePriceInclTax = gwItemsBasePriceInclTax;
    }

    public String getGwItemsBasePriceInclTax() {
        return gwItemsBasePriceInclTax;
    }

    public void setBaseSubtotal(String baseSubtotal) {
        this.baseSubtotal = baseSubtotal;
    }

    public String getBaseSubtotal() {
        return baseSubtotal;
    }

    public void setCustomerPartnerFunction(Object customerPartnerFunction) {
        this.customerPartnerFunction = customerPartnerFunction;
    }

    public Object getCustomerPartnerFunction() {
        return customerPartnerFunction;
    }

    public void setPaymentAuthorizationAmount(Object paymentAuthorizationAmount) {
        this.paymentAuthorizationAmount = paymentAuthorizationAmount;
    }

    public Object getPaymentAuthorizationAmount() {
        return paymentAuthorizationAmount;
    }

    public void setBaseSubtotalInclTax(String baseSubtotalInclTax) {
        this.baseSubtotalInclTax = baseSubtotalInclTax;
    }

    public String getBaseSubtotalInclTax() {
        return baseSubtotalInclTax;
    }

    public void setCustomerGender(Object customerGender) {
        this.customerGender = customerGender;
    }

    public Object getCustomerGender() {
        return customerGender;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setCustomerBalanceRefunded(Object customerBalanceRefunded) {
        this.customerBalanceRefunded = customerBalanceRefunded;
    }

    public Object getCustomerBalanceRefunded() {
        return customerBalanceRefunded;
    }

    public void setCustomerNoteNotify(String customerNoteNotify) {
        this.customerNoteNotify = customerNoteNotify;
    }

    public String getCustomerNoteNotify() {
        return customerNoteNotify;
    }

    public void setRwrdCrrncyAmntRefunded(Object rwrdCrrncyAmntRefunded) {
        this.rwrdCrrncyAmntRefunded = rwrdCrrncyAmntRefunded;
    }

    public Object getRwrdCrrncyAmntRefunded() {
        return rwrdCrrncyAmntRefunded;
    }

    public void setShippingDiscountAmount(String shippingDiscountAmount) {
        this.shippingDiscountAmount = shippingDiscountAmount;
    }

    public String getShippingDiscountAmount() {
        return shippingDiscountAmount;
    }

    public void setExtOrderId(Object extOrderId) {
        this.extOrderId = extOrderId;
    }

    public Object getExtOrderId() {
        return extOrderId;
    }

    public void setGwCardPriceRefunded(Object gwCardPriceRefunded) {
        this.gwCardPriceRefunded = gwCardPriceRefunded;
    }

    public Object getGwCardPriceRefunded() {
        return gwCardPriceRefunded;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setTotalOfflineRefunded(Object totalOfflineRefunded) {
        this.totalOfflineRefunded = totalOfflineRefunded;
    }

    public Object getTotalOfflineRefunded() {
        return totalOfflineRefunded;
    }

    public void setRelationParentId(Object relationParentId) {
        this.relationParentId = relationParentId;
    }

    public Object getRelationParentId() {
        return relationParentId;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setCustomerArOverdue(Object customerArOverdue) {
        this.customerArOverdue = customerArOverdue;
    }

    public Object getCustomerArOverdue() {
        return customerArOverdue;
    }

    public void setGwCardTaxAmount(String gwCardTaxAmount) {
        this.gwCardTaxAmount = gwCardTaxAmount;
    }

    public String getGwCardTaxAmount() {
        return gwCardTaxAmount;
    }

    public void setBaseShippingRefunded(Object baseShippingRefunded) {
        this.baseShippingRefunded = baseShippingRefunded;
    }

    public Object getBaseShippingRefunded() {
        return baseShippingRefunded;
    }

    public void setRelationChildRealId(Object relationChildRealId) {
        this.relationChildRealId = relationChildRealId;
    }

    public Object getRelationChildRealId() {
        return relationChildRealId;
    }

    public void setBaseShippingTaxAmount(String baseShippingTaxAmount) {
        this.baseShippingTaxAmount = baseShippingTaxAmount;
    }

    public String getBaseShippingTaxAmount() {
        return baseShippingTaxAmount;
    }

    public void setBaseGrandTotal(String baseGrandTotal) {
        this.baseGrandTotal = baseGrandTotal;
    }

    public String getBaseGrandTotal() {
        return baseGrandTotal;
    }

    public void setBillingAddressId(String billingAddressId) {
        this.billingAddressId = billingAddressId;
    }

    public String getBillingAddressId() {
        return billingAddressId;
    }

    public void setWmosInbound(String wmosInbound) {
        this.wmosInbound = wmosInbound;
    }

    public String getWmosInbound() {
        return wmosInbound;
    }

    public void setBaseTotalOfflineRefunded(Object baseTotalOfflineRefunded) {
        this.baseTotalOfflineRefunded = baseTotalOfflineRefunded;
    }

    public Object getBaseTotalOfflineRefunded() {
        return baseTotalOfflineRefunded;
    }

    public void setCustomerKtpAddress(Object customerKtpAddress) {
        this.customerKtpAddress = customerKtpAddress;
    }

    public Object getCustomerKtpAddress() {
        return customerKtpAddress;
    }

    public void setSendEmail(String sendEmail) {
        this.sendEmail = sendEmail;
    }

    public String getSendEmail() {
        return sendEmail;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeight() {
        return weight;
    }

    public void setShippingCanceled(Object shippingCanceled) {
        this.shippingCanceled = shippingCanceled;
    }

    public Object getShippingCanceled() {
        return shippingCanceled;
    }

    public void setBaseCustomerBalanceRefunded(Object baseCustomerBalanceRefunded) {
        this.baseCustomerBalanceRefunded = baseCustomerBalanceRefunded;
    }

    public Object getBaseCustomerBalanceRefunded() {
        return baseCustomerBalanceRefunded;
    }

    public void setBaseTotalOnlineRefunded(Object baseTotalOnlineRefunded) {
        this.baseTotalOnlineRefunded = baseTotalOnlineRefunded;
    }

    public Object getBaseTotalOnlineRefunded() {
        return baseTotalOnlineRefunded;
    }

    public void setBaseShippingDiscountTaxCompensationAmnt(Object baseShippingDiscountTaxCompensationAmnt) {
        this.baseShippingDiscountTaxCompensationAmnt = baseShippingDiscountTaxCompensationAmnt;
    }

    public Object getBaseShippingDiscountTaxCompensationAmnt() {
        return baseShippingDiscountTaxCompensationAmnt;
    }

    public void setSapExport(String sapExport) {
        this.sapExport = sapExport;
    }

    public String getSapExport() {
        return sapExport;
    }

    public void setBaseDiscountCanceled(Object baseDiscountCanceled) {
        this.baseDiscountCanceled = baseDiscountCanceled;
    }

    public Object getBaseDiscountCanceled() {
        return baseDiscountCanceled;
    }

    public void setBaseDiscountInvoiced(String baseDiscountInvoiced) {
        this.baseDiscountInvoiced = baseDiscountInvoiced;
    }

    public String getBaseDiscountInvoiced() {
        return baseDiscountInvoiced;
    }

    public void setBaseToGlobalRate(String baseToGlobalRate) {
        this.baseToGlobalRate = baseToGlobalRate;
    }

    public String getBaseToGlobalRate() {
        return baseToGlobalRate;
    }

    public void setGwItemsPriceRefunded(Object gwItemsPriceRefunded) {
        this.gwItemsPriceRefunded = gwItemsPriceRefunded;
    }

    public Object getGwItemsPriceRefunded() {
        return gwItemsPriceRefunded;
    }

    public void setGiftCardsRefunded(Object giftCardsRefunded) {
        this.giftCardsRefunded = giftCardsRefunded;
    }

    public Object getGiftCardsRefunded() {
        return giftCardsRefunded;
    }

    public void setBaseTotalInvoicedCost(String baseTotalInvoicedCost) {
        this.baseTotalInvoicedCost = baseTotalInvoicedCost;
    }

    public String getBaseTotalInvoicedCost() {
        return baseTotalInvoicedCost;
    }

    public void setEmailSent(String emailSent) {
        this.emailSent = emailSent;
    }

    public String getEmailSent() {
        return emailSent;
    }

    public void setDiscountTaxCompensationInvoiced(String discountTaxCompensationInvoiced) {
        this.discountTaxCompensationInvoiced = discountTaxCompensationInvoiced;
    }

    public String getDiscountTaxCompensationInvoiced() {
        return discountTaxCompensationInvoiced;
    }

    public void setBaseTaxCanceled(Object baseTaxCanceled) {
        this.baseTaxCanceled = baseTaxCanceled;
    }

    public Object getBaseTaxCanceled() {
        return baseTaxCanceled;
    }

    public void setGwBaseTaxAmount(String gwBaseTaxAmount) {
        this.gwBaseTaxAmount = gwBaseTaxAmount;
    }

    public String getGwBaseTaxAmount() {
        return gwBaseTaxAmount;
    }

    public void setGiftMessageId(Object giftMessageId) {
        this.giftMessageId = giftMessageId;
    }

    public Object getGiftMessageId() {
        return giftMessageId;
    }

    public void setBaseCustomerBalanceAmount(Object baseCustomerBalanceAmount) {
        this.baseCustomerBalanceAmount = baseCustomerBalanceAmount;
    }

    public Object getBaseCustomerBalanceAmount() {
        return baseCustomerBalanceAmount;
    }

    public void setGwCardBasePriceRefunded(Object gwCardBasePriceRefunded) {
        this.gwCardBasePriceRefunded = gwCardBasePriceRefunded;
    }

    public Object getGwCardBasePriceRefunded() {
        return gwCardBasePriceRefunded;
    }

    public void setGwItemsBaseTaxRefunded(Object gwItemsBaseTaxRefunded) {
        this.gwItemsBaseTaxRefunded = gwItemsBaseTaxRefunded;
    }

    public Object getGwItemsBaseTaxRefunded() {
        return gwItemsBaseTaxRefunded;
    }

    public void setGwTaxAmountInvoiced(Object gwTaxAmountInvoiced) {
        this.gwTaxAmountInvoiced = gwTaxAmountInvoiced;
    }

    public Object getGwTaxAmountInvoiced() {
        return gwTaxAmountInvoiced;
    }

    public void setBaseSubtotalRefunded(Object baseSubtotalRefunded) {
        this.baseSubtotalRefunded = baseSubtotalRefunded;
    }

    public Object getBaseSubtotalRefunded() {
        return baseSubtotalRefunded;
    }

    public void setBaseTotalPaid(String baseTotalPaid) {
        this.baseTotalPaid = baseTotalPaid;
    }

    public String getBaseTotalPaid() {
        return baseTotalPaid;
    }

    public void setCustomerMiddlename(Object customerMiddlename) {
        this.customerMiddlename = customerMiddlename;
    }

    public Object getCustomerMiddlename() {
        return customerMiddlename;
    }

    public void setBaseRwrdCrrncyAmntRefnded(Object baseRwrdCrrncyAmntRefnded) {
        this.baseRwrdCrrncyAmntRefnded = baseRwrdCrrncyAmntRefnded;
    }

    public Object getBaseRwrdCrrncyAmntRefnded() {
        return baseRwrdCrrncyAmntRefnded;
    }

    public void setShippingDescription(String shippingDescription) {
        this.shippingDescription = shippingDescription;
    }

    public String getShippingDescription() {
        return shippingDescription;
    }

    public void setGwBasePriceInclTax(String gwBasePriceInclTax) {
        this.gwBasePriceInclTax = gwBasePriceInclTax;
    }

    public String getGwBasePriceInclTax() {
        return gwBasePriceInclTax;
    }

    public void setTaxRefunded(Object taxRefunded) {
        this.taxRefunded = taxRefunded;
    }

    public Object getTaxRefunded() {
        return taxRefunded;
    }

    public void setStoreToOrderRate(Object storeToOrderRate) {
        this.storeToOrderRate = storeToOrderRate;
    }

    public Object getStoreToOrderRate() {
        return storeToOrderRate;
    }

    public void setGiftCards(String giftCards) {
        this.giftCards = giftCards;
    }

    public String getGiftCards() {
        return giftCards;
    }

    public void setCustomerDob(Object customerDob) {
        this.customerDob = customerDob;
    }

    public Object getCustomerDob() {
        return customerDob;
    }

    public void setBaseTotalDue(String baseTotalDue) {
        this.baseTotalDue = baseTotalDue;
    }

    public String getBaseTotalDue() {
        return baseTotalDue;
    }

    public void setShippingInclTax(String shippingInclTax) {
        this.shippingInclTax = shippingInclTax;
    }

    public String getShippingInclTax() {
        return shippingInclTax;
    }

    public void setCustomerDivision(Object customerDivision) {
        this.customerDivision = customerDivision;
    }

    public Object getCustomerDivision() {
        return customerDivision;
    }

    public void setCustomerBlockFlag(Object customerBlockFlag) {
        this.customerBlockFlag = customerBlockFlag;
    }

    public Object getCustomerBlockFlag() {
        return customerBlockFlag;
    }

    public void setCustomerFirstname(String customerFirstname) {
        this.customerFirstname = customerFirstname;
    }

    public String getCustomerFirstname() {
        return customerFirstname;
    }

    public void setBaseDiscountTaxCompensationInvoiced(String baseDiscountTaxCompensationInvoiced) {
        this.baseDiscountTaxCompensationInvoiced = baseDiscountTaxCompensationInvoiced;
    }

    public String getBaseDiscountTaxCompensationInvoiced() {
        return baseDiscountTaxCompensationInvoiced;
    }

    public void setCouponRuleName(Object couponRuleName) {
        this.couponRuleName = couponRuleName;
    }

    public Object getCouponRuleName() {
        return couponRuleName;
    }

    public void setHoldBeforeStatus(Object holdBeforeStatus) {
        this.holdBeforeStatus = holdBeforeStatus;
    }

    public Object getHoldBeforeStatus() {
        return holdBeforeStatus;
    }

    public void setTaxCanceled(Object taxCanceled) {
        this.taxCanceled = taxCanceled;
    }

    public Object getTaxCanceled() {
        return taxCanceled;
    }

    public void setGwTaxAmount(String gwTaxAmount) {
        this.gwTaxAmount = gwTaxAmount;
    }

    public String getGwTaxAmount() {
        return gwTaxAmount;
    }

    public void setGwItemsPriceInclTax(String gwItemsPriceInclTax) {
        this.gwItemsPriceInclTax = gwItemsPriceInclTax;
    }

    public String getGwItemsPriceInclTax() {
        return gwItemsPriceInclTax;
    }

    public void setGwItemsPriceInvoiced(Object gwItemsPriceInvoiced) {
        this.gwItemsPriceInvoiced = gwItemsPriceInvoiced;
    }

    public Object getGwItemsPriceInvoiced() {
        return gwItemsPriceInvoiced;
    }

    public void setBaseGiftCardsInvoiced(Object baseGiftCardsInvoiced) {
        this.baseGiftCardsInvoiced = baseGiftCardsInvoiced;
    }

    public Object getBaseGiftCardsInvoiced() {
        return baseGiftCardsInvoiced;
    }

    public void setTotalPaid(String totalPaid) {
        this.totalPaid = totalPaid;
    }

    public String getTotalPaid() {
        return totalPaid;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setBaseDiscountRefunded(Object baseDiscountRefunded) {
        this.baseDiscountRefunded = baseDiscountRefunded;
    }

    public Object getBaseDiscountRefunded() {
        return baseDiscountRefunded;
    }

    public void setGwCardPriceInclTax(String gwCardPriceInclTax) {
        this.gwCardPriceInclTax = gwCardPriceInclTax;
    }

    public String getGwCardPriceInclTax() {
        return gwCardPriceInclTax;
    }

    public void setCustomerLastname(String customerLastname) {
        this.customerLastname = customerLastname;
    }

    public String getCustomerLastname() {
        return customerLastname;
    }

    public void setBaseTotalCanceled(Object baseTotalCanceled) {
        this.baseTotalCanceled = baseTotalCanceled;
    }

    public Object getBaseTotalCanceled() {
        return baseTotalCanceled;
    }

    public void setShippingTaxRefunded(Object shippingTaxRefunded) {
        this.shippingTaxRefunded = shippingTaxRefunded;
    }

    public Object getShippingTaxRefunded() {
        return shippingTaxRefunded;
    }

    public void setCustomerTaxvat(Object customerTaxvat) {
        this.customerTaxvat = customerTaxvat;
    }

    public Object getCustomerTaxvat() {
        return customerTaxvat;
    }

    public void setGwId(Object gwId) {
        this.gwId = gwId;
    }

    public Object getGwId() {
        return gwId;
    }

    public void setBaseSubtotalCanceled(Object baseSubtotalCanceled) {
        this.baseSubtotalCanceled = baseSubtotalCanceled;
    }

    public Object getBaseSubtotalCanceled() {
        return baseSubtotalCanceled;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setGwItemsBasePriceRefunded(Object gwItemsBasePriceRefunded) {
        this.gwItemsBasePriceRefunded = gwItemsBasePriceRefunded;
    }

    public Object getGwItemsBasePriceRefunded() {
        return gwItemsBasePriceRefunded;
    }

    public void setIsDjpExported(String isDjpExported) {
        this.isDjpExported = isDjpExported;
    }

    public String getIsDjpExported() {
        return isDjpExported;
    }

    public void setSubtotalInclTax(String subtotalInclTax) {
        this.subtotalInclTax = subtotalInclTax;
    }

    public String getSubtotalInclTax() {
        return subtotalInclTax;
    }

    public void setBaseShippingInclTax(String baseShippingInclTax) {
        this.baseShippingInclTax = baseShippingInclTax;
    }

    public String getBaseShippingInclTax() {
        return baseShippingInclTax;
    }

    public void setDokuPaid(String dokuPaid) {
        this.dokuPaid = dokuPaid;
    }

    public String getDokuPaid() {
        return dokuPaid;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setBaseTotalQtyOrdered(Object baseTotalQtyOrdered) {
        this.baseTotalQtyOrdered = baseTotalQtyOrdered;
    }

    public Object getBaseTotalQtyOrdered() {
        return baseTotalQtyOrdered;
    }

    public void setGlobalCurrencyCode(String globalCurrencyCode) {
        this.globalCurrencyCode = globalCurrencyCode;
    }

    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    public void setGwTaxAmountRefunded(Object gwTaxAmountRefunded) {
        this.gwTaxAmountRefunded = gwTaxAmountRefunded;
    }

    public Object getGwTaxAmountRefunded() {
        return gwTaxAmountRefunded;
    }

    public void setCanShipPartially(Object canShipPartially) {
        this.canShipPartially = canShipPartially;
    }

    public Object getCanShipPartially() {
        return canShipPartially;
    }

    public void setRewardPointsBalance(Object rewardPointsBalance) {
        this.rewardPointsBalance = rewardPointsBalance;
    }

    public Object getRewardPointsBalance() {
        return rewardPointsBalance;
    }

    public void setCustomerSuffix(Object customerSuffix) {
        this.customerSuffix = customerSuffix;
    }

    public Object getCustomerSuffix() {
        return customerSuffix;
    }

    public void setStoreCurrencyCode(String storeCurrencyCode) {
        this.storeCurrencyCode = storeCurrencyCode;
    }

    public String getStoreCurrencyCode() {
        return storeCurrencyCode;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setGwItemsTaxInvoiced(Object gwItemsTaxInvoiced) {
        this.gwItemsTaxInvoiced = gwItemsTaxInvoiced;
    }

    public Object getGwItemsTaxInvoiced() {
        return gwItemsTaxInvoiced;
    }

    public void setRwrdCurrencyAmountInvoiced(Object rwrdCurrencyAmountInvoiced) {
        this.rwrdCurrencyAmountInvoiced = rwrdCurrencyAmountInvoiced;
    }

    public Object getRwrdCurrencyAmountInvoiced() {
        return rwrdCurrencyAmountInvoiced;
    }

    public void setGwCardTaxRefunded(Object gwCardTaxRefunded) {
        this.gwCardTaxRefunded = gwCardTaxRefunded;
    }

    public Object getGwCardTaxRefunded() {
        return gwCardTaxRefunded;
    }

    public void setTotalOnlineRefunded(Object totalOnlineRefunded) {
        this.totalOnlineRefunded = totalOnlineRefunded;
    }

    public Object getTotalOnlineRefunded() {
        return totalOnlineRefunded;
    }

    public void setShippingTaxAmount(String shippingTaxAmount) {
        this.shippingTaxAmount = shippingTaxAmount;
    }

    public String getShippingTaxAmount() {
        return shippingTaxAmount;
    }

    public void setHoldBeforeState(Object holdBeforeState) {
        this.holdBeforeState = holdBeforeState;
    }

    public Object getHoldBeforeState() {
        return holdBeforeState;
    }

    public void setStoreToBaseRate(Object storeToBaseRate) {
        this.storeToBaseRate = storeToBaseRate;
    }

    public Object getStoreToBaseRate() {
        return storeToBaseRate;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setGwCardPriceInvoiced(Object gwCardPriceInvoiced) {
        this.gwCardPriceInvoiced = gwCardPriceInvoiced;
    }

    public Object getGwCardPriceInvoiced() {
        return gwCardPriceInvoiced;
    }

    public void setCouponCode(Object couponCode) {
        this.couponCode = couponCode;
    }

    public Object getCouponCode() {
        return couponCode;
    }

    public void setRewardPointsBalanceRefund(Object rewardPointsBalanceRefund) {
        this.rewardPointsBalanceRefund = rewardPointsBalanceRefund;
    }

    public Object getRewardPointsBalanceRefund() {
        return rewardPointsBalanceRefund;
    }

    public void setCustomerDjp(Object customerDjp) {
        this.customerDjp = customerDjp;
    }

    public Object getCustomerDjp() {
        return customerDjp;
    }

    public void setGwPriceInclTax(String gwPriceInclTax) {
        this.gwPriceInclTax = gwPriceInclTax;
    }

    public String getGwPriceInclTax() {
        return gwPriceInclTax;
    }

    public void setBaseDiscountAmount(String baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }

    public String getBaseDiscountAmount() {
        return baseDiscountAmount;
    }

    public void setGwBaseTaxAmountInvoiced(Object gwBaseTaxAmountInvoiced) {
        this.gwBaseTaxAmountInvoiced = gwBaseTaxAmountInvoiced;
    }

    public Object getGwBaseTaxAmountInvoiced() {
        return gwBaseTaxAmountInvoiced;
    }

    public void setCustomerDistChannel(Object customerDistChannel) {
        this.customerDistChannel = customerDistChannel;
    }

    public Object getCustomerDistChannel() {
        return customerDistChannel;
    }

    public void setShippingAmount(String shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    public String getShippingAmount() {
        return shippingAmount;
    }

    public void setSubtotalInvoiced(String subtotalInvoiced) {
        this.subtotalInvoiced = subtotalInvoiced;
    }

    public String getSubtotalInvoiced() {
        return subtotalInvoiced;
    }

    public void setCustomerBalanceAmount(Object customerBalanceAmount) {
        this.customerBalanceAmount = customerBalanceAmount;
    }

    public Object getCustomerBalanceAmount() {
        return customerBalanceAmount;
    }

    public void setGwBaseTaxAmountRefunded(Object gwBaseTaxAmountRefunded) {
        this.gwBaseTaxAmountRefunded = gwBaseTaxAmountRefunded;
    }

    public Object getGwBaseTaxAmountRefunded() {
        return gwBaseTaxAmountRefunded;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setDiscountInvoiced(String discountInvoiced) {
        this.discountInvoiced = discountInvoiced;
    }

    public String getDiscountInvoiced() {
        return discountInvoiced;
    }

    public void setCustomerGroupId(String customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getCustomerGroupId() {
        return customerGroupId;
    }

    public void setBaseGiftCardsAmount(String baseGiftCardsAmount) {
        this.baseGiftCardsAmount = baseGiftCardsAmount;
    }

    public String getBaseGiftCardsAmount() {
        return baseGiftCardsAmount;
    }

    public void setBaseTaxInvoiced(String baseTaxInvoiced) {
        this.baseTaxInvoiced = baseTaxInvoiced;
    }

    public String getBaseTaxInvoiced() {
        return baseTaxInvoiced;
    }

    public void setBaseShippingInvoiced(String baseShippingInvoiced) {
        this.baseShippingInvoiced = baseShippingInvoiced;
    }

    public String getBaseShippingInvoiced() {
        return baseShippingInvoiced;
    }

    public void setGwAddCard(Object gwAddCard) {
        this.gwAddCard = gwAddCard;
    }

    public Object getGwAddCard() {
        return gwAddCard;
    }

    public void setGwItemsBasePrice(String gwItemsBasePrice) {
        this.gwItemsBasePrice = gwItemsBasePrice;
    }

    public String getGwItemsBasePrice() {
        return gwItemsBasePrice;
    }

    public void setFakturPajakNumber(String fakturPajakNumber) {
        this.fakturPajakNumber = fakturPajakNumber;
    }

    public String getFakturPajakNumber() {
        return fakturPajakNumber;
    }

    public void setCustomerNote(Object customerNote) {
        this.customerNote = customerNote;
    }

    public Object getCustomerNote() {
        return customerNote;
    }

    public void setBaseTotalRefunded(Object baseTotalRefunded) {
        this.baseTotalRefunded = baseTotalRefunded;
    }

    public Object getBaseTotalRefunded() {
        return baseTotalRefunded;
    }

    public void setCanShipPartiallyItem(Object canShipPartiallyItem) {
        this.canShipPartiallyItem = canShipPartiallyItem;
    }

    public Object getCanShipPartiallyItem() {
        return canShipPartiallyItem;
    }

    public void setRewardCurrencyAmount(Object rewardCurrencyAmount) {
        this.rewardCurrencyAmount = rewardCurrencyAmount;
    }

    public Object getRewardCurrencyAmount() {
        return rewardCurrencyAmount;
    }

    public void setAdjustmentNegative(Object adjustmentNegative) {
        this.adjustmentNegative = adjustmentNegative;
    }

    public Object getAdjustmentNegative() {
        return adjustmentNegative;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setForcedShipmentWithInvoice(Object forcedShipmentWithInvoice) {
        this.forcedShipmentWithInvoice = forcedShipmentWithInvoice;
    }

    public Object getForcedShipmentWithInvoice() {
        return forcedShipmentWithInvoice;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setShippingAddressId(String shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    public String getShippingAddressId() {
        return shippingAddressId;
    }

    public void setShippingInvoiced(String shippingInvoiced) {
        this.shippingInvoiced = shippingInvoiced;
    }

    public String getShippingInvoiced() {
        return shippingInvoiced;
    }

    public void setGwBasePrice(String gwBasePrice) {
        this.gwBasePrice = gwBasePrice;
    }

    public String getGwBasePrice() {
        return gwBasePrice;
    }

    public void setDiscountTaxCompensationAmount(String discountTaxCompensationAmount) {
        this.discountTaxCompensationAmount = discountTaxCompensationAmount;
    }

    public String getDiscountTaxCompensationAmount() {
        return discountTaxCompensationAmount;
    }

    public void setGwItemsBasePriceInvoiced(Object gwItemsBasePriceInvoiced) {
        this.gwItemsBasePriceInvoiced = gwItemsBasePriceInvoiced;
    }

    public Object getGwItemsBasePriceInvoiced() {
        return gwItemsBasePriceInvoiced;
    }

    public void setGwPriceInvoiced(Object gwPriceInvoiced) {
        this.gwPriceInvoiced = gwPriceInvoiced;
    }

    public Object getGwPriceInvoiced() {
        return gwPriceInvoiced;
    }

    public void setGiftCardsAmount(String giftCardsAmount) {
        this.giftCardsAmount = giftCardsAmount;
    }

    public String getGiftCardsAmount() {
        return giftCardsAmount;
    }

    public void setBaseShippingTaxRefunded(Object baseShippingTaxRefunded) {
        this.baseShippingTaxRefunded = baseShippingTaxRefunded;
    }

    public Object getBaseShippingTaxRefunded() {
        return baseShippingTaxRefunded;
    }

    public void setDokuExport(String dokuExport) {
        this.dokuExport = dokuExport;
    }

    public String getDokuExport() {
        return dokuExport;
    }

    public void setGwItemsBaseTaxInvoiced(Object gwItemsBaseTaxInvoiced) {
        this.gwItemsBaseTaxInvoiced = gwItemsBaseTaxInvoiced;
    }

    public Object getGwItemsBaseTaxInvoiced() {
        return gwItemsBaseTaxInvoiced;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setTotalInvoiced(String totalInvoiced) {
        this.totalInvoiced = totalInvoiced;
    }

    public String getTotalInvoiced() {
        return totalInvoiced;
    }

    public void setOriginalIncrementId(Object originalIncrementId) {
        this.originalIncrementId = originalIncrementId;
    }

    public Object getOriginalIncrementId() {
        return originalIncrementId;
    }

    public void setBsCustomerBalTotalRefunded(Object bsCustomerBalTotalRefunded) {
        this.bsCustomerBalTotalRefunded = bsCustomerBalTotalRefunded;
    }

    public Object getBsCustomerBalTotalRefunded() {
        return bsCustomerBalTotalRefunded;
    }

    public void setGwCardBaseTaxInvoiced(Object gwCardBaseTaxInvoiced) {
        this.gwCardBaseTaxInvoiced = gwCardBaseTaxInvoiced;
    }

    public Object getGwCardBaseTaxInvoiced() {
        return gwCardBaseTaxInvoiced;
    }

    public void setIsVirtual(String isVirtual) {
        this.isVirtual = isVirtual;
    }

    public String getIsVirtual() {
        return isVirtual;
    }

    public void setBaseAdjustmentPositive(Object baseAdjustmentPositive) {
        this.baseAdjustmentPositive = baseAdjustmentPositive;
    }

    public Object getBaseAdjustmentPositive() {
        return baseAdjustmentPositive;
    }

    public void setBaseShippingCanceled(Object baseShippingCanceled) {
        this.baseShippingCanceled = baseShippingCanceled;
    }

    public Object getBaseShippingCanceled() {
        return baseShippingCanceled;
    }

    public void setPaypalIpnCustomerNotified(String paypalIpnCustomerNotified) {
        this.paypalIpnCustomerNotified = paypalIpnCustomerNotified;
    }

    public String getPaypalIpnCustomerNotified() {
        return paypalIpnCustomerNotified;
    }

    public void setDiscountCanceled(Object discountCanceled) {
        this.discountCanceled = discountCanceled;
    }

    public Object getDiscountCanceled() {
        return discountCanceled;
    }

    public void setBaseCustomerBalanceInvoiced(Object baseCustomerBalanceInvoiced) {
        this.baseCustomerBalanceInvoiced = baseCustomerBalanceInvoiced;
    }

    public Object getBaseCustomerBalanceInvoiced() {
        return baseCustomerBalanceInvoiced;
    }

    public void setExtCustomerId(Object extCustomerId) {
        this.extCustomerId = extCustomerId;
    }

    public Object getExtCustomerId() {
        return extCustomerId;
    }

    public void setFakturPajakPdfAttachmentPath(Object fakturPajakPdfAttachmentPath) {
        this.fakturPajakPdfAttachmentPath = fakturPajakPdfAttachmentPath;
    }

    public Object getFakturPajakPdfAttachmentPath() {
        return fakturPajakPdfAttachmentPath;
    }

    public void setGwBasePriceInvoiced(Object gwBasePriceInvoiced) {
        this.gwBasePriceInvoiced = gwBasePriceInvoiced;
    }

    public Object getGwBasePriceInvoiced() {
        return gwBasePriceInvoiced;
    }

    public void setBaseShippingDiscountAmount(String baseShippingDiscountAmount) {
        this.baseShippingDiscountAmount = baseShippingDiscountAmount;
    }

    public String getBaseShippingDiscountAmount() {
        return baseShippingDiscountAmount;
    }

    public void setGwItemsPrice(String gwItemsPrice) {
        this.gwItemsPrice = gwItemsPrice;
    }

    public String getGwItemsPrice() {
        return gwItemsPrice;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setBaseRewardCurrencyAmount(Object baseRewardCurrencyAmount) {
        this.baseRewardCurrencyAmount = baseRewardCurrencyAmount;
    }

    public Object getBaseRewardCurrencyAmount() {
        return baseRewardCurrencyAmount;
    }

    public void setBaseCurrencyCode(String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }

    public String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    public void setCustomerBalTotalRefunded(Object customerBalTotalRefunded) {
        this.customerBalTotalRefunded = customerBalTotalRefunded;
    }

    public Object getCustomerBalTotalRefunded() {
        return customerBalTotalRefunded;
    }

    public void setIsAppointment(String isAppointment) {
        this.isAppointment = isAppointment;
    }

    public String getIsAppointment() {
        return isAppointment;
    }

    public void setDiscountTaxCompensationRefunded(Object discountTaxCompensationRefunded) {
        this.discountTaxCompensationRefunded = discountTaxCompensationRefunded;
    }

    public Object getDiscountTaxCompensationRefunded() {
        return discountTaxCompensationRefunded;
    }

    public void setGwCardBasePriceInvoiced(Object gwCardBasePriceInvoiced) {
        this.gwCardBasePriceInvoiced = gwCardBasePriceInvoiced;
    }

    public Object getGwCardBasePriceInvoiced() {
        return gwCardBasePriceInvoiced;
    }

    public void setCustomerBalanceInvoiced(Object customerBalanceInvoiced) {
        this.customerBalanceInvoiced = customerBalanceInvoiced;
    }

    public Object getCustomerBalanceInvoiced() {
        return customerBalanceInvoiced;
    }

    public void setCustomerNpwp(Object customerNpwp) {
        this.customerNpwp = customerNpwp;
    }

    public Object getCustomerNpwp() {
        return customerNpwp;
    }

    public void setTotalDue(String totalDue) {
        this.totalDue = totalDue;
    }

    public String getTotalDue() {
        return totalDue;
    }

    public void setGiftCardsInvoiced(Object giftCardsInvoiced) {
        this.giftCardsInvoiced = giftCardsInvoiced;
    }

    public Object getGiftCardsInvoiced() {
        return giftCardsInvoiced;
    }

    public void setGwItemsBaseTaxAmount(String gwItemsBaseTaxAmount) {
        this.gwItemsBaseTaxAmount = gwItemsBaseTaxAmount;
    }

    public String getGwItemsBaseTaxAmount() {
        return gwItemsBaseTaxAmount;
    }

    public void setCustomerKtp(Object customerKtp) {
        this.customerKtp = customerKtp;
    }

    public Object getCustomerKtp() {
        return customerKtp;
    }

    public void setEditIncrement(Object editIncrement) {
        this.editIncrement = editIncrement;
    }

    public Object getEditIncrement() {
        return editIncrement;
    }

    public void setBaseDiscountTaxCompensationAmount(String baseDiscountTaxCompensationAmount) {
        this.baseDiscountTaxCompensationAmount = baseDiscountTaxCompensationAmount;
    }

    public String getBaseDiscountTaxCompensationAmount() {
        return baseDiscountTaxCompensationAmount;
    }

    public void setProtectCode(String protectCode) {
        this.protectCode = protectCode;
    }

    public String getProtectCode() {
        return protectCode;
    }

    public void setOrderCurrencyCode(String orderCurrencyCode) {
        this.orderCurrencyCode = orderCurrencyCode;
    }

    public String getOrderCurrencyCode() {
        return orderCurrencyCode;
    }

    public void setGwAllowGiftReceipt(Object gwAllowGiftReceipt) {
        this.gwAllowGiftReceipt = gwAllowGiftReceipt;
    }

    public Object getGwAllowGiftReceipt() {
        return gwAllowGiftReceipt;
    }

    public void setQuoteAddressId(Object quoteAddressId) {
        this.quoteAddressId = quoteAddressId;
    }

    public Object getQuoteAddressId() {
        return quoteAddressId;
    }

    public void setPaymentAuthExpiration(Object paymentAuthExpiration) {
        this.paymentAuthExpiration = paymentAuthExpiration;
    }

    public Object getPaymentAuthExpiration() {
        return paymentAuthExpiration;
    }

    @Override
    public String toString() {
        return
                "OrderDetail{" +
                        "tax_invoiced = '" + taxInvoiced + '\'' +
                        ",subtotal_canceled = '" + subtotalCanceled + '\'' +
                        ",customer_price_list_type = '" + customerPriceListType + '\'' +
                        ",relation_child_id = '" + relationChildId + '\'' +
                        ",discount_refunded = '" + discountRefunded + '\'' +
                        ",gw_price_refunded = '" + gwPriceRefunded + '\'' +
                        ",total_refunded = '" + totalRefunded + '\'' +
                        ",base_tax_refunded = '" + baseTaxRefunded + '\'' +
                        ",state = '" + state + '\'' +
                        ",gw_price = '" + gwPrice + '\'' +
                        ",gw_card_base_price_incl_tax = '" + gwCardBasePriceInclTax + '\'' +
                        ",gw_card_price = '" + gwCardPrice + '\'' +
                        ",quote_id = '" + quoteId + '\'' +
                        ",wmos_outbound = '" + wmosOutbound + '\'' +
                        ",x_forwarded_for = '" + xForwardedFor + '\'' +
                        ",base_total_invoiced = '" + baseTotalInvoiced + '\'' +
                        ",base_rwrd_crrncy_amt_invoiced = '" + baseRwrdCrrncyAmtInvoiced + '\'' +
                        ",base_shipping_amount = '" + baseShippingAmount + '\'' +
                        ",base_subtotal_invoiced = '" + baseSubtotalInvoiced + '\'' +
                        ",applied_rule_ids = '" + appliedRuleIds + '\'' +
                        ",gw_items_tax_amount = '" + gwItemsTaxAmount + '\'' +
                        ",subtotal = '" + subtotal + '\'' +
                        ",gw_base_price_refunded = '" + gwBasePriceRefunded + '\'' +
                        ",gw_items_tax_refunded = '" + gwItemsTaxRefunded + '\'' +
                        ",customer_is_guest = '" + customerIsGuest + '\'' +
                        ",items = '" + items + '\'' +
                        ",customer_sales_org = '" + customerSalesOrg + '\'' +
                        ",shipping_refunded = '" + shippingRefunded + '\'' +
                        ",total_canceled = '" + totalCanceled + '\'' +
                        ",total_item_count = '" + totalItemCount + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",gw_card_base_price = '" + gwCardBasePrice + '\'' +
                        ",customer_prefix = '" + customerPrefix + '\'' +
                        ",discount_description = '" + discountDescription + '\'' +
                        ",grand_total = '" + grandTotal + '\'' +
                        ",gw_card_base_tax_refunded = '" + gwCardBaseTaxRefunded + '\'' +
                        ",base_tax_amount = '" + baseTaxAmount + '\'' +
                        ",relation_parent_real_id = '" + relationParentRealId + '\'' +
                        ",shipping_discount_tax_compensation_amount = '" + shippingDiscountTaxCompensationAmount + '\'' +
                        ",base_discount_tax_compensation_refunded = '" + baseDiscountTaxCompensationRefunded + '\'' +
                        ",gw_card_base_tax_amount = '" + gwCardBaseTaxAmount + '\'' +
                        ",total_qty_ordered = '" + totalQtyOrdered + '\'' +
                        ",gw_card_tax_invoiced = '" + gwCardTaxInvoiced + '\'' +
                        ",base_adjustment_negative = '" + baseAdjustmentNegative + '\'' +
                        ",subtotal_refunded = '" + subtotalRefunded + '\'' +
                        ",base_to_order_rate = '" + baseToOrderRate + '\'' +
                        ",adjustment_positive = '" + adjustmentPositive + '\'' +
                        ",base_gift_cards_refunded = '" + baseGiftCardsRefunded + '\'' +
                        ",gw_items_base_price_incl_tax = '" + gwItemsBasePriceInclTax + '\'' +
                        ",base_subtotal = '" + baseSubtotal + '\'' +
                        ",customer_partner_function = '" + customerPartnerFunction + '\'' +
                        ",payment_authorization_amount = '" + paymentAuthorizationAmount + '\'' +
                        ",base_subtotal_incl_tax = '" + baseSubtotalInclTax + '\'' +
                        ",customer_gender = '" + customerGender + '\'' +
                        ",tax_amount = '" + taxAmount + '\'' +
                        ",customer_balance_refunded = '" + customerBalanceRefunded + '\'' +
                        ",customer_note_notify = '" + customerNoteNotify + '\'' +
                        ",rwrd_crrncy_amnt_refunded = '" + rwrdCrrncyAmntRefunded + '\'' +
                        ",shipping_discount_amount = '" + shippingDiscountAmount + '\'' +
                        ",ext_order_id = '" + extOrderId + '\'' +
                        ",gw_card_price_refunded = '" + gwCardPriceRefunded + '\'' +
                        ",discount_amount = '" + discountAmount + '\'' +
                        ",total_offline_refunded = '" + totalOfflineRefunded + '\'' +
                        ",relation_parent_id = '" + relationParentId + '\'' +
                        ",increment_id = '" + incrementId + '\'' +
                        ",customer_ar_overdue = '" + customerArOverdue + '\'' +
                        ",gw_card_tax_amount = '" + gwCardTaxAmount + '\'' +
                        ",base_shipping_refunded = '" + baseShippingRefunded + '\'' +
                        ",relation_child_real_id = '" + relationChildRealId + '\'' +
                        ",base_shipping_tax_amount = '" + baseShippingTaxAmount + '\'' +
                        ",base_grand_total = '" + baseGrandTotal + '\'' +
                        ",billing_address_id = '" + billingAddressId + '\'' +
                        ",wmos_inbound = '" + wmosInbound + '\'' +
                        ",base_total_offline_refunded = '" + baseTotalOfflineRefunded + '\'' +
                        ",customer_ktp_address = '" + customerKtpAddress + '\'' +
                        ",send_email = '" + sendEmail + '\'' +
                        ",weight = '" + weight + '\'' +
                        ",shipping_canceled = '" + shippingCanceled + '\'' +
                        ",base_customer_balance_refunded = '" + baseCustomerBalanceRefunded + '\'' +
                        ",base_total_online_refunded = '" + baseTotalOnlineRefunded + '\'' +
                        ",base_shipping_discount_tax_compensation_amnt = '" + baseShippingDiscountTaxCompensationAmnt + '\'' +
                        ",sap_export = '" + sapExport + '\'' +
                        ",base_discount_canceled = '" + baseDiscountCanceled + '\'' +
                        ",base_discount_invoiced = '" + baseDiscountInvoiced + '\'' +
                        ",base_to_global_rate = '" + baseToGlobalRate + '\'' +
                        ",gw_items_price_refunded = '" + gwItemsPriceRefunded + '\'' +
                        ",gift_cards_refunded = '" + giftCardsRefunded + '\'' +
                        ",base_total_invoiced_cost = '" + baseTotalInvoicedCost + '\'' +
                        ",email_sent = '" + emailSent + '\'' +
                        ",discount_tax_compensation_invoiced = '" + discountTaxCompensationInvoiced + '\'' +
                        ",base_tax_canceled = '" + baseTaxCanceled + '\'' +
                        ",gw_base_tax_amount = '" + gwBaseTaxAmount + '\'' +
                        ",gift_message_id = '" + giftMessageId + '\'' +
                        ",base_customer_balance_amount = '" + baseCustomerBalanceAmount + '\'' +
                        ",gw_card_base_price_refunded = '" + gwCardBasePriceRefunded + '\'' +
                        ",gw_items_base_tax_refunded = '" + gwItemsBaseTaxRefunded + '\'' +
                        ",gw_tax_amount_invoiced = '" + gwTaxAmountInvoiced + '\'' +
                        ",base_subtotal_refunded = '" + baseSubtotalRefunded + '\'' +
                        ",base_total_paid = '" + baseTotalPaid + '\'' +
                        ",customer_middlename = '" + customerMiddlename + '\'' +
                        ",base_rwrd_crrncy_amnt_refnded = '" + baseRwrdCrrncyAmntRefnded + '\'' +
                        ",shipping_description = '" + shippingDescription + '\'' +
                        ",gw_base_price_incl_tax = '" + gwBasePriceInclTax + '\'' +
                        ",tax_refunded = '" + taxRefunded + '\'' +
                        ",store_to_order_rate = '" + storeToOrderRate + '\'' +
                        ",gift_cards = '" + giftCards + '\'' +
                        ",customer_dob = '" + customerDob + '\'' +
                        ",base_total_due = '" + baseTotalDue + '\'' +
                        ",shipping_incl_tax = '" + shippingInclTax + '\'' +
                        ",customer_division = '" + customerDivision + '\'' +
                        ",customer_block_flag = '" + customerBlockFlag + '\'' +
                        ",customer_firstname = '" + customerFirstname + '\'' +
                        ",base_discount_tax_compensation_invoiced = '" + baseDiscountTaxCompensationInvoiced + '\'' +
                        ",coupon_rule_name = '" + couponRuleName + '\'' +
                        ",hold_before_status = '" + holdBeforeStatus + '\'' +
                        ",tax_canceled = '" + taxCanceled + '\'' +
                        ",gw_tax_amount = '" + gwTaxAmount + '\'' +
                        ",gw_items_price_incl_tax = '" + gwItemsPriceInclTax + '\'' +
                        ",gw_items_price_invoiced = '" + gwItemsPriceInvoiced + '\'' +
                        ",base_gift_cards_invoiced = '" + baseGiftCardsInvoiced + '\'' +
                        ",total_paid = '" + totalPaid + '\'' +
                        ",shipping_method = '" + shippingMethod + '\'' +
                        ",base_discount_refunded = '" + baseDiscountRefunded + '\'' +
                        ",gw_card_price_incl_tax = '" + gwCardPriceInclTax + '\'' +
                        ",customer_lastname = '" + customerLastname + '\'' +
                        ",base_total_canceled = '" + baseTotalCanceled + '\'' +
                        ",shipping_tax_refunded = '" + shippingTaxRefunded + '\'' +
                        ",customer_taxvat = '" + customerTaxvat + '\'' +
                        ",gw_id = '" + gwId + '\'' +
                        ",base_subtotal_canceled = '" + baseSubtotalCanceled + '\'' +
                        ",entity_id = '" + entityId + '\'' +
                        ",gw_items_base_price_refunded = '" + gwItemsBasePriceRefunded + '\'' +
                        ",is_djp_exported = '" + isDjpExported + '\'' +
                        ",subtotal_incl_tax = '" + subtotalInclTax + '\'' +
                        ",base_shipping_incl_tax = '" + baseShippingInclTax + '\'' +
                        ",doku_paid = '" + dokuPaid + '\'' +
                        ",status = '" + status + '\'' +
                        ",base_total_qty_ordered = '" + baseTotalQtyOrdered + '\'' +
                        ",global_currency_code = '" + globalCurrencyCode + '\'' +
                        ",gw_tax_amount_refunded = '" + gwTaxAmountRefunded + '\'' +
                        ",can_ship_partially = '" + canShipPartially + '\'' +
                        ",reward_points_balance = '" + rewardPointsBalance + '\'' +
                        ",customer_suffix = '" + customerSuffix + '\'' +
                        ",store_currency_code = '" + storeCurrencyCode + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",gw_items_tax_invoiced = '" + gwItemsTaxInvoiced + '\'' +
                        ",rwrd_currency_amount_invoiced = '" + rwrdCurrencyAmountInvoiced + '\'' +
                        ",gw_card_tax_refunded = '" + gwCardTaxRefunded + '\'' +
                        ",total_online_refunded = '" + totalOnlineRefunded + '\'' +
                        ",shipping_tax_amount = '" + shippingTaxAmount + '\'' +
                        ",hold_before_state = '" + holdBeforeState + '\'' +
                        ",store_to_base_rate = '" + storeToBaseRate + '\'' +
                        ",store_id = '" + storeId + '\'' +
                        ",gw_card_price_invoiced = '" + gwCardPriceInvoiced + '\'' +
                        ",coupon_code = '" + couponCode + '\'' +
                        ",reward_points_balance_refund = '" + rewardPointsBalanceRefund + '\'' +
                        ",customer_djp = '" + customerDjp + '\'' +
                        ",gw_price_incl_tax = '" + gwPriceInclTax + '\'' +
                        ",base_discount_amount = '" + baseDiscountAmount + '\'' +
                        ",gw_base_tax_amount_invoiced = '" + gwBaseTaxAmountInvoiced + '\'' +
                        ",customer_dist_channel = '" + customerDistChannel + '\'' +
                        ",shipping_amount = '" + shippingAmount + '\'' +
                        ",subtotal_invoiced = '" + subtotalInvoiced + '\'' +
                        ",customer_balance_amount = '" + customerBalanceAmount + '\'' +
                        ",gw_base_tax_amount_refunded = '" + gwBaseTaxAmountRefunded + '\'' +
                        ",customer_id = '" + customerId + '\'' +
                        ",discount_invoiced = '" + discountInvoiced + '\'' +
                        ",customer_group_id = '" + customerGroupId + '\'' +
                        ",base_gift_cards_amount = '" + baseGiftCardsAmount + '\'' +
                        ",base_tax_invoiced = '" + baseTaxInvoiced + '\'' +
                        ",base_shipping_invoiced = '" + baseShippingInvoiced + '\'' +
                        ",gw_add_card = '" + gwAddCard + '\'' +
                        ",gw_items_base_price = '" + gwItemsBasePrice + '\'' +
                        ",faktur_pajak_number = '" + fakturPajakNumber + '\'' +
                        ",customer_note = '" + customerNote + '\'' +
                        ",base_total_refunded = '" + baseTotalRefunded + '\'' +
                        ",can_ship_partially_item = '" + canShipPartiallyItem + '\'' +
                        ",reward_currency_amount = '" + rewardCurrencyAmount + '\'' +
                        ",adjustment_negative = '" + adjustmentNegative + '\'' +
                        ",remote_ip = '" + remoteIp + '\'' +
                        ",forced_shipment_with_invoice = '" + forcedShipmentWithInvoice + '\'' +
                        ",payment_method = '" + paymentMethod + '\'' +
                        ",shipping_address_id = '" + shippingAddressId + '\'' +
                        ",shipping_invoiced = '" + shippingInvoiced + '\'' +
                        ",gw_base_price = '" + gwBasePrice + '\'' +
                        ",discount_tax_compensation_amount = '" + discountTaxCompensationAmount + '\'' +
                        ",gw_items_base_price_invoiced = '" + gwItemsBasePriceInvoiced + '\'' +
                        ",gw_price_invoiced = '" + gwPriceInvoiced + '\'' +
                        ",gift_cards_amount = '" + giftCardsAmount + '\'' +
                        ",base_shipping_tax_refunded = '" + baseShippingTaxRefunded + '\'' +
                        ",doku_export = '" + dokuExport + '\'' +
                        ",gw_items_base_tax_invoiced = '" + gwItemsBaseTaxInvoiced + '\'' +
                        ",customer_email = '" + customerEmail + '\'' +
                        ",total_invoiced = '" + totalInvoiced + '\'' +
                        ",original_increment_id = '" + originalIncrementId + '\'' +
                        ",bs_customer_bal_total_refunded = '" + bsCustomerBalTotalRefunded + '\'' +
                        ",gw_card_base_tax_invoiced = '" + gwCardBaseTaxInvoiced + '\'' +
                        ",is_virtual = '" + isVirtual + '\'' +
                        ",base_adjustment_positive = '" + baseAdjustmentPositive + '\'' +
                        ",base_shipping_canceled = '" + baseShippingCanceled + '\'' +
                        ",paypal_ipn_customer_notified = '" + paypalIpnCustomerNotified + '\'' +
                        ",discount_canceled = '" + discountCanceled + '\'' +
                        ",base_customer_balance_invoiced = '" + baseCustomerBalanceInvoiced + '\'' +
                        ",ext_customer_id = '" + extCustomerId + '\'' +
                        ",faktur_pajak_pdf_attachment_path = '" + fakturPajakPdfAttachmentPath + '\'' +
                        ",gw_base_price_invoiced = '" + gwBasePriceInvoiced + '\'' +
                        ",base_shipping_discount_amount = '" + baseShippingDiscountAmount + '\'' +
                        ",gw_items_price = '" + gwItemsPrice + '\'' +
                        ",store_name = '" + storeName + '\'' +
                        ",base_reward_currency_amount = '" + baseRewardCurrencyAmount + '\'' +
                        ",base_currency_code = '" + baseCurrencyCode + '\'' +
                        ",customer_bal_total_refunded = '" + customerBalTotalRefunded + '\'' +
                        ",is_appointment = '" + isAppointment + '\'' +
                        ",discount_tax_compensation_refunded = '" + discountTaxCompensationRefunded + '\'' +
                        ",gw_card_base_price_invoiced = '" + gwCardBasePriceInvoiced + '\'' +
                        ",customer_balance_invoiced = '" + customerBalanceInvoiced + '\'' +
                        ",customer_npwp = '" + customerNpwp + '\'' +
                        ",total_due = '" + totalDue + '\'' +
                        ",gift_cards_invoiced = '" + giftCardsInvoiced + '\'' +
                        ",gw_items_base_tax_amount = '" + gwItemsBaseTaxAmount + '\'' +
                        ",customer_ktp = '" + customerKtp + '\'' +
                        ",edit_increment = '" + editIncrement + '\'' +
                        ",base_discount_tax_compensation_amount = '" + baseDiscountTaxCompensationAmount + '\'' +
                        ",protect_code = '" + protectCode + '\'' +
                        ",order_currency_code = '" + orderCurrencyCode + '\'' +
                        ",gw_allow_gift_receipt = '" + gwAllowGiftReceipt + '\'' +
                        ",quote_address_id = '" + quoteAddressId + '\'' +
                        ",payment_auth_expiration = '" + paymentAuthExpiration + '\'' +
                        "}";
    }
}