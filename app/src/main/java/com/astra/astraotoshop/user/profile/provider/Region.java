package com.astra.astraotoshop.user.profile.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Region implements Parcelable{

    @SerializedName("region_id")
    private int regionId;

    @SerializedName("region")
    private String region;

    @SerializedName("region_code")
    private String regionCode;

    protected Region(Parcel in) {
        regionId = in.readInt();
        region = in.readString();
        regionCode = in.readString();
    }

    public static final Creator<Region> CREATOR = new Creator<Region>() {
        @Override
        public Region createFromParcel(Parcel in) {
            return new Region(in);
        }

        @Override
        public Region[] newArray(int size) {
            return new Region[size];
        }
    };

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    @Override
    public String toString() {
        return
                "Region{" +
                        "region_id = '" + regionId + '\'' +
                        ",region = '" + region + '\'' +
                        ",region_code = '" + regionCode + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(regionId);
        dest.writeString(region);
        dest.writeString(regionCode);
    }
}