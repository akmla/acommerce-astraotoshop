package com.astra.astraotoshop.user.voucher.provider.model;

import com.google.gson.annotations.SerializedName;

public class VoucherData {

    @SerializedName("item_sku")
    private String itemSku;

    @SerializedName("voucher_status")
    private String voucherStatus;

    @SerializedName("voucher_id")
    private String voucherId;

    @SerializedName("voucher_code")
    private String voucherCode;

    @SerializedName("item_name")
    private String itemName;

    @SerializedName("voucher_expired_at")
    private String voucherExpiredAt;

    @SerializedName("mechanic_come_to_home")
    private String mechanicComeToHome;

    @SerializedName("order_increment_id")
    private String orderIncrementId;

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setVoucherExpiredAt(String voucherExpiredAt) {
        this.voucherExpiredAt = voucherExpiredAt;
    }

    public String getVoucherExpiredAt() {
        return voucherExpiredAt;
    }

    public void setMechanicComeToHome(String mechanicComeToHome) {
        this.mechanicComeToHome = mechanicComeToHome;
    }

    public String getMechanicComeToHome() {
        return mechanicComeToHome;
    }

    public void setOrderIncrementId(String orderIncrementId) {
        this.orderIncrementId = orderIncrementId;
    }

    public String getOrderIncrementId() {
        return orderIncrementId;
    }

    @Override
    public String toString() {
        return
                "VoucherData{" +
                        "item_sku = '" + itemSku + '\'' +
                        ",voucher_status = '" + voucherStatus + '\'' +
                        ",voucher_id = '" + voucherId + '\'' +
                        ",voucher_code = '" + voucherCode + '\'' +
                        ",item_name = '" + itemName + '\'' +
                        ",voucher_expired_at = '" + voucherExpiredAt + '\'' +
                        ",mechanic_come_to_home = '" + mechanicComeToHome + '\'' +
                        ",order_increment_id = '" + orderIncrementId + '\'' +
                        "}";
    }
}