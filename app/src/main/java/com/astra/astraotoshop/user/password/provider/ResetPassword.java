package com.astra.astraotoshop.user.password.provider;

import com.google.gson.annotations.SerializedName;

public class ResetPassword {

    @SerializedName("template")
    private String template;

    @SerializedName("websiteId")
    private int websiteId;

    @SerializedName("email")
    private String email;

    public ResetPassword() {
    }

    public ResetPassword(String template, int websiteId, String email) {
        this.template = template;
        this.websiteId = websiteId;
        this.email = email;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public void setWebsiteId(int websiteId) {
        this.websiteId = websiteId;
    }

    public int getWebsiteId() {
        return websiteId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return
                "ResetPassword{" +
                        "template = '" + template + '\'' +
                        ",websiteId = '" + websiteId + '\'' +
                        ",email = '" + email + '\'' +
                        "}";
    }
}