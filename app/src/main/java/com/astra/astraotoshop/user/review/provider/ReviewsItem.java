package com.astra.astraotoshop.user.review.provider;

import com.google.gson.annotations.SerializedName;

public class ReviewsItem {

	@SerializedName("store_id")
	private String storeId;

	@SerializedName("review_id")
	private String reviewId;

	@SerializedName("review_created_at")
	private String reviewCreatedAt;

	@SerializedName("type_id")
	private String typeId;

	@SerializedName("has_options")
	private String hasOptions;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("updated_in")
	private String updatedIn;

	@SerializedName("entity_id")
	private String entityId;

	@SerializedName("title")
	private String title;

	@SerializedName("search_weight")
	private String searchWeight;

	@SerializedName("sum_review")
	private int sumReview;

	@SerializedName("required_options")
	private String requiredOptions;

	@SerializedName("status_id")
	private String statusId;

	@SerializedName("attribute_set_id")
	private String attributeSetId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("nickname")
	private String nickname;

	@SerializedName("name")
	private String name;

	@SerializedName("detail")
	private String detail;

	@SerializedName("row_id")
	private String rowId;

	@SerializedName("sku")
	private String sku;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("created_in")
	private String createdIn;

	public void setStoreId(String storeId){
		this.storeId = storeId;
	}

	public String getStoreId(){
		return storeId;
	}

	public void setReviewId(String reviewId){
		this.reviewId = reviewId;
	}

	public String getReviewId(){
		return reviewId;
	}

	public void setReviewCreatedAt(String reviewCreatedAt){
		this.reviewCreatedAt = reviewCreatedAt;
	}

	public String getReviewCreatedAt(){
		return reviewCreatedAt;
	}

	public void setTypeId(String typeId){
		this.typeId = typeId;
	}

	public String getTypeId(){
		return typeId;
	}

	public void setHasOptions(String hasOptions){
		this.hasOptions = hasOptions;
	}

	public String getHasOptions(){
		return hasOptions;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setUpdatedIn(String updatedIn){
		this.updatedIn = updatedIn;
	}

	public String getUpdatedIn(){
		return updatedIn;
	}

	public void setEntityId(String entityId){
		this.entityId = entityId;
	}

	public String getEntityId(){
		return entityId;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setSearchWeight(String searchWeight){
		this.searchWeight = searchWeight;
	}

	public String getSearchWeight(){
		return searchWeight;
	}

	public void setSumReview(int sumReview){
		this.sumReview = sumReview;
	}

	public int getSumReview(){
		return sumReview;
	}

	public void setRequiredOptions(String requiredOptions){
		this.requiredOptions = requiredOptions;
	}

	public String getRequiredOptions(){
		return requiredOptions;
	}

	public void setStatusId(String statusId){
		this.statusId = statusId;
	}

	public String getStatusId(){
		return statusId;
	}

	public void setAttributeSetId(String attributeSetId){
		this.attributeSetId = attributeSetId;
	}

	public String getAttributeSetId(){
		return attributeSetId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setNickname(String nickname){
		this.nickname = nickname;
	}

	public String getNickname(){
		return nickname;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	public void setRowId(String rowId){
		this.rowId = rowId;
	}

	public String getRowId(){
		return rowId;
	}

	public void setSku(String sku){
		this.sku = sku;
	}

	public String getSku(){
		return sku;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setCreatedIn(String createdIn){
		this.createdIn = createdIn;
	}

	public String getCreatedIn(){
		return createdIn;
	}

	@Override
 	public String toString(){
		return 
			"ReviewsItem{" +
			"store_id = '" + storeId + '\'' + 
			",review_id = '" + reviewId + '\'' + 
			",review_created_at = '" + reviewCreatedAt + '\'' + 
			",type_id = '" + typeId + '\'' + 
			",has_options = '" + hasOptions + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",updated_in = '" + updatedIn + '\'' + 
			",entity_id = '" + entityId + '\'' + 
			",title = '" + title + '\'' + 
			",search_weight = '" + searchWeight + '\'' + 
			",sum_review = '" + sumReview + '\'' + 
			",required_options = '" + requiredOptions + '\'' + 
			",status_id = '" + statusId + '\'' + 
			",attribute_set_id = '" + attributeSetId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",product_id = '" + productId + '\'' + 
			",nickname = '" + nickname + '\'' + 
			",name = '" + name + '\'' + 
			",detail = '" + detail + '\'' + 
			",row_id = '" + rowId + '\'' + 
			",sku = '" + sku + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			",created_in = '" + createdIn + '\'' + 
			"}";
		}
}