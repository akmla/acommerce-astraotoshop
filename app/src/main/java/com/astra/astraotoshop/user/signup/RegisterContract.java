package com.astra.astraotoshop.user.signup;

import com.astra.astraotoshop.user.signup.provider.RegisterBody;
import com.astra.astraotoshop.utils.view.StringFormat;

/**
 * Created by Henra Setia Nugraha on 2/15/2018.
 */

public interface RegisterContract {
    interface RegisterView{
        void onRegisterSuccess();
        void onRegisterFailed();
    }
    interface RegisterPresenter{
        void submit(String storeCode, String email, String firstName, String lastName, String password, int djp, String npwp, String ktp, String address, boolean isSubscribe);
        void onRegisterSuccess();
        void onRegisterFailed();
    }
    interface RegisterInteractor{
        void submit(String storeCode,RegisterBody registerBody);
    }
}
