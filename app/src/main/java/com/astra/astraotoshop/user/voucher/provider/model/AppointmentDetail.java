package com.astra.astraotoshop.user.voucher.provider.model;

import com.google.gson.annotations.SerializedName;

public class AppointmentDetail {

    @SerializedName("location")
    private Location location;

    @SerializedName("mechanic_come_to_home")
    private String mechanicComeToHome;

    @SerializedName("slot")
    private Slot slot;

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setMechanicComeToHome(String mechanicComeToHome) {
        this.mechanicComeToHome = mechanicComeToHome;
    }

    public String getMechanicComeToHome() {
        return mechanicComeToHome;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public Slot getSlot() {
        return slot;
    }

    @Override
    public String toString() {
        return
                "AppointmentDetail{" +
                        "location = '" + location + '\'' +
                        ",mechanic_come_to_home = '" + mechanicComeToHome + '\'' +
                        ",slot = '" + slot + '\'' +
                        "}";
    }
}