package com.astra.astraotoshop.user.password.provider;

import com.astra.astraotoshop.user.password.PasswordContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 5/16/2018.
 */

public class ResetPasswordInteractor extends BaseInteractor implements PasswordContract.ResetPasswordInteractor {
    private PasswordContract.ResetPasswordPresenter presenter;

    public ResetPasswordInteractor(PasswordContract.ResetPasswordPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void emailChecking(IsEmailAvailable IsEmailAvailable, Action1<Boolean> onSuccess, Action1<Throwable> onError) {
        getGeneralNetworkManager()
                .isEmailAvailable(new NetworkHandler(),IsEmailAvailable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onSuccess,onError);

    }

    @Override
    public void resetPassword(ResetPassword resetPassword) {
        getGeneralNetworkManager()
                .resetPassword(new NetworkHandler(),resetPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.onPasswordReset(),
                        e->presenter.onResetPasswordFail()
                );
    }
}
