package com.astra.astraotoshop.user.profile.provider;

/**
 * Created by Henra Setia Nugraha on 1/22/2018.
 */

public interface ProfileContract {
    interface ProfileView{
        void setProfileData(ProfileEntity profileEntity);
        void showError(String message);
    }

    interface ProfilePresenter{
        void requestProfile(String storeCode);
        void onProfileReceived(ProfileEntity profileEntity, String storeCode);
        void onFailedReceivingProfile();
    }
    interface ProfileInteractor{
        void requestProfile(String storeCode);
    }

    interface UpdateProfileView{
        void showUpdateProfileMessage(boolean isSuccess);
    }
    interface UpdateProfilePresenter{
        void setProfile(ProfileEntity profile);
        void updateProfile(String storeCode, String firstName, String lastName, String email);
        void profileUpdated(ProfileEntity profileEntity);
        void failedUpdate();
    }
    interface UpdateProfileInteractor{
        void updateProfile(String storeCode,UpdateProfile profile);
    }
}
