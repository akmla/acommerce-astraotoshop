package com.astra.astraotoshop.user.password.provider;

import com.astra.astraotoshop.user.password.PasswordContract;

/**
 * Created by Henra Setia Nugraha on 5/15/2018.
 */

public class ChangePasswordPresenter implements PasswordContract.ChangePasswordPresenter {

    private PasswordContract.ChangePasswordView view;
    private PasswordContract.ChangePasswordInteractor interactor;

    public ChangePasswordPresenter(PasswordContract.ChangePasswordView view) {
        this.view = view;
        interactor = new ChangePasswordInteractor(this);
    }

    @Override
    public void changePassword(String storeCode, String currentPass, String newPass) {
        interactor.changePassword(storeCode, new ChangePassword(newPass, currentPass));
    }

    @Override
    public void onPasswordChanged() {
        view.showPasswordMessage(true);
    }

    @Override
    public void onPasswordUnchanged() {
        view.showPasswordMessage(false);
    }
}
