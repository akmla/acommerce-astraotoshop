package com.astra.astraotoshop.user.signup.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Customer {

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("group_id")
    private int groupId;

    @SerializedName("extension_attributes")
    private ExtensionAttributes extensionAttributes;

    @SerializedName("email")
    private String email;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("custom_attributes")
    private List<CustomAttributesItem> customAttributes;

    public Customer(String firstname, int groupId, ExtensionAttributes extensionAttributes, String email, String lastname, List<CustomAttributesItem> customAttributes) {
        this.firstname = firstname;
        this.groupId = groupId;
        this.extensionAttributes = extensionAttributes;
        this.email = email;
        this.lastname = lastname;
        this.customAttributes = customAttributes;
    }

    public Customer() {
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setExtensionAttributes(ExtensionAttributes extensionAttributes) {
        this.extensionAttributes = extensionAttributes;
    }

    public ExtensionAttributes getExtensionAttributes() {
        return extensionAttributes;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setCustomAttributes(List<CustomAttributesItem> customAttributes) {
        this.customAttributes = customAttributes;
    }

    public List<CustomAttributesItem> getCustomAttributes() {
        return customAttributes;
    }

    @Override
    public String toString() {
        return
                "Customer{" +
                        "firstname = '" + firstname + '\'' +
                        ",group_id = '" + groupId + '\'' +
                        ",extension_attributes = '" + extensionAttributes + '\'' +
                        ",email = '" + email + '\'' +
                        ",lastname = '" + lastname + '\'' +
                        ",custom_attributes = '" + customAttributes + '\'' +
                        "}";
    }
}