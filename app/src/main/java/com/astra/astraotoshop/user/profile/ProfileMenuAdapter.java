package com.astra.astraotoshop.user.profile;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.utils.view.FontIcon;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Henra Setia Nugraha on 11/30/2017.
 */

public class ProfileMenuAdapter extends RecyclerView.Adapter<ProfileMenuAdapter.ProfileMenuViewHolder> {

    private String[] menus;
    private String[] icons;
    private int[] ids;
    private Context context;
    private OnItemClickListener clickListener;

    public ProfileMenuAdapter(String[] menus, String[] icons, int[] ids, Context context, OnItemClickListener clickListener) {
        this.menus = menus;
        this.icons = icons;
        this.ids = ids;
        this.context = context;
        this.clickListener = clickListener;
    }

    @Override
    public ProfileMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProfileMenuViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false));
    }

    @Override
    public void onBindViewHolder(ProfileMenuViewHolder holder, int position) {
        holder.icon.setText(icons[position]);
        holder.title.setText(menus[position]);
    }

    @Override
    public int getItemCount() {
        return menus.length;
    }

    class ProfileMenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.icon)
        FontIcon icon;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.parent)
        ConstraintLayout parent;

        public ProfileMenuViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            parent.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                clickListener.onItemClick(ids[getAdapterPosition()]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    interface OnItemClickListener {
        void onItemClick(int id);
    }
}
