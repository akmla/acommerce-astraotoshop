package com.astra.astraotoshop.user.token;

import com.astra.astraotoshop.provider.entity.TokenEntity;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

/**
 * Created by Henra Setia Nugraha on 1/4/2018.
 */

public interface TokenContract {

    interface TokenView {
        void onRequestComplete();
        void onFailedLogin(String message);
    }

    interface TokenPresenter {
        void requestToken(String userName, String password, String role, String rolePath);

        void onTokenReceived(String response, String role);

        void onRequestTokenFailed();


    }

    interface TokenInteractor {
        void requestToken(NetworkHandler networkHandler, String rolePath, TokenEntity tokenEntity, String role);
    }
}
