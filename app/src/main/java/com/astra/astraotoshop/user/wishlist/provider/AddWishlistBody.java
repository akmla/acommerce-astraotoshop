package com.astra.astraotoshop.user.wishlist.provider;

import com.google.gson.annotations.SerializedName;

public class AddWishlistBody {

    @SerializedName("product")
    private int product;

    public AddWishlistBody(int product) {
        this.product = product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public int getProduct() {
        return product;
    }

    @Override
    public String toString() {
        return
                "AddWishlistBody{" +
                        "product = '" + product + '\'' +
                        "}";
    }
}