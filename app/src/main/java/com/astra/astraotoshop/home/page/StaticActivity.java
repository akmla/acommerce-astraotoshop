package com.astra.astraotoshop.home.page;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.checkout.doku.AOPWebClient;
import com.astra.astraotoshop.order.checkout.doku.WebViewListener;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StaticActivity extends BaseActivity implements WebViewListener {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("title")) {
            actionBarSetup(getIntent().getStringExtra("title"));
        } else {
            actionBarSetup();
        }
        webSetup();
        String url = BuildConfig.MAINURL + "simplecms/index/index?pageurl=" + getIntent().getStringExtra("url");
        webView.loadUrl(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void webSetup() {
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new AOPWebClient(this, this));
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.clearHistory();
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onFailed() {

    }

    @Override
    public void onLoaded() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        webView.getSettings().setJavaScriptEnabled(false);
        super.onDestroy();
    }
}
