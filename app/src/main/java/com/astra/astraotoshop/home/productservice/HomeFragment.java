package com.astra.astraotoshop.home.productservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.BrandAdapter;
import com.astra.astraotoshop.dummies.PopularProductAdapter;
import com.astra.astraotoshop.product.catalog.productservice.ProductServiceAdapter;
import com.astra.astraotoshop.product.catalog.productservice.ProductServicesActivity;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.synnapps.carouselview.CarouselView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/16/2017.
 */

public class HomeFragment extends Fragment implements ProductServiceAdapter.ItemClickListener, PopularProductAdapter.PopularListener{

    @BindView(R.id.brandList)
    RecyclerView brandList;
    @BindView(R.id.popularPackageList)
    RecyclerView popularPackageList;
    Unbinder unbinder;
    @BindView(R.id.carouselView)
    CarouselView carouselView;

    int[] carouselImage = {R.drawable.banner1, R.drawable.banner2, R.drawable.banner1, R.drawable.banner2};
    int[] brandImage = {R.drawable.brand1, R.drawable.brand2, R.drawable.brand3, R.drawable.brand1, R.drawable.brand2, R.drawable.brand3};
    private BrandAdapter brandAdapter;
    private PopularProductAdapter popularProductAdapter;
    private LinearLayoutManager brandLayoutManager;
    private LinearLayoutManager popularLayoutManager;

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_product, container, false);
        unbinder = ButterKnife.bind(this, view);

        carouselView.setPageCount(carouselImage.length);
        brandAdapter = new BrandAdapter(getContext(),null);
        popularProductAdapter = new PopularProductAdapter(this);
        carouselView.setImageClickListener(position -> {
            startActivity(new Intent(getContext(), ProductServicesActivity.class));
        });

        brandLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        popularLayoutManager = new LinearLayoutManager(getContext());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        carouselView.setImageListener((position, imageView) -> imageView.setImageResource(carouselImage[position]));

        brandList.setLayoutManager(brandLayoutManager);
        brandList.setAdapter(brandAdapter);

        popularPackageList.setLayoutManager(popularLayoutManager);
        popularPackageList.setAdapter(popularProductAdapter);
    }

    @Override
    public void onItemClick() {
        startActivity(new Intent(getContext(),ProductServicesActivity.class));
    }

    @Override
    public void onItemClick(int viewId) {

    }

    @Override
    public void onItemPopularClick() {
        startActivity(new Intent(getContext(),ProductDetailActivity.class));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
