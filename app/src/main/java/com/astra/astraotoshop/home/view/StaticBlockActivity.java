package com.astra.astraotoshop.home.view;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.checkout.doku.AOPWebClient;
import com.astra.astraotoshop.order.checkout.doku.WebViewListener;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StaticBlockActivity extends BaseActivity implements WebViewListener {

    @BindView(R.id.loading)
    ProgressBar loading;
    @BindView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_block);
        ButterKnife.bind(this);
        if (getIntent().hasExtra("alt")) {
            actionBarSetup(getIntent().getStringExtra("alt"));
        } else {
            actionBarSetup();
        }
        if (getIntent().hasExtra("pageUrl")) {
            webSetup();
            webView.loadUrl(getIntent().getStringExtra("pageUrl"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    private void webSetup() {
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new AOPWebClient(this, this));
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.clearHistory();
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onFailed() {

    }

    @Override
    public void onLoaded() {
        loading.setVisibility(View.GONE);
    }
}
