package com.astra.astraotoshop.home.productservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.home.page.StaticActivity;
import com.astra.astraotoshop.home.product.HomeProductActivity;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainPageActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav)
    NavigationView nav;
    @BindView(R.id.nav1)
    NavigationView nav1;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        ButterKnife.bind(this);
        toolbar.setTitle("");

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        nav.setNavigationItemSelectedListener(this);
        nav1.setNavigationItemSelectedListener(this);
    }

    @OnClick(R.id.drive)
    public void drive() {
        toHomePage(getResources().getInteger(R.integer.state_product_service));
    }

    @OnClick(R.id.parts)
    public void parts() {
        toHomePage(getResources().getInteger(R.integer.state_product));
    }

    private void toHomePage(int state) {
        startActivity((getIntent(this, HomeProductActivity.class)).putExtra(STATE, state));
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = getIntent(this, StaticActivity.class);
        switch (item.getItemId()) {
            case R.id.action_about_us: {
                intent.putExtra("title", "Tentang Kami");
                intent.putExtra("url", BuildConfig.MAINURL + "about-aop");
                break;
            }
            case R.id.action_contact: {
                intent.putExtra("title", "Kontak");
                intent.putExtra("url", BuildConfig.MAINURL + "contact");
                break;
            }
            case R.id.action_privacy: {
                intent.putExtra("title", "Kebijakan Privasi");
                intent.putExtra("url", BuildConfig.MAINURL + "privacy-policy");
                break;
            }
            case R.id.action_tac: {
                intent.putExtra("title", "Syarat dan Ketentuan");
                intent.putExtra("url", BuildConfig.MAINURL + "terms-and-conditions");
                break;
            }
            case R.id.action_faq: {
                intent.putExtra("title", "FAQ");
                intent.putExtra("url", BuildConfig.MAINURL + "faq");
                break;
            }
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        startActivity(intent);
        return false;
    }
}
