package com.astra.astraotoshop.home.provider;

import com.google.gson.annotations.SerializedName;

public class CategoryItem {

    @SerializedName("image")
    private String image;

    @SerializedName("category_id")
    private String categoryId;

    @SerializedName("name")
    private String name;

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return
                "ProductssItem{" +
                        "image = '" + image + '\'' +
                        ",category_id = '" + categoryId + '\'' +
                        ",name = '" + name + '\'' +
                        "}";
    }
}