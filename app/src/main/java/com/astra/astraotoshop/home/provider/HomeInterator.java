package com.astra.astraotoshop.home.provider;

import android.content.Context;

import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;
import com.astra.astraotoshop.utils.pref.Pref;
import com.google.gson.Gson;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/10/2018.
 */

public class HomeInterator extends BaseInteractor implements HomeContract.HomeInteractor {

    HomeContract.HomePresenter presenter;

    public HomeInterator(Context context, HomeContract.HomePresenter presenter) {
        super(context);
        this.presenter = presenter;
    }

    @Override
    public void requestStaticBlock(NetworkHandler networkHandler, String storeCode, String identifier) {
        getGeneralNetworkManager()
                .requestStaticBlock(networkHandler, storeCode, identifier)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        staticData -> presenter.onDataReceived(staticData, identifier),
                        error -> presenter.onFailedReceivingData(error)
                );
    }

    @Override
    public void requestStaticCategory(NetworkHandler networkHandler, String storeCode, String identifier) {
        getGeneralNetworkManager()
                .requestStaticCategory(
                        new NetworkHandler(),
                        storeCode,
                        identifier)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        staticCategory -> presenter.onCategoryReceived(staticCategory, identifier),
                        error -> presenter.onFailedReceivingCategory(error)
                );
    }

    @Override
    public void getBestSelling(String storeCode) {
        getGeneralNetworkManager()
                .requestBestSelling(new NetworkHandler(), storeCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        productList -> presenter.onBestSellingReceived(productList),
                        error -> presenter.onFailedReceivingBestSelling(error)
                );
    }

    @Override
    public void getNewestProduct(String product) {
        getGeneralNetworkManager()
                .requestNewestProduct(new NetworkHandler(),product)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        productList -> presenter.onNewestProductReceived(productList),
                        error -> presenter.onFailedReceivingNewestProduct(error)
                );
    }

    @Override
    public void getPromoProduct(String product) {
        getGeneralNetworkManager()
                .requestPromoProduct(new NetworkHandler(),product)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        productList -> presenter.onPromoProductReceived(productList),
                        error -> presenter.onFailedReceivingPromoProduct(error)
                );
    }
}
