package com.astra.astraotoshop.dummies;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Henra Setia Nugraha on 11/7/2017.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolders> {

    String[] a = {"aaaaaaa", "aaaaaaa", "aaaaaaa", "aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa","aaaaaaa", "aaaaaaa"};
    Adapater listener;

    public Adapter(Adapater listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolders(LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolders holder, int position) {
        holder.textView.setText(a[position]);
    }

    @Override
    public int getItemCount() {
        return a.length;
    }

    @Override
    public void onViewAttachedToWindow(ViewHolders holder) {
        super.onViewAttachedToWindow(holder);
        listener.onRecycled();
    }

    class ViewHolders extends RecyclerView.ViewHolder {

        TextView textView;

        public ViewHolders(View itemView) {
            super(itemView);
            textView = itemView.findViewById(android.R.id.text1);
        }
    }

    public interface Adapater{
        void onRecycled();
    }
}
